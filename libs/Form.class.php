<?php

/**
 * Class Form
 */
class Form {
	public $input2view = "input";
	/**
	 * email validator
	 * @param string $email
	 * @return string     'ok' if email is valid, otherwise error message
	 */
	function email_validation($email) {
		$mail_parts = explode("@", $email);
		// validate email
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			return _lang('email_is_incorrect');
		}
		// validate email domain
		elseif (!checkdnsrr(array_pop($mail_parts), "MX")) {
			return _lang('email_domain_is_incorrect');
		}
		else {
			return "ok";
		}
	}

	/**
	 * pid validator
	 * @param string $pid
	 * @return string     'ok' if pid is valid, otherwise error message
	 */
	function pid_validation($pid) {
		// pid email
		if (strlen($pid) !== 11 || !is_numeric($pid)) {
			return _lang('pid_is_incorrect');
		}
		else {
			return "ok";
		}
	}

	/**
	 * date fromat validator
	 * @param   array $parameters [date, format]
	 * @return bool TRUE || FALSE
	 */
	function date_validation($parameters) {
		$d = DateTime::createFromFormat($parameters['format'], $parameters['date']);

		return $d && $d->format($parameters['format']) == $parameters['date'];
	}

	/**
	 * checks password strength
	 * @param string $password
	 * @return int     password strength (0-100)
	 */
	function password_strength_checker($password) {
		$strength = 0;
		// check password length
		if (preg_match("/[^\w!@£]/", $password)) {
			$strength++;
		}
		if (preg_match("/[\d]/", $password)) {
			$strength++;
		}
		if (preg_match("/[A-Z]/", $password)) {
			$strength++;
		}
		if (preg_match("/[a-z]/", $password)) {
			$strength++;
		}

		$strength = strlen($password) < 6 ? strlen($password) : $strength * 25;

		return $strength;
	}

	/**
	 * @param array $parameters <b>parameter_key</b> is a form field name.<br>
	 *                          <b>value</b> - default: <b>$Router->post(field)</b>.<br>
	 *                          <b>required</b> - 1 required, default(0): optional.<br>
	 *                          <b>validator</b> - custom validator function. function must return TRUE or FALSE.<br>
	 *                          <b>validator_parameters</b> - validator function parameters
	 *                          <b>range</b> - accepted range of values.<br>
	 *                          <b>error_message</b> - custom error message.
	 * @return array    <b>if error:</b><br>
	 *                          ['errors'][$n]['field'] - field_name<br>
	 *                          ['errors'][$n]['info'] - error_message<br><br>
	 *                          <b>if ok:</b><br>
	 *                          validated data - [fields_name => field_value]
	 */
	function validate_form_fields($parameters) {
		global $Router;

		$n = 0;
		$data['errors'] = array();
		$data['fields'] = array();
		foreach ($parameters as $field_name => $field_info) {

			$field_info['required'] = isset($field_info['format']) && $field_info['format'] == 'required' ? 1 : 0;

			$value = $Router->post($field_name);
			$data['data'][$field_name] = $value;

			// check required fields
			if (isset($field_info['required']) && $field_info['required'] == 1 && $value === false) {
				$data['errors'][$n]['field'] = $field_name;
				$data['errors'][$n]['info'] = lang('FILL_REQUIRED_FIELDS');
			}

			// if isset validator
			if (isset($field_info['validator'])) {
				$validator_data = explode("->", $field_info['validator']);
				$validator_parameters = isset($field_info['validator_parameters']) ? $field_info['validator_parameters'] : null;
				$validation = false;
				// is validator is class method
				if (count($validator_data) == 2 && class_exists($validator_data[0])) {
					$validator_class = new $validator_data[0];
					if (method_exists($validator_class, $validator_data[1])) {
						$validation = $validator_class->{$validator_data[1]}($validator_parameters);
					}
				}
				// if validator is function
				elseif (function_exists($field_info['validator'])) {
					$validation = $field_info['validator']($validator_parameters);
				}

				if ($validation === false) {
					$data['errors'][$n]['field'] = $field_name;
					$data['errors'][$n]['info'] = isset($field_info['error_message']) ? $field_info['error_message'] : lang('DATA_IS_INCORRECT');
				}
			}

			// check accepted range
			if (isset($field_info['range']) && is_array($field_info['range']) && !in_array($value, $field_info['range'])) {
				$data['errors'][$n]['field'] = $field_name;
				$data['errors'][$n]['info'] = lang('DATA_IS_INCORRECT');
			}

			$n++;
		}

		return $data;
	}

	/**
	 * get array from db by keyword, for autocomplete forms
	 * @param   array   $parameters name    => field name in db<br>
	 *                              value   => keyword<br>
	 *                              table   => table name in db
	 * @return  array   id => field value
	 */
	function get_list_items_by_keyword($parameters) {
		global $MysqlDb;

		if (mb_strlen($parameters['value']) >= 3) {
			$MysqlDb->prepare_vars['keyword'] = $parameters['value'];
			return $MysqlDb->get_list($parameters['table'], "id", $parameters['name'], $parameters['name']." LIKE '%{{keyword}}%'");
		}
	}

	/**
	 * get list item id from db by name. If dosen't exists creates it
	 * @param   array   $parameters name    => field name in db<br>
	 *                              value   => field value<br>
	 *                              table   => table name in db
	 * @return  int   item_id
	 */
	function get_list_item_id_by_name($parameters) {
		global $MysqlDb;

		$parameters['name'] = isset($parameters['name']) ? $parameters['name'] : 'name';
		$MysqlDb->prepare_vars['value'] = $parameters['value'];
		$item_info = $MysqlDb->get_first_row($parameters['table'], "id", $parameters['name']." LIKE '{{value}}'");
		if(isset($item_info['id']) && (int)$item_info['id'] !== 0){
			return $item_info['id'];
		}
		elseif(strlen($parameters['value']) !== 0){
			$insert_id = $MysqlDb->insert($parameters['table'], array($parameters['name'] => $parameters['value']));

			return $insert_id;
		}
		else{
			return false;
		}
	}

    /**
     * Generate input field
     * @param string $name
     * @param string $type
     * @param string $value
     * @param string $items
     * @param string $style
     * @param string $class
     * @param string $other
     * @return mixed
     */
	function input_form($name, $type, $value = '', $items = '', $style = '', $class = '', $other = '') {
		global $Html;

		if (is_array($value))
			$edit_value = isset($value[$name]) ? htmlspecialchars((string)$value[$name]) : "";
		else
			$edit_value = htmlspecialchars((string)$value);

        switch ($type) {
            case "view" :
                $out ['input'] = "<span id=\"".$name."\">".htmlspecialchars_decode($edit_value)."</span>";
                $out ['view'] = "<span id=\"view_".$name."\">".htmlspecialchars_decode($edit_value)."</span>";
                break;
            case "calendar" :
            case "textbox" :
            case "text" :
                $out ['input'] = "<input name=\"".$name."\" id=\"".$name."\" type=\"text\" value=\"".$edit_value."\" class=\"".$class."\" style=\"".$style."\" ".$other.">\n";
                $out ['view'] = "<span id=\"view_".$name."\">".htmlspecialchars_decode($edit_value)."</span>";
                break;
            case "number" :
                $out ['input'] = "<input name=\"".$name."\" id=\"".$name."\" type=\"number\" value=\"".$edit_value."\" class=\"".$class."\" style=\"".$style."\" ".$other.">\n";
                $out ['view'] = "<span id=\"view_".$name."\">".htmlspecialchars_decode($edit_value)."</span>";
                break;
            case "hidden" :
                $out ['input'] = "<input name=\"".$name."\" id=\"".$name."\" type=\"hidden\" value=\"".$edit_value."\">\n";
                break;
            case "password" :
                $out ['input'] = "<input name=\"".$name."\" id=\"".$name."\" type=\"password\" class=\"".$class."\" style=\"".$style."\" ".$other.">\n";
                break;
            case "select" :
            case "select_style" :
                settype($items, "array");
                $out['input'] = "<select size=\"1\" id=\"".$name."\" name=\"".$name."\" style=\"".$style."\" class=\"".$class."\" ".$other.">\n";
                $multi_select_name = str_replace(array('[', ']'), '', $name, $multi_select);
                if ($multi_select == 2 && is_array($value[$multi_select_name])) {
                    foreach ($value[$multi_select_name] as $selected_items) {
                        $select[$selected_items] = " selected";
                    }
                } elseif (is_array($edit_value)) {
                    foreach ($edit_value as $value_val) {
                        $select[$value_val] = " selected";
                    }
                } else {
                    $select[$edit_value] = " selected";
                }
                if (isset($items[0]) && $items[0] == "_hide_") {
                    unset($items[0]);
                } elseif (!isset($items[0])) {
                    $out['input'] .= "<option value=\"\">------</option>\n";
                }

                if (is_array($items))
                    foreach ($items as $key => $value) {
                        /*if (!is_numeric($key)) {
                            list($key, $value) = array($value, $key);
                        }*/
                        $selected = isset($select[$key]) ? $select[$key] : "";
                        $out['input'] .= "<option value=\"".$key."\"".$selected.">".$value."</option>\n";
                    }
                $out['input'] .= "</select>\n";
                if ($type == "select_style") {
                    $out['input'] = "<span class=\"style_select\">".$out['input']."</span>";
                }
                //$items = @array_flip($items);
                $out['view'] = isset($items[$edit_value]) ? $items[$edit_value] : "";
                break;
            case "select_group" :
                $out['input'] = "<select size=\"1\" id=\"".$name."\" name=\"".$name."\" style=\"".$style."\" class=\"".$class."\" ".$other.">\n";
                $multi_select_name = str_replace(array('[', ']'), '', $name, $multi_select);
                if ($multi_select == 2 && is_array($value[$multi_select_name])) {
                    foreach ($value[$multi_select_name] as $selected_items) {
                        $select[$selected_items] = " selected";
                    }
                } else {
                    $select[$edit_value] = " selected";
                }
                $out['input'] .= "<option value=\"\">------</option>\n";
                if (is_array($items))
                    foreach ($items['groups'] as $group_key => $group_value) {
                        $out['input'] .= "<optgroup label=\"".$group_value."\">\n";
                        if (is_array($items['options'][$group_key])) {
                            foreach ($items['options'][$group_key] as $key => $value) {
                                if (!is_numeric($key)) {
                                    list($key, $value) = array(
                                        $value,
                                        $key
                                    );
                                }
                                $out['input'] .= "<option value=\"".$key."\"".$select[$key].">".$value."</option>\n";
                            }
                        }
                        $out['input'] .= "</optgroup>\n";
                    }
                $out['input'] .= "</select>\n";
                //$items = @array_flip($items);
                $out['view'] = $items[$edit_value];
                break;
            case "radio" :
                if (is_array($items)) {
                    $out['input'] = "";
                    foreach ($items as $key => $value) {
                        if (!is_numeric($key)) {
                            list($key, $value) = array($value, $key);
                        }
                        $select = $key == $edit_value ? " checked" : "";
                        $out['input'] .= "<input name=\"".$name."\" id=\"".$name."_".$key."\" type=\"radio\" value=\"".$key."\" ".$select." style=\"".$style."\" class=\"".$class."\" ".$other.">\n";
                        $out['input'] .= "<label for=\"".$name."_".$key."\" id=\"label_".$name."_".$key."\">".$value."</label>";
                    }
                    //$items = array_flip($items);
                    $out['view'] = isset($items[$edit_value]) ? $items[$edit_value] : "";
                }
                break;
            case "single_radio" :
                $select = $edit_value == $items ? " checked" : "";
                $out ['input'] = "<input name=\"".$name."\" id=\"".$name."_".$items."\" type=\"radio\" value=\"".$items."\" ".$select." style=\"".$style."\" class=\"".$class."\" ".$other.">\n";
                $out ['view'] = "<span id=\"view_".$name."\">".$items."</span>";
                break;
            case "checkbox" :
                $checked = "";
                if ((int)$edit_value == 1) {
                    $checked = " checked";
                }
                $out ['input'] = "<input name=\"".$name."\" id=\"".$name."\" type=\"checkbox\" value=\"1\"".$checked." style=\"".$style."\" class=\"".$class."\" ".$other.">\n";
                $out ['view'] = (int)$edit_value == 1 ? "<i class='fa fa-check'></i>" : "<i class='fa fa-ban'></i>";
                break;
            case "textarea" :
                $edit_value = str_replace("\r\n", "\n", $edit_value);
                $out ['input'] = "<textarea name=\"".$name."\" id=\"".$name."\" style=\"".$style."\" class=\"".$class."\" ".$other.">".$edit_value."</textarea>\n";
                $out ['view'] = "<span id=\"view_".$name."\">".nl2br($edit_value)."</span>";
                break;
            case "textarea_editor" :
                $out ['input'] = "<textarea name=\"".$name."\" style=\"".$style."\" id=\"editor".$items."\" class=\"".$class."\" ".$other.">".$edit_value."</textarea>\n";
                $out ['view'] = "<span id=\"view_".$name."\">".htmlspecialchars_decode($edit_value)."</span>";
                break;
            case "number":
                settype($items, "array");
                $min_max = isset($items['min']) && (int)$items['min'] !== 0 ? "min=\"".$items['min']."\" " : "";
                $min_max .= isset($items['max']) && (int)$items['max'] !== 0 ? "max=\"".$items['max']."\" " : "";
                $out ['input'] = "<input name=\"".$name."\" id=\"".$name."\" type=\"number\" value=\"".$edit_value."\" ".$min_max." class=\"".$class."\" style=\"".$style."\" ".$other.">\n";
                $out ['view'] = "<span id=\"view_".$name."\">".htmlspecialchars_decode($edit_value)."</span>";
                break;
            case "file":
                $id = rtrim($name, "[]");
                $out ['input'] = "<input type=\"file\" id=\"".$id."\" name=\"".$name."\" ".$other.">\n";
                if (is_array($items)) {
                    $n = 0;
                    $out ['view'] = "";
                    foreach ($items as $file_info) {
                        $n++;
                        if (is_file($file_info['file_src'])) {
                            $out ['view'] .= "<div class='upload_files'>";
                            $out ['view'] .= "	<i class=\"fa fa-paperclip\" aria-hidden=\"true\" style=\"margin-right:5px;\"></i>
												<a style='margin-right: 10px' target=\"_blank\" href=\"/".$file_info['file_src']."\">".$n.". ".$file_info['file_name']."</a>";
                            $out ['view'] .= $this->input2view == "input" ? $this->input_form($file_info['file_src']."_delete", "checkbox")." ".$Html->_lang('delete') : "";
                            $out ['view'] .= "</div>";
                        }
                    }
                    $out ['input'] .= $out['view'];
                }
                break;
        }
		return isset($out[$this->input2view]) ? $out[$this->input2view] : "";
	}

	function upload_image($uploadDir, $name, $pre, $type = 'none', $big_width = 600, $big_height = 400, $width = 150, $height = 150) {
		$file_ext = explode('.', $_FILES [$name] ['name']);
		$uploadFile = $pre.".".strtolower($file_ext [(count($file_ext) - 1)]);

		//*** create dir, if not exist
		if(!is_dir($uploadDir)){
			mkdir($uploadDir);
			mkdir($uploadDir."/thumb");
		}

		require_once "../libs/image.php";
		if ($_FILES [$name] ['name']) {
			move_uploaded_file($_FILES [$name] ['tmp_name'], "uploads/temp/".rawurldecode($uploadFile));
			$pic = $uploadFile;
			@$size = getimagesize("uploads/temp/".rawurldecode($uploadFile));
			$thumb = new thumbnail("uploads/temp/".rawurldecode($uploadFile));
			if ($type == 'logo') {
				if ($size [0] >= $size [1]) {
					$thumb->size_width($big_width);
				}
				else {
					$thumb->size_height($big_height);
				}
				$thumb->save($uploadDir."/".rawurldecode($uploadFile));
			}
			else {
				if ($size [0] <= $big_width && $size [1] <= $big_height) {
					$thumb->size_width($size [0]);;
				}
				elseif ($size [0] >= $size [1]) {
					$thumb->size_width($big_width);
				}
				else {
					$thumb->size_height($big_height);
				}
				$thumb->save($uploadDir."/".rawurldecode($uploadFile));
				$thumb = new thumbnail("uploads/temp/".rawurldecode($uploadFile));
				if ($size [0] >= $size [1]) {
					$thumb->size_width($width);
				}
				else {
					$thumb->size_height($height);
				}
				$thumb->save($uploadDir."/thumb/".rawurldecode($uploadFile));
			}
			@unlink("uploads/temp/".rawurldecode($uploadFile));
			return $uploadFile;
		}
		else {
			return false;
		}
	}

	function copy_image_percent($source, $destination, $size_percent = 100) {
		$uploadFile = $destination;
		if (is_file($source)) {
            require_once "../libs/image.php";
			@$size = getimagesize($source);
			$thumb = new thumbnail($source);

			$thumb->size_width(round($size[0] * $size_percent / 100));
			$thumb->save(rawurldecode($uploadFile));

			return $uploadFile;
		}
		else {
			return false;
		}
	}

	function file_extention($file_name, $exploder = '.') {
		$file_ext = explode($exploder, $file_name);
		return strtolower($file_ext[count($file_ext) - 1]);
	}

	function upload_multi_images($uploadDir, $name, $pre, $type = 'none', $big_width = 600, $big_height = 400, $width = 150, $height = 150) {
		global $Router;

		$uploadFile = [];
		for ($i = 0; $i < count($_FILES [$name]['name']); $i++) {
			$file_name = $_FILES [$name]['name'][$i];
			$file_tmp_name = $_FILES [$name]['tmp_name'][$i];

			$file_extension = $this->file_extention($file_name);
			//$file_name = post_int('skill_id')."_".time()."_".rand();
			//$image_name = $Router->post('image_name') == false ? rtrim($file_name, '.'.$file_extention) : $Router->post('image_name');
			$file_name = $pre."_".substr(time(), 6)."_".$i;
			$file_name = $file_name.".".$file_extension;

			require_once "../libs/image.php";
			if ($file_name && ($file_extension == "jpg" || $file_extension == "png")) {

				//*** create dir, if not exist
				if(!is_dir($uploadDir)){
					mkdir($uploadDir);
					mkdir($uploadDir."/thumb");
				}

				$uploadFile[$i] = strtolower($file_name);
				move_uploaded_file($file_tmp_name, "uploads/temp/".rawurldecode($uploadFile[$i]));
				$pic = $uploadFile[$i];
				@$size = getimagesize("uploads/temp/".rawurldecode($uploadFile[$i]));
				$thumb = new thumbnail("uploads/temp/".rawurldecode($uploadFile[$i]));
				if ($type == 'logo') {
					if ($size [0] >= $size [1]) {
						$thumb->size_width($big_width);
					}
					else {
						$thumb->size_height($big_height);
					}
					$thumb->save($uploadDir."/".rawurldecode($uploadFile[$i]));
				}
				else {
					if ($size[0] <= $big_width && $size[1] <= $big_height) {
						$thumb->size_width($size[0]);;
					}
					elseif ($size[0] >= $size[1]) {
						$thumb->size_width($big_width);
					}
					else {
						$thumb->size_height($big_height);
					}

					if ($big_width == "original") {
						copy("uploads/temp/".rawurldecode($uploadFile[$i]), $uploadDir."/".rawurldecode($uploadFile[$i]));
					}
					else {
						$thumb->save($uploadDir."/".rawurldecode($uploadFile[$i]));
					}

					$thumb = new thumbnail("uploads/temp/".rawurldecode($uploadFile[$i]));
					if ($size [0] >= $size [1]) {
						$thumb->size_width($width);
					}
					else {
						$thumb->size_height($height);
					}
					$thumb->save($uploadDir."/thumb/".rawurldecode($uploadFile[$i]));
				}
				@unlink("uploads/temp/".rawurldecode($uploadFile[$i]));
			}
		}
		return $uploadFile;
	}

	/**
	 * check if file exists
	 * @param string $name file name
	 * @return bool
	 */
	function file_name($name) {
		if (isset($_FILES [$name] ) && strlen($_FILES [$name]['name']) !== 0) {
			return $_FILES [$name] ['name'];
		}
		else {
			return false;
		}
	}

	/**
	 * @param $name file name
	 * @param $pref file prefix
	 * @param $upload_dir
	 * @return bool|string
	 */
	function file_upload($name, $pref, $upload_dir) {
		if ($this->file_name($name) !== false) {
			//** make dir */
			if(!is_dir($upload_dir)){
				mkdir($upload_dir);
			}

			$file_parts = explode('.', $this->file_name($name));
			$file_ext = array_pop($file_parts);
			move_uploaded_file($_FILES [$name] ['tmp_name'], $upload_dir."/".$pref.".".$file_ext);
			return $pref.".".$file_ext;
		}
		else {
			return false;
		}
	}

}
