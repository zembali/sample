<?php
/**
 * Router class
 */
class Router{
	/** @var int $enable_global_vars enable $_REQUEST vars to store in rote vars. */
	public $enable_global_vars = 1;

	/** @var int $admin_panel  if set to 1, then output will be in admin panel */
	public $admin_panel = 0;

	/** @var int $ajax  if set to 1, then output will be json */
	//public $ajax = 0;

    /** @var int $post  if set to 1, then output will be module output */
    public $post = 0;

    /** @var int $print  if set to 1, then output will be for printing */
    public $print = 0;

    /** @var int $cron  if set to 1, then exec_time will be increased */
	public $cron = 0;

	/** @var int $only_body  if set to 1, then output will be only with module template */
	public $only_body = 0;

	/** @var array   $get_data    request array (_GET). */
	public $get_data = array();
	/** @var array   $post_data    request array (_POST). */
	public $post_data = array();
	/** @var array   $post_data    request array (_POST). */
	public $post_data_with_tags = array();
	/** @var array   $request_data    request array (_GET, _POST). */
	public $request_data = array();

	/** @var string  $module current module */
	public $module = "";

	/** @var string  $page   current page */
	public $page = "";

	/** @var string  $action   form action name */
	public $action = "";

	/** @var string  $form_name */
	public $form_name = "";

	/** @var array   $modules_list   initialized modules list */
	public $modules_list = array();

	function __construct(){
        $this->url_to_array();
    }

	/**
	 * explode url and store in array
	 * @return array
	 * @param   string   $url    url string
	 */
	function url_to_array(){
		$url = isset($_GET['url']) ? $_GET['url'] : "";

		if($url !== "") {
			$url = ltrim($url, "/");
			// check if url starts with /admin/
			if(strpos($url, "admin") === 0){
				$this->admin_panel = 1;
				$url = substr($url, 5);
			}
			// check if url starts with /ajax/
			if(strpos($url, "ajax/") === 0){
				$this->ajax = 1;
				$url = substr($url, 5);
			}
			// check if url starts with /body/
			if(strpos($url, "body/") === 0){
				$this->only_body = 1;
				$url = substr($url, 5);
			}
			// check if url starts with /post/
			if(strpos($url, "post/") === 0){
				$this->post = 1;
				$url = substr($url, 5);
			}
			// check if url starts with /post/
			if(strpos($url, "print/") === 0){
				$this->print = 1;
				$url = substr($url, 5);
			}
			// check if url starts with /cron/
			if(strpos($url, "cron/") === 0){
				$this->cron = 1;
			}
			// change lang /lang/
			if(strpos($url, "lang/") === 0){
				$data = explode('/', $url);
				$_SESSION['lang'] = array_pop($data);

				header("Location: /");
				exit;
			}
			$data = explode('/', $url);
			while (!empty($data) && strlen(reset($data)) === 0) {
				array_shift($data);
			}
			while (!empty($data) && strlen(end($data)) === 0) {
				array_pop($data);
			}

			$data = array_map("sanitize_string", $data);

			//*** set defult module
			$data[0] = !isset($data[0]) ? "main" : $data[0];

			// if new module is set, load new one, or leave the same
			$this->module = $this->module !== "" && $this->module == $data[0] ? $data[0] : $this->sanitize_module_name($data[0]);
			$this->page = isset($data[1]) ? $data[1] : "";
			$this->action = isset($data[2]) ? $data[2] : false;
			$this->form_name = isset($data[3]) ? $data[3] : false;
		}
		else{
			$this->module = $this->load_module_init_files();
			$this->page = "";
		}

		if($this->enable_global_vars == 1){
			if($_SERVER['REQUEST_METHOD'] == "POST") {
				$this->post_data = array_map("sanitize_string", $_POST);
				$this->post_data_with_tags = array_map("sanitize_string_with_tags", $_POST);
			}

			if (isset($_GET['module']))	unset($_GET['module']);
			if (isset($_GET['page'])) unset($_GET['page']);

			$this->get_data = array_map("sanitize_string", $_GET);
			$this->request_data = array_merge($this->get_data, $this->post_data);
		}

		return $url;
	}

	/**
	 * Returns $module if it is in db, "main" otherwise
	 * @param string $module
	 * @return module
	 */
	function sanitize_module_name($module = "main") {
		global $MysqlDb;

		if(!in_array($module, $this->modules_list)) {
			$MysqlDb->prepare_vars['module'] = $module;
			if ($MysqlDb->record_exist("modules", "name = '{{module}}'") == false) {
				$module = "main";
			}
		}
		return $module;
	}

	/**
	 * Load module files
	 */
	function load_module_init_files() {
		global $Router, $MysqlDb, $UserInfo, $_lang; //load app classes

		$lang_file = ROOT_DIR."locale/".$this->app_lang()."/".$this->module.".php";
		if (is_file($lang_file)) {
			require_once $lang_file;
		}

		$index_file = ROOT_DIR."modules/".$this->module."/index.php";

		if(is_file($index_file)) {
			include $index_file;
		}

		$this->call_requested_method();
	}

	/**
	 * application current language
	 */
	function app_lang() {
		return isset($_SESSION['lang']) ? $_SESSION['lang'] : config('default_language');
	}

	/**
	 * call $_POST['action'] method if exists
	 * @return mixed method output
	 */
	function call_requested_method(){
		// call requested method
		$n = 'controller\\'.$this->module.'\\'.$this->page; // init controller namespace
		if (class_exists($n)) {
			$Controller = new $n(); // init controller class

			// call method if exists
			$action = isset($this->action) && strlen($this->action) !== 0 ? $this->action : "_html";
			$method_name = preg_replace("/[^A-Za-z0-9?!\_]/", "", $action);
			//$method_name = $action !== false ? $action : "content";
			if (method_exists($Controller, $method_name) && is_callable([$Controller, $method_name])) {
				echo $Controller->{$method_name}();
			}
		}
	}

	/**
	 * @param string    $name   _POST name
	 * @return mixed|string if exists returns _POST data, else returns $default
	 */
	function post($name, $default = false){
		if(array_key_exists($name, $this->post_data) && $this->post_data[$name] !== ""){
			return $this->post_data[$name];
		}
		else{
			return $default;
		}
	}

	/**
	 * post data with html tags
	 * @param string    $name   _POST name
	 * @return mixed|string if exists returns _POST data, else returns $default
	 */
	function post_with_tags($name, $default = false){
		if(array_key_exists($name, $this->post_data_with_tags) && $this->post_data_with_tags[$name] !== ""){
			return $this->post_data_with_tags[$name];
		}
		else{
			return $default;
		}
	}

	/**
	 * Loads file if is in accepted files list
	 * @param array		$pages acccepted pages
	 * @param string 	$default_page
	 */
	function load_module_page($pages, $default_page = "") {
		global $Router, $MysqlDb, $Html, $UserInfo; //load app classes

		$default_page = $default_page == "" ? ($Router->admin_panel == 1 ? "adminMain" : "main") : "main";

		if (isset($this->page) and in_array($this->page, $pages)) {
			$load_page = $this->page;
		}
		else {
			$load_page = $default_page;
			$this->page = $default_page;
		}

		$file_src = ROOT_DIR."modules/".$this->module."/pages/".$load_page.".php";
		if(is_file($file_src)){
			require_once $file_src;
		}

		return $load_page;
	}

	/**
	 * Loads module content with parameters (_GET)
	 * @param string	$module     module to load
	 * @param string	$page       page to load
	 * @param array 	$parameters _GET parameters
	 */
	function load_module_content_with_parameters($url = "", $parameters = array()) {
		global $Html; //load app classes

		//*** extract module/page/action
		$url = ltrim($url, "/");
		$data = explode('/', $url);
		$module = isset($data[0]) ? $data[0] : "";;
		$page = isset($data[1]) ? $data[1] : "";
		$action = isset($data[2]) ? $data[2] : "";

		// save global data
		$temp_data['module'] = $this->module;
		$temp_data['page'] = $this->page;
		$temp_data['action'] = $this->action;
		$temp_data['get_data'] = $this->get_data;

		$this->module = $module !== "" ? $module : $this->module;
		$this->page = $page !== "" ? $page : $this->page;
		$this->action = $action !== "" ? $action : $this->action;

		if(count($parameters) > 0){
			$this->get_data = array_merge($this->get_data, $parameters);
		}

		$this->load_module_init_files();

		// restore global data
		$this->module = $temp_data['module'];
		$this->page = $temp_data['page'];
		$this->action = $temp_data['action'];
		$this->get_data = $temp_data['get_data'];

		$output = $Html->output;
        $Html->output = "";
		return $output;
	}

	/**
	 * @param string    $name   request name
	 * @return int if exists returns request data, else returns $default
	 */
	function request_int($name, $default = 0){
		$request = $this->request($name, $default);

		if(is_numeric($request)){
			return (int)$request;
		}
		else{
			$default;
		}
	}

	/**
	 * @param string    $name   request name
	 * @return mixed|string if exists returns request data, else returns $default
	 */
	function request($name, $default = false){
		if(array_key_exists($name, $this->request_data)){
			return $this->request_data[$name];
		}
		else{
			return $default;
		}
	}

	/**
	 * @param string    $name   _GET name
	 * @return int if exists returns _GET data, else returns $default
	 */
	function get_int($name, $default = 0){
		$request = $this->get($name, $default);

		if(is_numeric($request)){
			return (int)$request;
		}
		else{
            return $default;
		}
	}

	/**
	 * @param string    $name   _GET name
	 * @return mixed|string if exists returns _GET data, else returns $default
	 */
	function get($name, $default = false){
		if(is_array($this->get_data) && array_key_exists($name, $this->get_data) && $this->get_data[$name] !== ""){
			return $this->get_data[$name];
		}
		else{
			return $default;
		}
	}

	/**
	 * @param string    $name   _POST name
	 * @return int if exists returns _POST data, else returns $default
	 */
	function post_int($name, $default = 0){
		$request = $this->post($name, $default);

		if(is_numeric($request)){
			return (int)$request;
		}
		else{
			$default;
		}
	}
}
