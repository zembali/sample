<?php
class Html {
	public $content_data = [];
	public $output = "";
	public $pager_items_on_page = [];
	public $pager_result_count = [];
	public $cover_image = "";
	public $direction = [];
	public $breadcrumb = [];
	
	function generate_module_html($module = "", $page_name = "", $custom_src = ""){
		global $Router, $UserInfo;

		$module = $module == "" ? $Router->module : $module;
		$page_name = $page_name == "" ? $Router->page.".tpl" : $page_name.".tpl";

		if($custom_src == ""){
			$file_src = ROOT_DIR."modules/".$module."/view/".$page_name;
		}
		else{
			$file_src = rtrim($custom_src, "/")."/".$page_name;
		}

		$this->output = "";

		if(is_file($file_src)){
			ob_start();
			extract($this->content_data);
			include $file_src;
			$lang_file = ROOT_DIR."Locale/".$Router->app_lang()."/".$module.".php";
			if(is_file($lang_file)){
				require_once $lang_file;
			}
			$this->output = ob_get_contents();
			ob_end_clean();
		}

		return $this->output;
	}

	/**
	 * set parameters for pager
	 * @param array $parameters [$pager_name => [<br>
	 *                              'result_count' => {value},
	 *                             'items_on_page' => {value}<br>
	 *                             ]<br>
	 *                          ]<br>
	 */
	function set_pager_info($parameters){
		foreach($parameters as $pager_name => $pager_info){
			if(isset($pager_info['result_count'])) {
				$this->pager_result_count[$pager_name] = $pager_info['result_count'];
			}
			if(isset($pager_info['items_on_page'])) {
				$this->pager_items_on_page[$pager_name] = $pager_info['items_on_page'];
			}
		}

	}

	/**
	 * get pager parameter(s)
	 * @param string $pager_name        you can create different parameters for different pager
	 * @param string $parameter_name    <b>items_on_page</b> - how many items on page<br>
	 *                                  <b>result_count</b> - how many rows in result<br>
	 *                                  <b>start_from</b> - ($pg - 1) * $items_on_page
	 * @return array|int if set <b>$parameter_name</b> returns parameters valus, otherwise array of parameters
	 */
	function get_pager_info($pager_name, $parameter_name = ""){
		global $Router;
		$result['items_on_page'] = isset($this->pager_items_on_page[$pager_name]) ? $this->pager_items_on_page[$pager_name] : 10;
		$result['result_count'] = isset($this->pager_result_count[$pager_name]) ? $this->pager_result_count[$pager_name] : 0;
		$result['start_from'] = $Router->get_int('pg') <= 0 ? 0 : ($Router->get_int('pg') - 1) * $result['items_on_page'];

		return $parameter_name !== "" ? $result[$parameter_name] : $result;
	}

	/**
	 * generate pager html
	 * @param string $pager_name
	 * @param string $pg
	 * @param string $page_url
	 * @return string
	 */
	function pager($pager_name, $pg = null, $page_url = ""){
		global $Router;

		$pager_info = $this->get_pager_info($pager_name);
		if($pager_info['result_count'] > $pager_info['items_on_page']){
			$this->content_data['pg'] = $pg == null ? $Router->get_int('pg') : $pg;
			$this->content_data['page_amount'] = ceil($pager_info['result_count']/$pager_info['items_on_page']);
			$this->content_data['start_page'] = $pg > 4 ? $pg - 3 : 1;

			$this->content_data['page_url'] = $page_url == "" ? preg_replace('/\b([&|&amp;]{0,1}pg=[0-9]*)\b/i', '', $_SERVER['REQUEST_URI']) : $page_url;

			return $this->generate_module_html("", "pager", ROOT_DIR."view/");
		}
	}

	function _lang($name){
		global $_lang;

        $name = strtolower($name);
		if(array_key_exists($name, (array)$_lang)){
			return $_lang[$name];
		}
		else{
			return "_".$name."_";
		}
	}

    /**
     * if key exists returns ar[key], otherwise default_value
     * @param $key
     * @param $ar
     * @param string $default
     * @return string
     */
	function valid_key($key, $ar, $default = ""){
        return array_key_exists($key, $ar) ? $ar[$key] : $default;
    }

	function e($name, $default = ""){
		if(array_key_exists($name, $this->content_data)){
			return $this->content_data[$name];
		}
		else{
			return $default;
		}
	}

    /**
     * get mail template text
     * @param $name template name
     * @param bool $parameters  parameters to be replaced
     * @return string
     */
    function get_mail_template($name, $parameters = false){
	    global $MysqlDb, $Router;

	    $template = $MysqlDb->get_first_row("email_templates", "title, template", "lang = '".$Router->app_lang()."'");

	    if($parameters){
	        foreach ($parameters as $key => $value) {
                $template['template'] = str_replace("{{".$key."}}", $value, $template['template']);
            }
        }

	    return $template;
    }
}
