<?php

/**
 * user permissions
 * @author Givi
 */
class Acl {
	/** Mysql class */
	public $MysqlDb;
	/** @var array $modules_info    [module_name => [id, custom_permissions]] */
	public $modules_info = array();
	/** @var array  $permissions_id [permission_name => id] */
	public $permissions_id = array();
	/** @var array  $permissions_id [id => permission_name] */
	public $permissions_name = array();
	/** @var array  $custom_field_id [id => field_name] */
	public $custom_field_name = array();

	function __construct() {
		global $MysqlDb;

		$this->MysqlDb = $MysqlDb;
	}

	/**
	 * get module info by id
	 * @param   string  $module_id
	 * @return  array   [id, name, custom_permissions] or FALSE
	 */
	function get_module_info($module_id) {
		if (!isset($this->modules_info[$module_id])) {
			$result = $this->MysqlDb->select("modules", "id, name, custom_permissions");
			while ($row = $this->MysqlDb->get_result($result)) {
				$this->modules_info[$row['id']] = [
					'name'                 => $row['name'],
					'custom_permissions' => $row['custom_permissions']
				];
			}
		}

		return isset($this->modules_info[$module_id]) ? $this->modules_info[$module_id] : false;
	}

	/**
	 * get permission name by id
	 * @param   int     $permission_id
	 * @return  string  permission_name or FALSE
	 */
	function get_permission_name($permission_id) {
		if (!isset($this->permissions_name[$permission_id])) {
			$this->permissions_name = $this->MysqlDb->get_list("modules_permissions", "id", "name");
		}

		return isset($this->permissions_name[$permission_id]) ? $this->permissions_name[$permission_id] : false;
	}

	/**
	 * check if user is root admin
	 * @return  bool
	 */
	function is_root_admin(){
		global $MysqlDb, $UserInfo;

		return $MysqlDb->record_exist("users_groups", "user_id = ".(int)$UserInfo->user_id." AND group_id = 1");
	}

	/**
	 * get custom field name by id
	 * @param   int     $field_id
	 * @return  string  field_name or FALSE
	 */
	function get_custom_field_name($field_id) {
		if (!isset($this->custom_field_name[$field_id])) {
			$this->custom_field_name = $this->MysqlDb->get_list("modules_permissions_custom_values", "id", "custom_field");
		}

		return isset($this->custom_field_name[$field_id]) ? $this->custom_field_name[$field_id] : false;
	}

	/**
	 * checks if user groups has changed
	 * @param int $user_id  default => current user_id
	 * @return bool
	 */
	function is_user_groups_changed($user_id = 0){
		global $UserInfo;

		$user_id = (int)$user_id == 0 ? $UserInfo->user_id : $user_id;

		$this->MysqlDb->prepare_vars['permission_last_update'] = $UserInfo->permission_last_update;
		return $this->MysqlDb->record_exist("users_groups_change_log", "user_id = ".(int)$user_id." AND add_time > '{{permission_last_update}}'");
	}

	/**
	 * @param int   $user_id  default => current user_id
	 * @return array    user groups id
	 */
	function get_user_groups($user_id = 0){
		global $UserInfo;

		$user_id = (int)$user_id == 0 ? $UserInfo->user_id : $user_id;

		if(!isset($_SESSION['user']['user_groups']) || $this->is_user_groups_changed()){
			$groups = $this->MysqlDb->get_list("users_groups", "group_id", "group_id", "user_id = ".(int)$user_id);
		}
		else{
            $groups = is_array($_SESSION['user']['user_groups']) && count($_SESSION['user']['user_groups']) ? $_SESSION['user']['user_groups'] : false;
		}

		//*** add to user group
        if($UserInfo->auth_status()){
		    $groups[3] = 3;
        }

        return $groups;
	}

	/**
	 * @param   array   $group_ids  user groups id
	 * @return array|bool           Group permissions array, or FALSE
	 */
	function get_group_permissions($group_ids){
		$permissions = array();
		if(is_array($group_ids) && count($group_ids) > 0){
			// group permissions
			$result_perm = $this->MysqlDb->select("groups_permissions", "id, module_id, permission_id", "group_id IN (".implode(",", $group_ids).")");
			$custom_permissions = array();
			while($row_perm = $this->MysqlDb->get_result($result_perm)){
				// basic permissions
				$module_info = $this->get_module_info($row_perm['module_id']);
				$permission_name = $this->get_permission_name($row_perm['permission_id']);
				if($permission_name === false) continue;
				$permissions[$module_info['name']][$permission_name]['basic'] = 1;

				// custom permissions
				if((int)$module_info['custom_permissions'] == 1 && !isset($custom_permissions[$module_info['name']])){
					$custom_permissions[$module_info['name']] = 1;
					$result_custom = $this->MysqlDb->select("group_custom_permissions", "custom_field_id, field_value, permission_id",
															"module_id = ".(int)$row_perm['module_id']." AND group_id = ".(int)$row_perm['id']);
					while($row_custom = $this->MysqlDb->get_result($result_custom)){
						if(!$permission_name = $this->get_permission_name($row_custom['permission_id'])) continue;
						if(!$custom_field_name = $this->get_custom_field_name($row_custom['custom_field_id'])) continue;
						$permissions[$module_info['name']][$permission_name]['custom'][$custom_field_name] = $row_custom['field_value'];
					}
				}
			}
		}

		return count($permissions) !== 0 ? $permissions : false;
	}

	/**
	 * @return array    updates cache and returns user permissions array
	 */
	function get_permission_cache(){
		global $UserInfo;

		// check if group permissions has updated
		$permission_updated = false;
		if($user_groups = $this->get_user_groups()){
			$this->MysqlDb->prepare_vars['permission_last_update'] = $UserInfo->permission_last_update;
			$permission_updated = $this->MysqlDb->record_exist("groups", "id IN (".implode(',', $user_groups).") AND last_update_time > '{{permission_last_update}}'");
		}

		// return permissions
		if($this->is_user_groups_changed() || $permission_updated){
			$permissions = $this->get_group_permissions($user_groups);

			if(!$this->MysqlDb->record_exist("users_permissions", "user_id = ".(int)$UserInfo->user_id)){
				$this->MysqlDb->insert("users_permissions", [
					'user_id'          => (int)$UserInfo->user_id,
					'permissions'      => json_encode($permissions),
					'last_update_time' => time()
				]);
			}
			else {
				$this->MysqlDb->update("users_permissions", [
					'permissions'      => json_encode($permissions),
					'last_update_time' => time()
				], "user_id = ".(int)$UserInfo->user_id);
			}

			// update user permissions last update time
			$_SESSION['user']['permission_last_update'] = time();
			$this->MysqlDb->update("users_permissions", ['last_update_time' => $UserInfo->permission_last_update], "user_id = ".(int)$UserInfo->user_id);
			$_SESSION['permissions'] = $permissions;
		}
		elseif(isset($_SESSION['permissions'])){
			$permissions = $_SESSION['permissions'];
		}
		else{
			$permissions_json = $this->MysqlDb->get_first_row("users_permissions", "permissions, last_update_time", "user_id = ".(int)$UserInfo->user_id);
			$permissions = $permissions_json['permissions'] !== "" ? json_decode($permissions_json['permissions'], true) : false;
			$_SESSION['permissions'] = $permissions;
		}

		return $permissions;
	}

	function has_permission($permission = "", $module = "", $custom_permission = 0){
		global $Router;

		if($this->is_root_admin()){
			return true;
		}
		else {
			$user_permissions = $this->get_permission_cache();

			$module = $module == "" ? $Router->module : $module;

			$has_permission = false;
			// if $permission is empty. Check if user has any permissions to this module
			if ($permission == "" && isset($user_permissions[$module])) {
				foreach ($user_permissions[$module] as $perm_info) {
					if (isset($perm_info['basic']) && $perm_info['basic'] == 1) {
						$has_permission = true;
						break;
					}
				}
			}
			else {
				// check basic permission
				if (isset($user_permissions[$module][$permission]['basic']) && (int)$user_permissions[$module][$permission]['basic'] == 1) {
					$has_permission = true;
				}

				// check custom permission
				if (is_array($custom_permission)) {
					foreach ($custom_permission as $perm_name => $perm_value) {
						if (isset($user_permissions[$module][$permission]['custom'][$perm_name]) &&
							(int)$user_permissions[$module][$permission]['custom'][$perm_name] == (int)$perm_value
						) {

							$has_permission = true;
							break;
						}
					}
				}
			}

			return $has_permission;
		}
	}

	/**
	 * if user doesn't have permission, redirect to main page
	 * @param   string  $permission permission name
	 * @param   string  $module     module name
	 * @param   array   $custom_permission
	 * @return  bool    redirect to main page
	 */
	function permission_redirect($permission = "", $module = "", $custom_permission = 0){
	    global $UserInfo;

		if(!$this->has_permission($permission, $module, $custom_permission)){
            if($UserInfo->auth_status()){
                header("Location: ".config('site_url'));
            }
            else {
                @header("Location: /users/login/?referrer=".base64_encode($_SERVER['REQUEST_URI']));
                echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=/users/login/?referrer=".base64_encode($_SERVER['REQUEST_URI'])."\"> ";
            }
			exit;
		}
	}
}
