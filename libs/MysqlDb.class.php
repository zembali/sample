<?php
class MysqlDb {
	public $host = "";
	public $username = "";
	public $password = "";
	public $db = "";
	public $table_prefix = "";
	/** @var array $connections connections */
	public $connections = array();
	/** @var array $active_connection active connection */
	public $mysql = array();
	/** @var array $prepare_vars vars to by prepared */
	public $prepare_vars = array();
	/** @var array $increase_value increase value (count = count + $increase_value) */
	public $increase_value = array();
	/** @var array $decrease_value decrease value (count = count + $decrease_value) */
	public $decrease_value = array();
	/** @var int $affected_rows */
	public $affected_rows = 0;
	/** @var string $key_value_splitter splitter for get_key_value() when $value is array*/
	public $key_value_splitter = ", ";
	/** @var bool   $show_deleted  if you wouldn't like to add 'del_time IS NULL' in WHERE statement, set TRUE  */
	public $show_deleted = false;

	function __construct($host = "", $username = "", $password = "", $db = "") {
		$this->host = $host == "" ? config('mysql_host') : $host;
		$this->username = $username == "" ? config('mysql_username') : $username;
		$this->password = $password == "" ? config('mysql_password') : $password;
		$this->db = $db == "" ? config('mysql_db') : $db;
		$this->table_prefix = config('table_pref');

		$this->connect();
	}

	private function connect(){
		if(!key_exists($this->db, $this->connections)){
			$this->mysql = new mysqli($this->host, $this->username, $this->password, $this->db);
			$this->mysql->set_charset("utf8");
		}
		if($this->mysql->connect_errno){
			die($this->error_report("db connection error"));
		}
		else{
			$this->connections[$this->db] = $this->mysql;
		}
		return $this->mysql;
	}

	/**
	 * Returns mysql error and query script if debug_mode is 1
	 * @param $query
	 * @return string
	 */
	private function error_report($query){
		global $global_config;
		if($global_config['debug_mode'] == 1){
			return "<span style=\"color: #D50000\"><b>Mysql Error</b></span>\n<P>".$this->mysql->error."<P>\n\n".$query;
		}
	}

	/**
	 * @param string       $table
	 * @param string|array $fields
	 * @param string       $where
	 * @param string       $order_by
	 * @param string       $limit
	 * @param string       $group_by
	 * @return mixed
	 */
	function select($table, $fields = "*", $where="", $order_by="", $limit = "", $group_by = ""){
		if(is_array($fields) && count($fields) > 0){
			$fields = implode(',', $fields);
		}

		$sql = "SELECT ".$fields." FROM ".$this->table_prefix.$table;
		if($where !== ""){
			if(count($this->prepare_vars) > 0){
				foreach ($this->prepare_vars as $key => $value){
					$where = str_replace("{{".$key."}}", $this->sanitize($value), $where);
				}
			}
			$sql .= " WHERE ".$where;
		}
		// select WHERE del_time IS NULL
		if($this->show_deleted == false){
			$sql .= $where == "" ? " WHERE del_time IS NULL" : " AND del_time IS NULL";
		}

		if($group_by !== "")
			$sql .= " GROUP BY ".$group_by;
		if($order_by !== "")
			$sql .= " ORDER BY ".$order_by;
		if($limit !== "")
			$sql .= " LIMIT ".$limit;
		return $this->execute_query($sql);
	}

	function sanitize($string) {
		$string = stripslashes($string);
		return $this->mysql->real_escape_string($string);
	}

	function execute_query($sql){
		$result = $this->mysql->query($sql) or die($this->error_report($sql));
		return $result;
	}

	/**
	 * Gets result
	 * @param $result
	 * @return array
	 */
	function get_result($result){
		return $result->fetch_assoc();
	}

	/**
	 * Insert data to table and return insert_id
	 * @param       $table
	 * @param array $fields
	 * @return insert_id
	 */
	function insert($table, $fields){
		$insert_field = $insert_value = '';
		foreach($fields as $key => $value){
			$insert_field .= ", `".$key."`";

            if($value === '_NULL_'){
                $insert_value .= ", null";
            }
            else{
                $insert_value .= ", '".$this->sanitize($value)."'";
            }
		}
		$insert_field = substr($insert_field, 2);
		$insert_value = substr($insert_value, 2);
		$sql = "INSERT INTO ".$this->table_prefix.$table." (".$insert_field.") VALUE (".$insert_value.")";
		$this->execute_query($sql);
		return $this->mysql->insert_id;
	}

	/**
	 * Update field "del_time" to time
	 * @param $table
	 * @param $where
	 */
	function delete($table, $where){
		$this->update($table, array("del_time" => current_time()), $where);
	}

	/**
	 * Update row and return affected rows
	 * @param       $table
	 * @param array $fields
	 * @param       $where
	 * @return affected_rows
	 */
	function update($table, $fields, $where){
		$update_field = $update_value = '';
		foreach($fields as $key => $value){
			if($value === '_INVERSE_'){
				$update_field .= ", `".$key."` = !".$key."";
			}
			elseif($value === '_INCREASE_'){
				$increase_value = array_key_exists($key, $this->increase_value) ? (int)$this->increase_value[$key] : 1;
				$update_field .= ", `".$key."` = ".$key."+".$increase_value;
			}
			elseif($value === '_DECREASE_'){
				$decrease_value = array_key_exists($key, $this->decrease_value) ? (int)$this->decrease_value[$key] : 1;
				$update_field .= ", `".$key."` = ".$key."-".$decrease_value;
			}
			elseif($value === '_NULL_'){
				$update_field .= ", `".$key."` = null";
			}
			else{
				$update_field .= ", `".$key."` = '".$this->sanitize($value)."'";
			}

		}

		if($where !== ""){
			if(count($this->prepare_vars) > 0){
				foreach ($this->prepare_vars as $key => $value){
					$where = str_replace("{{".$key."}}", $this->sanitize($value), $where);
				}
			}
		}

		// select WHERE del_time IS NULL
		if($this->show_deleted == false){
			$where .= $where == "" ? "del_time IS NULL" : " AND del_time IS NULL";
		}

		$update_field = substr($update_field, 2);
		$sql = "UPDATE ".$this->table_prefix.$table." SET ".$update_field." WHERE ".$where."";
		$this->execute_query($sql);
		return $this->mysql->affected_rows;
	}

	/**
	 * Delete row from table
	 * @param $table
	 * @param $where
	 */
	function force_delete($table, $where){
		if($where !== ""){
			if(count($this->prepare_vars) > 0){
				foreach ($this->prepare_vars as $key => $value){
					$where = str_replace("{{".$key."}}", $this->sanitize($value), $where);
				}
			}
		}
		$sql = "DELETE FROM ".$this->table_prefix.$table." WHERE ".$where;
		$this->execute_query($sql);
	}

	/**
	 * Returns TRUE if record exists, FALSE otherwise
	 * @param        $table
	 * @param string $where
	 * @return bool TRUE|FALSE
	 */
	function record_exist($table, $where = "1"){
		$row = $this->get_first_row($table, "id", $where);
		return isset($row['id']);
	}

	/**
	 * gets results first row
	 * @param string       $table
	 * @param string|array $fields
	 * @param string       $where
	 * @param string       $order_by
	 * @param string       $group_by
	 * @return array
	 */
	function get_first_row($table, $fields = "*", $where = "", $order_by = "", $group_by = ""){
		$result = $this->select($table, $fields, $where, $order_by, "0,1", $group_by);
		$result_assoc = $result->fetch_assoc();
		return $result_assoc === null ? false : $result_assoc;
	}

	/**
	 * gets results assoc array
	 * @param string $table
	 * @param string $fields
	 * @param string $where
	 * @param string $order_by
	 * @param string $group_by
	 * @return array
	 */
	function get_rows($table, $fields = "*", $where = "", $order_by = "", $limit = "", $group_by = ""){
		$result = $this->select($table, $fields, $where, $order_by, $limit, $group_by);
		$result_array = [];
		while($row = $this->get_result($result)){
			$result_array[] = $row;
		}
		return $result_array;
	}


	/**
	 * Returns array with key => value or merge of values, if $value is array
	 * @param string        $table
	 * @param string        $key   array key
	 * @param string|array $value array value or merge of values
	 * @param int           $where
	 * @param string        $order_by
	 * @param string        $limit
	 * @param string        $group_by
	 * @return array
	 */
	function get_list($table, $key = "id", $value = "name", $where = "1", $order_by = "", $limit = "", $group_by = "") {
		if (is_array($value)) {
			$query_value = "";
			foreach ( $value as $v ) {
				$query_value .= ", " . $v;
			}
		}
		else{
			$query_value = ", " . $value;
		}
		$result = $this->select($table, $key . $query_value, $where, $order_by, $limit, $group_by);
		$result_array = array();
		while($row = $this->get_result($result)){
			if (is_array($value)) {
				$v_values = array();
				foreach ( $value as $v ) {
					$v_values[] = $row[$v] ;
				}
				$result_array[$row[$key]] = implode($this->key_value_splitter, $v_values);
			}
			else{
				$result_array [$row[$key]] = $row[$value];
			}
		}
		return $result_array;
	}

	/**
	 * Returns number of rows in the result set.
	 * @param        $table
	 * @param string $where
	 * @param string $field
	 * @return int
	 */
	function count($table, $where = "1", $field = '*'){
		$row = $this->get_first_row($table, "COUNT(".$field.") AS result", $where);
		return (int)$row['result'];
	}

    /**
     * @param $table
     * @param $field
     * @param string $where
     * @return int
     */
    function max_value($table, $field, $where = "") {
        $result = $this->select($table, "MAX(".$field.") AS val", $where);
        $row = mysqli_fetch_assoc($result);
        return isset($row['val']) ? $row['val'] : 0;
    }

    /**
     * @param $table
     * @param $field
     * @param string $where
     * @return int
     */
    function min_value($table, $field, $where = "") {
        $result = $this->select($table, "MIN(".$field.") AS val", $where);
        $row = mysqli_fetch_assoc($result);
        return isset($row['val']) ? $row['val'] : 0;
    }
}
