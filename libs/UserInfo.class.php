<?php

class UserInfo {
	public $user_id = 0;
	public $permission_last_update = "";
	public $social_logins = [1 => 'facebook', 2 => 'google'];
	private $provider = [null];

	function __construct() {
		if(isset($_SESSION['user']['user_id']) && (int)$_SESSION['user']['user_id'] !== 0){
			$this->user_id = $_SESSION['user']['user_id'];
			$this->permission_last_update = isset($_SESSION['user']['permission_last_update']) ? $_SESSION['user']['permission_last_update'] : 0;
		}
	}

	/**
	 * if user is authorized returns TRUE, otherwise FALSE
	 * @return boolean
	 */
	function auth_status(){
		return $this->user_id == 0 ? false : true;
	}

	/**
	 * get user info by email
	 * @param   int     $email
	 * @return array    user_info if user_exists, otherwise FALSE
	 */
	function get_user_info_by_email($email){
		global $MysqlDb;

		$fields = "id, id AS user_id, first_name, last_name, email, password, status, last_login_time";
		$MysqlDb->prepare_vars['email'] = $email;
		if(!$email || strlen($email) == 0){
		    return false;
        }
		else{
		    $user_info = $MysqlDb->get_first_row("users", $fields, "email = '{{email}}'");
            return (int)$user_info['id'] !== 0 ? $user_info : false;
        }
    }

	/**
	 * get user info by social network user_id
	 * @param int|string    $social     user_id social network user_id
	 * @return array    user_info if user_exists, otherwise FALSE
	 */
	function get_user_info_by_social_user_id($social_user_id){
		global $MysqlDb, $UserInfo;

		$MysqlDb->prepare_vars['social_user_id'] = $social_user_id;
		$social_login_info = $MysqlDb->get_first_row("users_login_social_users", "user_id, social_network_id, social_user_id", "social_user_id != '' AND social_user_id = '{{social_user_id}}'");
		$user_info = array();
		if($social_login_info){
			$user_info = $UserInfo->get_user_info_by_id($social_login_info['user_id']);
			$user_info = array_merge($user_info, $social_login_info);
		}
		
		return count($user_info) !== 0 ? $user_info : false;
	}

	/**
	 * get user info (user_id, first_name, last_name, email, status, last_login_time) by id
	 * @param   int     $user_id
     * @param   string  $info_type  basic|all
	 * @return array    user_info if user_exists, otherwise FALSE
	 */
	function get_user_info_by_id($user_id = 0, $info_type = 'basic'){
		global $MysqlDb;

		$user_id = $user_id == 0 ? $this->user_id : $user_id;
		$fields = "id, id AS user_id, first_name, last_name, email, password, status, last_login_time, login_hash";
		$user_info = $MysqlDb->get_first_row("users", $fields, "id = ".(int)$user_id);

        if ($info_type == "all"){
            $other_info = $MysqlDb->get_first_row("users_other_info", "*", "user_id = ".(int)$user_id);
            unset($other_info['id']);

            if(is_array($other_info) && count($other_info) > 0) {
                $user_info = array_merge($user_info, $other_info);
            }
        }

		return (int)$user_info['id'] !== 0 ? $user_info : false;
	}

    function check_restore_code($code){
        global $MysqlDb;

        $MysqlDb->prepare_vars['restore_password_code'] = $code;
        $code_info = $MysqlDb->get_first_row("users_other_info", "user_id, restore_password_code_time", "restore_password_code = '{{restore_password_code}}'");

        return (int)$code_info['restore_password_code_time'] < time() - 3600 ? false : $code_info['user_id'];
    }
}
