<?php

function config($name) {
	global $global_config;

	if (isset($global_config[$name])) {
		return $global_config[$name];
	}
	else {
		return false;
	}
}

/**
 * read file content
 * @param   string $file_src
 * @return  string
 */
function read_file($file_src) {
	if (is_file($file_src)) {
		$handle = fopen($file_src, "r");
		$contents = fread($handle, filesize($file_src));
		fclose($handle);
		return $contents;
	}
	else {
		return false;
	}
}

function dd($string, $type = "basic"){
    global $Html;

    $Html->content_data = [];

    echo "<meta charset=\"utf-8\">";
    echo "<link href=\"/bootstrap/css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\">";
    echo "<style>body{position: fixed}</style>";
    echo "<pre class='bg-light p-3' style='position: fixed; top: 0; left: 0; right: 0; bottom: 0; height: 100vh; overflow: auto; z-index: 999'>";
    if(is_string($string)){
        echo $string;
    }
    elseif($type == "all"){
        var_dump($string);
    }
    else{
        print_r($string);
    }
    echo "</pre>";
    exit;
}

/**
 * @param string $string
 * @return string
 */
function sanitize_string($string) {
	$sanitized_string = trim(strip_tags($string));

	return htmlspecialchars($sanitized_string);
}

function sanitize_string_with_tags($string) {
	$sanitized_string = str_replace('script', 'sc_ript', trim($string));

	return $sanitized_string;
}


function current_time($time = "") {
	$time = $time == "" ? time() : $time;
	return date("Y-m-d H:i:s", $time);
}

function current_date($time = "") {
	$time = $time == "" ? time() : $time;
	return date("Y-m-d", $time);
}

/**
 * if debug mode is on, returns true
 */
function debug_mode() {
	return (bool)config('debug_mode');
}

/**
 * shows error message, if debug mode is on
 * @param string $message error message
 * @param int    $exit    if set to 1, then exits after echo message
 */
function error_message($message, $exit = 0) {
	if (debug_mode()) {
		echo "<span style=\"color: red; font-size: 20px\">".$message."</span>";
	}

	if ($exit == 1) {
		exit;
	}
}

/**
 * @param array $parameters <br>
 * status => ok|error <br>
 * ok_message => message when all done <br>
 * errors => [error messages] or [field_id => error message] <br>
 * redirect_url => redirect url <br>
 * @return strin json
 */
function request_callback($parameters) {
    if(isset($parameters['errors']) && !is_array($parameters['errors'])){
        $parameters['errors'] = [$parameters['errors']];
    }

	return json_encode($parameters, JSON_UNESCAPED_UNICODE);
}


function lang($name) {
	global $_lang;

	return isset($_lang[$name]) ? $_lang[$name] : "_".$name."_";
}

function date_to_unix($date, $format = 'Y-m-d') {
	$date = date_to_mysql_date($date, $format);
	if (validate_date($date, $format)) {
	    $hour = 0; $minute = 0; $second = 0;
		$date_parts = explode("-", substr($date, 0, 10));

        if(strlen($date) == 19) {
            $time_parts = explode(":", substr($date, 11));
            $hour = (int)$time_parts[0];
            $minute = (int)$time_parts[1];
            $second = (int)$time_parts[2];
        }

		$unix_time = mktime($hour, $minute, $second, $date_parts[1], $date_parts[2], $date_parts[0]);
		return (int)$unix_time;
	}
	else {
		return 0;
	}
}

function date_to_mysql_date($date, $format = 'Y-m-d'){
	$date = rawurldecode($date);
	if($format == "Y-m-d" || $format == "Y-m-d H:i:s"){
		if(validate_date($date, $format)) {
			return $date;
		}
	}
	elseif($format == "m/d/Y"){
		if(validate_date($date, $format)) {
			$date_parts = explode("/", $date);
			return $date_parts[2]."-".$date_parts[0]."-".$date_parts[1];
		}
	}
	elseif($format == "d/m/Y"){
		if(validate_date($date, $format)) {
			$date_parts = explode("/", $date);
			return $date_parts[2]."-".$date_parts[1]."-".$date_parts[0];
		}
	}
}

function validate_date($date, $format = 'Y-m-d'){
	$date = rawurldecode($date);
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}

function only_numbers($string){
	$output = preg_replace( '/[^0-9]/', '', $string );

	return $output;
}

function generatePassword($length = 9, $strength = 1) {
	$consonants [1] = range(1, 9);
	$consonants [2] = range('a', 'z');
	$consonants [3] = range('A', 'Z');

	$password = '';
	for($i = 0; $i < $length; $i ++) {
		$ar1 = rand(1, $strength);
		$ar2 = rand(0, (count($consonants [$ar1]) - 1));
		$password .= $consonants [$ar1] [$ar2];
	}
	return $password;
}


function send_mail($to, $title, $body) {
	global $MysqlDb, $UserInfo;

	if(config('dont_send_emails') == 1){
		return;
	}
	elseif (config('mail_sender') == "sendgrid") {

        require_once("functions/sendgrid/sendgrid-php.php");

        $sendgrid = new SendGrid(config('sendgrid_key'));
        $email = new SendGrid\Email();
        $email
            ->addTo($to)
            ->setFrom(config('contact_email'))
            ->setFromName(config('contact_name'))
            ->setSubject($title)
            ->setHtml($body);

        $report = $sendgrid->send($email);

        $fields ['error_message'] = $report->body['message'];
    }
    elseif (config('mail_sender') == "smtp") {
        include_once "../vendor/autoload.php";
        try {
            $enc_type = config('mail_ssl') ? 'ssl' : null;
            $transport = (new Swift_SmtpTransport(config('mail_host'), config('mail_port'), $enc_type))
                ->setUsername(config('mail_username'))
                ->setPassword(config('mail_password'));

                if (config('mail_ssl')) {
                    $transport->setStreamOptions(['ssl' => ['allow_self_signed' => true]]);
                }

            $mailer = new Swift_Mailer($transport);

            $message = (new Swift_Message($title))
                ->setFrom([config('contact_email') => config('contact_name')])
                ->setTo([$to])
                ->setBody($body, 'text/html');

            $fields ['error_message'] = $mailer->send($message);
        } catch (Exception $e) {
            echo $fields ['error_message'] = $e;
        }
    }

	$fields ['email'] = $to;
	$fields ['title'] = $title;
	$fields ['text'] = $body;
	$fields ['user_id'] = $UserInfo->user_id;
	$MysqlDb->insert("email_log", $fields);
}

function explode_text($text, $world = 20, $stip_tags = '<b>, <p>, <a>, <font>, <br>') {
    $out_text = explode(" ", strip_tags(htmlspecialchars_decode($text), $stip_tags));
    if (count($out_text) > $world) {
        $cut_text = "";
        $dot = "...";
        for($i = 0; $i < $world; $i ++) {
            $cut_text .= $out_text [$i] . " ";
        }
    } else {
        $cut_text = strip_tags(htmlspecialchars_decode($text), $stip_tags);
        $dot = "";
    }
    return $cut_text . $dot;
}

function cut_text_on_space($text, $length) {
    $text = strip_tags($text);
    if (strlen($text) > $length) {
        $world = explode(" ", $text);
        $cut_text = "";
        for($i = 0; $i < count($world); $i ++) {
            if(strlen($cut_text.$world [$i]) <= $length){
                $cut_text .= $world [$i] . " ";
            }
            else{
                break;
            }
        }
    } else {
        $cut_text = $text;
    }
    return trim($cut_text);
}

/**
 * alias to $Html->_lang()
 * @param $text
 */
function _lang($text){
    global $Html;

    return $Html->_lang($text);
}


function response_log($data){
    file_put_contents("D:\\qb.txt", print_r($data, true)."\n", FILE_APPEND);
}

function curl_request($url, $data)
{
    $fields_string = "";
    foreach ($data as $key => $value) {
        $fields[$key] = urlencode($value);
        $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function obj_to_array($obj) {
    $json = json_encode($obj, JSON_UNESCAPED_UNICODE);

    return json_decode($json, true);
}
