<?php
/**
 * initial db
 */

$SessDb = new MysqlDb(config('sess_mysql_host'), config('sess_mysql_username'), config('sess_mysql_password'), config('sess_mysql_db'));
$SessDb->show_deleted = true;

function sess_open($sess_path, $sess_name) {
	return true;
}

function sess_close() {
	return true;
}

function sess_read($sess_id) {
	GLOBAL $SessDb;

	$SessDb->prepare_vars['sess_id'] = $sess_id;
	$session_info = $SessDb->get_first_row("sessions", "data", "session_id = '{{sess_id}}'");
	if (!isset($session_info['data'])) {
		$fields['session_id'] = $sess_id;
		$fields['access_time'] = time();
		$SessDb->insert("sessions", $fields);
		return '';
	}
	else {
		$SessDb->update("sessions", array("access_time" => time()), "session_id = '{{sess_id}}'");
		return $session_info['data'];
	}
}

function sess_write($sess_id, $data) {
	GLOBAL $SessDb;

	$SessDb->prepare_vars['sess_id'] = $sess_id;
	$SessDb->update("sessions", array("access_time" => time(), "data" => $data), "session_id = '{{sess_id}}'");

	return true;
}

function sess_destroy($sess_id) {
	GLOBAL $SessDb;

	$SessDb->prepare_vars['sess_id'] = $sess_id;
	$SessDb->force_delete("sessions", "session_id = '{{sess_id}}'");
	return true;
}

function sess_gc($sess_maxlifetime) {
	GLOBAL $SessDb;

	$SessDb->force_delete("sessions", "access_time + ".$sess_maxlifetime." < ".time());
	return true;
}