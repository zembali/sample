<?php

/**
 * get or set form fields array
 */
abstract class Controller {
	public $form_fields = array();
	public $model;

	function __construct() {
		//global $MysqlDb, $Router, $UserInfo;

		// check authorization
		/*if ($UserInfo->auth_status() == false) {
			header("Location: /users/login/");
			exit;
		}*/
	}


	/**
	 * generate module html
	 */
	function _html() {
		global $UserInfo, $Html, $Router, $Acl, $Form;

		$Html->content_data['module'] = $Router->module;
		$Html->content_data['Acl'] = $Acl;
		$Html->content_data['UserInfo'] = $UserInfo;
		$Html->content_data['Form'] = $Form;

		// generate content
		$Html->generate_module_html();
	}
}
