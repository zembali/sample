# Framework structure
* **/libs** - Core engine libraries
* **/locale** - Multi language files
* **/modules** - Modules
    * **/modules/{module}/libs** - module models
    * **/modules/{module}/pages** - module controllers
    * **/modules/{module}/view** - module views
* **public_html** - root folder for a web server
* **view** - global views

# Autoloader
To call any controller or model, you just need to use a namespace.
```php
$model = new \model\{module}\{model}();
or
$controller = new \controller\{module}\{controller}(); 
```
### E.g:

```php
$Cron = new \model\cron\Cron();
```

# Routing

All the routes are defined automatically. It has the following structure:
```
/{module}/{controller}/{method}/
```
### E.g:

following url is going to call _**adminMain()->_html()**_ method in _**modules/users/pages/adminMain.php**_ file. (_html is a default method)
```
/users/adminMain
```
following url is going to call _**adminMain()->add_user()**_ method in _**modules/users/pages/adminMain.php**_ file.
```
/users/adminMain/add_user
```

