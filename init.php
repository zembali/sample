<?php
/**
 * initial startup classes and etc
 */
include ROOT_DIR."Autoloader.php";
include ROOT_DIR."libs/common.func.php";

//check short tags
if ((int)ini_get("short_open_tag") == 0) {
	error_message("Short tags is off", 1);
}

/**
 * initial sessions
 */
//include ROOT_DIR."libs/sessions.func.php";

//session_set_save_handler("sess_open", "sess_close", "sess_read", "sess_write", "sess_destroy", "sess_gc");
@session_start();
date_default_timezone_set("US/Eastern");


$MysqlDb = new MysqlDb();
$Router = new Router();
$Html = new Html();
$UserInfo = new UserInfo();
$Acl = new Acl();
$Form = new Form();
