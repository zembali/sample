<?php
DEFINE("ROOT_DIR", dirname(__FILE__) . "/");

$global_config = [
    /**
     * mysql config
     */
    'mysql_host' => "127.0.0.1",
    'mysql_username' => "",
    'mysql_password' => "",
    'mysql_db' => "",
    'table_pref' => "qb_",

    'hostname' => "http://localhost",

    /** debug mode */
    'debug_mode' => 1,

    'location' => "http://qb.sync",

    /** site info */
    'site_url' => "http://qb.sync",
    'languages' => ['eng' => 'eng'],
    'default_language' => 'eng',
];
