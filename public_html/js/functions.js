function yes_no(loc, message){
	if(message == null){
		message = '{{:yes_no_delete:}}';
	}
	if(confirm(message)){
		location.href = loc;
	}
}

function post_action(parameters, form, callback){
    var form = $(form);
    var form_id = form.attr('id');

    if(form.find("#errors_" + form_id).length == 0){
        form.prepend('<div id="errors_' + form_id + '" class="errors alert alert-danger"></div>');
    }
    var error_box = $("#errors_" + form_id);

    if(form.find("#ok_message").length == 0){
        form.prepend('<div id="ok_message_' + form_id + '" class="ok_message alert alert-success"></div>');
    }
    var ok_box = $("#ok_message_" + form_id);

    form.find(".is-invalid").removeClass("is-invalid");

    stop_scrolltop = $(document).height() > $(window).height() ? 0 : 1;

    var scroll_top = parseInt(form.offset().top) > 10 ? parseInt(form.offset().top) - 10 : 0;

    try{
        data = jQuery.parseJSON(parameters);
    }
    catch (e) {
        if(stop_scrolltop == 0){
            $('html, body').animate({
                scrollTop: scroll_top
            },500);
            $('.modal').animate({ scrollTop: 0 });
        }
        error_box.show();
        error_box.html('Unknown error');
        form.find("button[type='submit']").attr('disabled', false);

        return false
    }

    if(data.status == 'ok'){
        error_box.hide();
        $('.close').click()
    }
    else if(data.status == 'ok_message'){
        error_box.hide();
    }
    else{
        if(typeof(data.errors) !== "undefined") {
            if (stop_scrolltop == 0) {
                $('html, body').animate({
                    scrollTop: scroll_top
                }, 500);
                $('.modal').animate({scrollTop: scroll_top});
            }
            error_box.show();
            var cList = $('<ul class="list-unstyled"/>').appendTo(error_box);
            for(field_id in data.errors) {
                cList.append('<li>' + data.errors[field_id] + '</li>');
                form.find("#" +field_id).addClass("is-invalid");
            }
            error_box.html(cList);
        }
        form.find("button[type='submit']").attr('disabled', false);

    }

    if(typeof(data.redirect_url) != "undefined"){
        console.log(data.redirect_url);
        redirect(data.redirect_url);
    }

    if(typeof(data.ok_message) != "undefined"){
        if(stop_scrolltop == 0){
            $('html, body').animate({
                scrollTop: scroll_top
            },500);
            $('.modal').animate({ scrollTop: scroll_top });
        }
        if(data.ok_message.length){
            ok_box.css('opacity', '1');
            ok_box.css('display', 'block');
            ok_box.html('<i class="fas fa-check-circle text-success"></i> ' + data.ok_message);

            setTimeout(function(){
                ok_box.fadeTo('slow', 0, function(){
                    ok_box.css('display', 'none')});
                form.find("button[type='submit']").attr('disabled', false);
            }, 3000);
        }
    }


    if (eval("typeof " + callback) == "function") {
        var parameters = typeof data.callback_parameters !== "undefined" ? data.callback_parameters : data.status
        if (typeof callback == "function") {
            callback(parameters);
        }
        else{
            window[callback](parameters);
        }
    }
}


function obj_value(field_id){
	return $('#'+field_id).is(':checkbox') ? $("input[name='"+field_id+"']:checked").val() : $('#'+field_id).val();
}

function post_call(form, callback){
    var form = $(form);
    form.find("button[type='submit']").attr('disabled', true);
    var request = $.ajax({
        //dataType: "json",
        url: form.attr('action'),
        method: "POST",
        data: form.serializeArray(),
        success: function(data) {
            post_action(data, form, callback);
        }
    });
}

function change_img_src(parameters){
	img_info = parameters;
	$("#"+img_info.img_id).attr('src', img_info.img_src);
}

function cool_selecxbox(){
	var config = {
		'.chosen-select'           : {search_contains: true},
		'.chosen-select-deselect'  : {allow_single_deselect:true},
		'.chosen-select-no-single' : {disable_search_threshold:10},
		'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		'.chosen-select-width'     : {width:"95%"}
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}
}

function roundNumber(num) {
	var result = Math.round(num*Math.pow(10,2))/Math.pow(10,2);
	return result;
}

function redirect(url){
	window.location = url;
}

function reload(){
	window.location.reload();
}

function scrollToAnchor(aid){
	var aTag = $("a[name='"+ aid +"']");
	$('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

function check_multi_file_size_limit(field_id, single_size_limit, sum_size_limit){
    var fp = $("#" + field_id);
    var lg = fp[0].files.length; // get length
    var items = fp[0].files;
    var fileSize = 0;
    var current_file_size = 0;

    if (lg > 0) {
        for (var i = 0; i < lg; i++) {
            current_file_size = items[i].size;

            if(current_file_size > single_size_limit * 1024 *1024){
                return false;
            }

            fileSize = fileSize+current_file_size; // get file size
        }
        return fileSize < sum_size_limit * 1024 *1024;
    }
    else{
        return false;
    }
}

function breadcrumb(path_json){
    try {
        var path = JSON.parse(path_json);
    }
    catch (e) {
        var path = [];
    }

    var html = '';
    var n = 0;
    for (const key in path){
        n ++;
        html += '<a href="'+path[key]+'">' + key + '</a>';
        if (Object.keys(path).length > n){
            html += '<i class="fas fa-angle-right"></i>';
        }
    }

    $("#breadcrumb").html(html);
}

// delete
jQuery.fn.extend({
    delete: function (parameters, callback) {
        var obj = $(this);
        var p = parameters;
        var cb = callback;
        $(this).click(function (){
            // title
            if (typeof p.title !== "undefined") {
                $("#delete-title").html(p.title);
            }
            else{
                $("#delete-title").html($("#delete-title").data('title'));
            }

            // content
            if (typeof p.content !== "undefined") {
                $("#delete-body").html(p.content);
            }
            else{
                $("#delete-body").html($("#delete-body").data('text'));
            }

            $('#confirm-delete').modal('show');
        });

        $(document).on("click", "#delete-confirmed", function () {
            var data = obj.data();
            if (typeof p.url !== "undefined") {
                $.post(p.url, data)
                    .done(function(data){
                        // callback
                        if (eval("typeof " + cb) == "function") {
                            var parameters = typeof data !== "undefined" ? JSON.parse(data) : true
                            if (typeof cb == "function") {
                                cb(parameters);
                            }
                            else{
                                window[cb](parameters);
                            }
                        }
                        $('#confirm-delete').modal('hide');
                    })
            }
        })
    }
});


