var fileReader = new FileReader();
var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

fileReader.onload = function (event) {
    var image = new Image();

    image.onload=function(){
        var max_width = 1024;
        var max_height = 768;
        //document.getElementById("original-Img").src=image.src;
        var canvas=document.createElement("canvas");
        var context=canvas.getContext("2d");

        //*** calc sizes
        var size_ratio = max_width / max_height;
        var img_ratio = image.width / image.height;

        if(size_ratio > img_ratio){
            canvas.height = max_height;
            canvas.width = max_height * img_ratio;
        }
        else{
            canvas.width = max_width;
            canvas.height = max_width / img_ratio;
        }

        /*canvas.width=image.width/4;
        canvas.height=image.height/4;*/
        context.drawImage(image,
            0,
            0,
            image.width,
            image.height,
            0,
            0,
            canvas.width,
            canvas.height
        );

       // $("#image_data").val(canvas.toDataURL());
        $("#multi_images").attr("disabled", true);
        $("#loading").show();
        $("#ready").hide();

        $.post($("#multi_images").closest('form').attr('action'), {img_data: canvas.toDataURL()})
            .done(function(data){
                $("#loading").hide();
                $("#ready").show();
                $("#submit").attr("disabled", false);
                $("#multi_images").attr("disabled", false);

                error_message(data, $('#add_multi_images_form'))
            });

    }
    image.src=event.target.result;
};

var loadImageFile = function () {
    var uploadImage = document.getElementById("multi_images");

    //check and retuns the length of uploded file.
    if (uploadImage.files.length === 0) {
        return;
    }

    //Is Used for validate a valid file.
    var uploadFile = document.getElementById("multi_images").files[0];
    if (!filterType.test(uploadFile.type)) {
        alert("Please select a valid image.");
        return;
    }

    fileReader.readAsDataURL(uploadFile);
}