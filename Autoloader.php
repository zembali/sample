<?php
spl_autoload_register(function ($class_name) {
    $class_name = str_replace("\\", "/", $class_name);
    $class_name_parts = explode("/", trim($class_name, "/"));
    $class = count($class_name_parts) == 0 ? $class_name : end($class_name_parts);

    $class_file = false;
    if ($class_name_parts[0] == "controller" && count($class_name_parts) > 1) {
        $class_file = ROOT_DIR . "/modules/" . $class_name_parts[1] . "/pages/" . $class . ".php";
    }
    elseif ($class_name_parts[0] == "model") {
        $class_file = ROOT_DIR . "/modules/" . $class_name_parts[1] . "/libs/" . $class . ".class.php";
    }
    elseif (count($class_name_parts) == 1) {
        $class_file = ROOT_DIR . "/libs/" . $class . ".class.php";
    }

    if ($class_file && is_file($class_file)) {
        include_once($class_file);
    }
});
