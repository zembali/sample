<header id="header" class="p-t-15 p-b-15 bg-white b-b-1 border-gray">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a class="logo" to="/" href="/">
                    <img src="/images/logo.svg" class="no_mobile">
                    <img src="/images/logo_compact.png" alt="Eventals" class="mobile">
                </a>
                <div class="float-right">
                    <?= $Router->load_module_content_with_parameters("/menu/menu") ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</header>

<div id="wrapper">
    <div class="filter-page p-t-20">
        <div class="container bg-white shadow p-30">
            <?= $Router->load_module_content_with_parameters() ?>
        </div>
    </div>
</div>
