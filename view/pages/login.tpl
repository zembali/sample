<div id="wrapper">
    <div class="filter-page p-t-20">
        <div class="container bg-white shadow p-30">
            <?= $Router->load_module_content_with_parameters() ?>
        </div>
    </div>
</div>
