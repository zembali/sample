<div class="text-sm-center">
    <ul class="pagination">
        <? if($pg > 1): ?>
        <li class="page-item">
            <a class="page-link" href="<?=$page_url?>&pg=<?=($pg - 1)?>" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <? endif ?>

        <? if($start_page > 1): ?>
        <li class="page-item"><a class="page-link" href="<?=$page_url?>&pg=<?=1?>">1</a></li>
        <li class="page-item page-link">...</li>
        <? endif ?>

        <? for ($i = $start_page; $i <= ($page_amount > 7 && $start_page + 6 <= $page_amount ? $start_page + 6 : $page_amount); $i++):?>
        <li class="page-item <? echo $pg == $i ? 'active' : ''; ?>"><a class="page-link" href="<?=$page_url?>&pg=<?=$i?>"><?=$i?></a></li>
        <? endfor; ?>

        <? if($start_page + 6 < $page_amount): ?>
        <li class="page-item page-link">...</li>
        <li class="page-item"><a class="page-link" href="<?=$page_url?>&pg=<?=$page_amount?>"><?=$page_amount?></a></li>
        <? endif ?>

        <? if($pg < $page_amount): ?>
        <li class="page-item">
            <a class="page-link" href="<?=$page_url?>&pg=<?=($pg + 1)?>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
        <? endif ?>
    </ul>
</div>