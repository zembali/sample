<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Panel</title>
    <link rel="shortcut icon" type="image/png" href="/images/fav.png?v=4"/>

    <link href="/css/fontawesome/css/all.min.css" rel='stylesheet' type='text/css'>

    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="/css/style.css?v=4" rel="stylesheet" type="text/css">
    <link href="/css/style_<?= $Router->app_lang() ?>.css?v=1" rel="stylesheet" type="text/css">

    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/jquery-ui-timepicker-addon.js"></script>
    <script src="/js/jquery.form.js"></script>
    <script src="/js/functions.js?v=6"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/bootstrap/js/bootstrap-confirmation.js"></script>
    <script>
        $(document).on('hidden.bs.modal', function (e) {
            $(e.target).removeData('bs.modal');
        });
        jQuery(function () {
            $('[data-toggle="popover"]').popover();
        });
        $(document).on('click', '[data-toggle="modal"]', function () {
            $(".ajax_modal").html("");
            $($(this).data("target") + ' .modal-content').load($(this).attr("href"));
        });

    </script>
</head>
<body>
<header id="header" class="p-t-15 p-b-15 bg-white b-b-1 border-gray">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a class="logo" to="/" href="/">
                    <img src="/images/logo.svg">
                </a>
                <div class="float-right">
                    <?= $Router->load_module_content_with_parameters("/menu/menu") ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</header>

<?= $layout_content ?>

<div id="popup" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content ajax_modal"></div>
    </div>
</div>

<div id="popup_lg" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ajax_modal"></div>
    </div>
</div>

<div id="popup_sm" class="modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content ajax_modal"></div>
    </div>
</div>

<script>
    $('form#search_form').submit(function () {
        $(':input', this).each(function () {
            this.disabled = !($(this).val());
        });
    });
</script>
</body>
</html>
