<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= _lang('PAGE_TITLE') ?></title>
    <link rel="shortcut icon" type="image/png" href="/images/fav.png?v=4"/>

    <link href="/css/fontawesome/css/all.min.css" rel='stylesheet' type='text/css'>

    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="/css/style.css?v=4" rel="stylesheet" type="text/css">
    <link href="/css/style_<?= $Router->app_lang() ?>.css?v=1" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Comfortaa:700&display=swap">

    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/jquery-ui-timepicker-addon.js"></script>
    <script src="/js/chosen.jquery.min.js"></script>
    <script src="/js/jquery.form.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/functions.js?v=6"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/bootstrap/js/bootstrap-confirmation.js"></script>
    <script>
        $(document).on('hidden.bs.modal', function (e) {
            $(e.target).removeData('bs.modal');
        });
        $(document).on('click', '[data-toggle="modal"]', function () {
            $(".ajax_modal").html("");
            if($(this).attr("href") !== undefined) {
                $($(this).data("target") + ' .modal-content').load($(this).attr("href"));
            }
        });

        jQuery(function () {
            $('[data-toggle="popover"]').popover();
        });

        $(document).on("click", ".fa-cog", function () {
            var current_block = $(this).parent().children("div");
            var current_block_visibility = current_block.is(":visible") ? "none" : "inline-block";

            $(".actions").children("div").css('display', 'none');
            current_block.css('display', current_block_visibility);
        });

    </script>
</head>
<body>

<?= $layout_content ?>

<div id="popup" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content ajax_modal"></div>
    </div>
</div>

<div id="popup_lg" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ajax_modal"></div>
    </div>
</div>

<div id="popup_sm" class="modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content ajax_modal"></div>
    </div>
</div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title pull-left" id="delete-title" data-title="<?= _lang('confirm_delete') ?>"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body" id="delete-body" data-text="<?= _lang('are_you_sure') ?>"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= _lang('cancel') ?></button>
                <button class="btn btn-danger" id="delete-confirmed"><?= _lang('delete') ?></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
    });

    $(".datepicker").datepicker({
        minDate: 0,
    });

    $('form#search_form').submit(function () {
        $(':input', this).each(function () {
            this.disabled = !($(this).val());
        });
    });

</script>
</body>
</html>
