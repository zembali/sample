<?php

$template_page_values = [
	'login'  => [
		["users/login/"],
	],
];

$page_match = 0;
if (count($Router->get_data) <= 1 && $Router->get("url") == "") {
	$templates_page = "main";
}
elseif ($Router->print == 1) {
	$templates_page = "print";
}
else {
	foreach (array_keys($template_page_values) as $page) {
		foreach ($template_page_values[$page] as $url) {
			$page_match = 1;
			$request_value = array();
			foreach ($url as $key => $value) {
				if (trim($Router->get('url'), "/") !== trim($value, "/")) {
					$page_match = 0;
					break;
				}

                if(rtrim($value, "*") !== $value && strpos($Router->get('url'), rtrim($value, "*")) === 0){
                    $page_match = 1;
                }

                if ($page_match == 1) {
                    $templates_page = $page;
                    break;
                }
			}
		}

		if ($page_match == 1) {
			break;
		}
	}

	if ($page_match == 0) {
		$templates_page = "default";
	}
}

$lang_file = ROOT_DIR."locale/".$Router->app_lang()."/common.php";
if (is_file($lang_file)) {
	require_once $lang_file;
}

$Html->content_data['Acl'] = $Acl;

if ($Router->post == 1) {
	$Router->load_module_init_files();
}
elseif ($Router->cron == 1) {
    set_time_limit(0);
	$Router->load_module_init_files();
}
elseif ($Router->only_body == 1) {
	$Router->load_module_init_files();

	echo $Html->output;
}
elseif ($Router->admin_panel == 1) {
	if($UserInfo->auth_status() == false){
		header("Location: /users/login");
		exit;
	}
	$Html->content_data['layout_content'] = $Html->generate_module_html("", "admin", ROOT_DIR."view/pages/");

	echo $Html->generate_module_html("", "admin", ROOT_DIR."view/");
}
else {
	// main page
	if($Router->module == ""){
		$Html->content_data['layout_content'] = $Html->generate_module_html("", $templates_page, ROOT_DIR."view/pages/");

		echo $Html->generate_module_html("", "index", ROOT_DIR."view/");
	}
	// other pages
	else {
		$Html->content_data['layout_content'] = $Html->generate_module_html("", $templates_page, ROOT_DIR."view/pages/");

		echo $Html->generate_module_html("", "index", ROOT_DIR."view/");
	}
}
