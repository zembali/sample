<?php
$_lang['home'] = "Home";
$_lang['services'] = "Services";
$_lang['terms_conditions'] = "Terms & Conditions";
$_lang['about_us'] = "About Us";
$_lang['faqs'] = "FAQs";
$_lang['contact'] = "Contact";
$_lang['how_it_works'] = "How it works";
$_lang['privacy_policy'] = "Privacy policy";
$_lang['join'] = "Join";
