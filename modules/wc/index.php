<?php

$accepted_pages = [
    'main',
    'wc',
    'services',
    'orders',
    'products',
    'updatePrice',
    'comparePrices',
    'qbConfig'
];

$Router->load_module_page($accepted_pages);
