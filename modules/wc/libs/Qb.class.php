<?php

namespace model\wc;

class Qb {
    private $source_config = [];

    function error_log($error_message, $source = "") {
        global $MysqlDb;

        $MysqlDb->insert("wc_error_log", [
            'source' => $source,
            'error_message' => $error_message,
            'add_time' => current_time()
        ]);
    }

    function get_source_config($source_id) {
        global $MysqlDb;

        if (isset($this->source_config[$source_id])) {
            return $this->source_config[$source_id];
        }
        else {
            $config = $MysqlDb->get_first_row("wc_sources", "*", "id = ".(int)$source_id);
            $config['parameters'] = json_decode($config['parameters'], true);
            $this->source_config[$config['id']] = $config;

            return $config;
        }
    }

    function get_qb_customer($source_id, $date){
        global $MysqlDb;

        if(!isset($this->qb_customer[$source_id.$date])) {
            $MysqlDb->prepare_vars['date'] = $date;
            $customer_info = $MysqlDb->get_first_row("wc_qb_customers", "customer",
                "source_id = ".(int)$source_id." AND date_from <= '{{date}}' AND date_to >= '{{date}}'");

            if(isset($customer_info['customer'])) {
                $this->qb_customer[$source_id.$date] = $customer_info['customer'];
                return $customer_info['customer'];
            }
            else {
                $this->error_log("Cant find the QB customer; <br>Source_id: ".$source_id."<br>Date: ".$date);
                return false;
            }
        }
        else {
            return $this->qb_customer[$source_id.$date];
        }
    }

    /**
     * get mapped items
     * @param array $items [sku, qty, title, price]
     * @return array
     */
    function get_mapped_items($items) {
        global $MysqlDb;

        $part_numbers = [];
        foreach ($items as $i) {
            $part_numbers[] = $i['sku'];
        }
        $part_numbers = array_unique($part_numbers);

        if(count($part_numbers) > 0) {
            $map = $MysqlDb->get_list("wc_map", "id", "sku", "`sku` IN ('" . implode("','", $part_numbers) . "')");
            $qb_items = $MysqlDb->get_list("quickbooks_item", "name", "name", "`name` IN ('".implode("','", $part_numbers)."')");

            $qb_items = array_map("strtolower", $qb_items);
            $map = array_map("strtolower", $map);

            // return mapped items
            if (count($map) > 0) {
                $result = $MysqlDb->select("wc_map_qb_items", "name AS sku, qty, price_rate, map_id", "map_id IN ( '".implode("','", array_keys($map))."')");

                $map_items = [];
                while($row = $MysqlDb->get_result($result)) {
                    $map_sku = $map[$row['map_id']];
                    $map_items[$map_sku][] = [
                        'sku' => $row['sku'],
                        'qty' => $row['qty'],
                        'price_rate' => $row['price_rate'] / $row['qty']
                    ];
                }
            }

            $result_items = [];
            foreach ($items as $i) {
                $i_sku = strtolower($i['sku']);

                if(in_array($i_sku, $map)) {
                    foreach ($map_items[$i_sku] as $mi) {
                        $result_items[] = [
                            'sku' => $mi['sku'],
                            'qty' => $mi['qty'] * $i['qty'],
                            'price' => $mi['price_rate'] * $i['price']
                        ];
                    }
                }
                elseif(in_array($i_sku, $qb_items)) {
                    unset($i['title']);
                    $result_items[] = $i;
                }
                else{
                    $result_items[] = $i;
                    $this->error_log("Missing item(s): ".$i['sku']);
                }
            }

            return $result_items;
        }
        else {
            return [];
        }
    }

    function get_mapped_items_qty($qb_item) {
        global $MysqlDb;

        $MysqlDb->prepare_vars['list_id'] = $qb_item['list_id'];
        $map_ids = $MysqlDb->get_rows("wc_map_items", "map_id", "qb_list_id = '{{list_id}}'");

        $map_qty = [];
        if(count($map_ids) > 0) {
            foreach ($map_ids as $mi) {
                $map_items = $MysqlDb->get_list("wc_map_items", "qb_list_id", "qty", "map_id = " . (int)$mi['map_id']);

                $result_qb = $MysqlDb->select("quickbooks_item", "list_id, quantity_on_hand, quantity_on_sales_order",
                    "list_id IN ('" . implode("','", array_keys($map_items)) . "')");
                $qty = [];
                while ($row_qb = $MysqlDb->get_result($result_qb)) {
                    $actual_qty = $row_qb['quantity_on_hand'] - $row_qb['quantity_on_sales_order'];
                    $qty[] = $actual_qty < $map_items[$row_qb['list_id']] ? 0 : $actual_qty / $map_items[$row_qb['list_id']];
                }

                $mp_info = $MysqlDb->get_first_row("wc_map", "sku", "id = ".(int)$mi['map_id']);
                $map_qty[$mp_info['sku']] = min($qty);
                $map_qty[$qb_item['name']] = $qb_item['quantity_on_hand'] - $qb_item['quantity_on_sales_order'];
            }
        }
        else {
            $map_qty[$qb_item['name']] = (float)$qb_item['quantity_on_hand'] - (float)$qb_item['quantity_on_sales_order'];
        }

        return $map_qty;
    }

    function get_qb_title_by_sku($sku) {
        global $MysqlDb;

        $MysqlDb->prepare_vars['sku'] = $sku;
        $info = $MysqlDb->get_first_row("quickbooks_item", "sales_desc", "name = '{{sku}}'");

        return $info['sales_desc'] ?? "";
    }

    function get_shelf_id_by_sku($sku) {
        global $MysqlDb;

        $MysqlDb->prepare_vars['sku'] = $sku;
        $info = $MysqlDb->get_first_row("quickbooks_item", "shelf_id", "name = '{{sku}}'");

        return $info['shelf_id'] ?? "";
    }

    function fix_hex($str){
        return str_replace(['u0022'], ['"'], $str);
    }

    function get_product_package($item_id, $title) {
        global $MysqlDb;

        $MysqlDb->prepare_vars['item_id'] = $item_id;
        $info = $MysqlDb->get_first_row("wc_items", "*", "item_source_id = '{{item_id}}'");

        if($info) {
            preg_match("/^\([0-9]+\)/", $title, $pocket);

            return isset($pocket[0]) && (float)$info['mqty'] > 0 && only_numbers($pocket[0]) / $info['mqty'] !== 1
                ? only_numbers($pocket[0]) / $info['mqty']
                : false;
        }
        else {
            return false;
        }
    }
}
