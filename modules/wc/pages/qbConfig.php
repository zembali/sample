<?php

namespace controller\wc;


class qbConfig extends \Controller {

    function __construct() {
        global $Acl;

        $Acl->permission_redirect("orders_admin" );
    }

    function delete_qb_customer() {
        global $MysqlDb, $Router;

        $MysqlDb->delete("wc_qb_customers", "id = ".$Router->get_int('id'));

        echo "ok";
        exit;
    }

    function save_customer() {
        global $MysqlDb, $Router;

        $input_fields = ['source_id', 'date_from', 'date_to', 'customer'];

        foreach ($input_fields as $field_name) {
            if($Router->post($field_name) == false) {
                echo request_callback([
                    'status' => "error",
                    'errors' => [$field_name => $field_name." is empty"],
                ]);
                exit;
            }

            $insert_fields[$field_name] = $Router->post($field_name);
        }

        if($Router->get_int('edit_id') == 0) {
            $MysqlDb->insert("wc_qb_customers", $insert_fields);
        }
        else{
            $MysqlDb->update("wc_qb_customers", $insert_fields, "id = ".$Router->get_int('edit_id'));
        }

        echo request_callback([
            'status' => "success",
        ]);
        exit;
    }

    function add_form() {
        global $MysqlDb, $Html, $Router;

        $edit_value = $MysqlDb->get_first_row("wc_qb_customers", "*", "id = ".$Router->get_int('edit_id'));
        $sources = $MysqlDb->get_list("wc_sources");

        $Html->content_data = [
            'edit_value' => $edit_value,
            'sources' => $sources
        ];

        $Router->page = "add_qb_customer";
        parent::_html();
    }

    function _html() {
        global $Html, $MysqlDb;

        $qb_customers = [];
        $result = $MysqlDb->select("wc_qb_customers", "*", 1, "source_id ASC, date_from ASC");
        while ($row = $MysqlDb->get_result($result)){
            $qb_customers[$row['source_id']][] = $row;
        }

        $Html->content_data = [
            'sources' => $MysqlDb->get_list("wc_sources"),
            'qb_customers' => $qb_customers,
        ];

        parent::_html();
    }
}
