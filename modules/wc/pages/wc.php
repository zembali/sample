<?php

namespace controller\wc;

use \model\wc\Qb;

class wc extends \Controller {

    protected $_dsn;
    protected $errmap;
    protected $hooks;
    protected $log_level;
    protected $soapserver;
    protected $soap_options;
    protected $handler_options;
    protected $driver_options;
    protected $callback_options;
    protected $odd_symbols = [
        'Á' => "A",
        'á' => "a",
        'Ã' => "A",
        'ã' => "a",
        'Ẵ' => "A",
        'ẵ' => "a",
        'Ẫ' => "A",
        'ẫ' => "a",
        'â' => "a",
        'Â' => "A",
        'ᵬ' => "b",
        'ᵭ' => "d",
        'Ẽ' => "E",
        'ẽ' => "e",
        'È' => "E",
        'è' => "e",
        'Ḛ' => "E",
        'ḛ' => "e",
        'é' => "e",
        'É' => "E",
        'Ĩ' => "I",
        'ĩ' => "i",
        'Í' => "I",
        'í' => "i",
        'Ñ' => "N",
        'ñ' => "n",
        'Õ' => "O",
        'õ' => "o",
        'Ṍ' => "O",
        'ṍ' => "o",
        'Ỡ' => "O",
        'ỡ' => "o",
        'Ṏ' => "O",
        'ṏ' => "o",
        'ó' => "o",
        'Ó' => "O",
        'P̃' => "P",
        'p̃' => "p",
        'Ũ' => "U",
        'ũ' => "u",
        'Ữ' => "U",
        'ữ' => "u",
        'ú' => "u",
        'Ú' => "U",
        'Ṽ' => "V",
        'ṽ' => "v",
        'Ỹ' => "Y",
        'ỹ' => "y",
        'u0022' => "\""
    ];

    function __construct() {
        global $Acl;

        require_once ROOT_DIR.'/quickbooks/QuickBooks.php';
        require_once ROOT_DIR.'/quickbooks/quickBooks/WebConnector/Handlers.php';

        $this->errmap = array(
            500 => [$this, '_quickbooks_error_e500_notfound'], 			// Catch errors caused by searching for things not present in QuickBooks
            1 => [$this, '_quickbooks_error_e500_notfound'],
            '*' => [$this, '_quickbooks_error_catchall'], 				// Catch any other errors that might occur
        );

        $this->hooks = [
            QUICKBOOKS_HANDLERS_HOOK_LOGINSUCCESS => [[$this, 'hookLoginSuccess']]
        ];

        $this->log_level = config('qb_log');

        $this->soapserver = QUICKBOOKS_SOAPSERVER_BUILTIN;

        $this->soap_options = [];
        $this->driver_options = [];
        $this->callback_options = [];

        $this->handler_options = array(
            'deny_concurrent_logins' => false,
            'deny_reallyfast_logins' => false,
        );

        $this->_dsn = "mysqli://".config('mysql_username').":".config('mysql_password')."@".config('mysql_host')."/".config('mysql_db');
    }

    function clear_odd_symbols($str) {

        return str_replace(array_keys($this->odd_symbols), $this->odd_symbols, $str);
    }

    /**
     * @param json $address_json
     * @return array
     */
    function qb_formatted_address($address_json){
        $address = json_decode(stripslashes($address_json), true);
        $address = array_map('htmlspecialchars', $address);

        foreach ($address as $key => $value) {
            $address[$key] = $this->clear_odd_symbols($value);
        }

        $qb_address = [];
        if(isset($address['company']) && $address['company'] !== ""){
            $qb_address[] = cut_text_on_space($address['company'], 41);
        }

        if(isset($address['firstName'])) {
            $qb_address[] = cut_text_on_space($address['firstName'] ." ". ($address['lastName'] ?? ""), 41);
        }

        if(isset($address['streetLine1'])){
            $qb_address[] = cut_text_on_space($address['streetLine1'] ." ". ($address['streetLine2'] ?? ""), 41);
        }

        if(isset($address['city'])) {
            $qb_address[] = $address['city'].", ".$address['state']." ".$address['zip'];
        }

        if(isset($address['phone'])) {
            $qb_address[] = $address['phone'];
        }

        /*$address_lines = count($qb_address);
        if($address_lines < 4) {
            for($i = 0; $i < (4 - $address_lines); $i ++) {
                $qb_address[] = "aa";
            }
        }*/

        return $qb_address;
    }

    function hookLoginSuccess($requestID, $user, $hook, &$err, $hook_data, $callback_config)
    {
        global $Router;
        // Fetch the queue instance
        $Queue = new \QuickBooks_WebConnector_Queue($this->_dsn);
        $date = '1983-01-02 12:01:01';

        // ... and for items
        if (!$this->_quickbooks_get_last_run($user, QUICKBOOKS_IMPORT_ITEM))
        {
            $this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_ITEM, $date);
        }

        if (!$this->_quickbooks_get_last_run($user, QUICKBOOKS_TRANSFER))
        {
            $this->_quickbooks_set_last_run($user, QUICKBOOKS_TRANSFER, $date);
        }

        if (!$this->_quickbooks_get_last_run($user, QUICKBOOKS_INVOICE))
        {
            $this->_quickbooks_set_last_run($user, QUICKBOOKS_INVOICE, $date);
        }

        // Make sure the requests get queued up
        if($Router->get('method') == false || $Router->get('method') == "stock"){
            $Queue->enqueue(QUICKBOOKS_IMPORT_ITEM, 1, 1, null, $user);
        }

        if($Router->get('method') == "transfer" && $this->transfer_exist()){
            $Queue->enqueue(QUICKBOOKS_TRANSFER, 1, 2, null, $user);
        }

        if($Router->get('method') == "invoice" && $this->invoice_exist()){
            $Queue->enqueue(QUICKBOOKS_INVOICE, 1, 2, null, $user);
        }
    }

    /**
     * Get the last date/time the QuickBooks sync ran
     *
     * @param string $user		The web connector username
     * @return string			A date/time in this format: "yyyy-mm-dd hh:ii:ss"
     */
    function _quickbooks_get_last_run($user, $action)
    {
        $type = null;
        $opts = null;
        return \QuickBooks_Utilities::configRead($this->_dsn, $user, md5(__FILE__), 'last' . '-' . $action, $type, $opts);
    }

    /**
     * Set the last date/time the QuickBooks sync ran to NOW
     *
     * @param string $user
     * @return boolean
     */
    function _quickbooks_set_last_run($user, $action, $force = null)
    {
        $value = date('Y-m-d') . 'T' . date('H:i:s', time() - 300);

        if ($force)
        {
            $value = date('Y-m-d', strtotime($force)) . 'T' . date('H:i:s', strtotime($force));
        }

        return \QuickBooks_Utilities::configWrite($this->_dsn, $user, md5(__FILE__), 'last' . '-' . $action, $value);
    }

    /**
     *
     *
     */
    function _quickbooks_get_current_run($user, $action)
    {
        $type = null;
        $opts = null;
        return \QuickBooks_Utilities::configRead($this->_dsn, $user, md5(__FILE__), 'curr' . '-' . $action, $type, $opts);
    }

    /**
     *
     *
     */
    function _quickbooks_set_current_run($user, $action, $force = null)
    {
        $value = date('Y-m-d') . 'T' . date('H:i:s');

        if ($force)
        {
            $value = date('Y-m-d', strtotime($force)) . 'T' . date('H:i:s', strtotime($force));
        }

        return \QuickBooks_Utilities::configWrite($this->_dsn, $user, md5(__FILE__), 'curr' . '-' . $action, $value);
    }

    /**
     * Build a request to import customers already in QuickBooks into our application
     */
    function _quickbooks_item_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
    {
        global $Html, $Router;
        // Iterator support (break the result set into small chunks)
        $attr_iteratorID = '';
        $attr_iterator = ' iterator="Start" ';

        if (empty($extra['iteratorID']))
        {
            // This is the first request in a new batch
            $last = $this->_quickbooks_get_last_run($user, $action);
            $this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()

            // Set the current run to $last
            $this->_quickbooks_set_current_run($user, $action, $last);
        }
        else
        {
            // This is a continuation of a batch
            $attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
            $attr_iterator = ' iterator="Continue" ';

            $last = $this->_quickbooks_get_current_run($user, $action);
        }

        // Build the request
        $Html->content_data = [
            'version' => $version,
            'attr_iterator' => $attr_iterator,
            'attr_iteratorID' => $attr_iteratorID,
            'requestID' => $requestID,
            'last' => $last
        ];

        $Router->page = "items_request";
        parent::_html();
        return $Html->output;
    }

    /**
     * Handle a response from QuickBooks
     */
    function _quickbooks_item_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
    {
        global $MysqlDb;

        if (!empty($idents['iteratorRemainingCount']))
        {
            // Queue up another request
            $Queue = new \QuickBooks_WebConnector_Queue($this->_dsn);
            $Queue->enqueue(QUICKBOOKS_IMPORT_ITEM, null, 1, array( 'iteratorID' => $idents['iteratorID'] ), $user);
        }

        $Qb = new QB();

        // Import all of the records
        $errnum = 0;
        $errmsg = '';
        $Parser = new \QuickBooks_XML_Parser($xml);
        if ($Doc = $Parser->parse($errnum, $errmsg))
        {
            $Root = $Doc->getRoot();
            $List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemQueryRs');

            foreach ($List->children() as $Item)
            {
                $type = substr(substr($Item->name(), 0, -3), 4);
                $ret = $Item->name();

                $arr = array(
                    'list_id' => $Item->getChildDataAt($ret . ' ListID'),
                    'time_created' => $Item->getChildDataAt($ret . ' TimeCreated'),
                    'time_modified' => $Item->getChildDataAt($ret . ' TimeModified'),
                    'name' => $Item->getChildDataAt($ret . ' Name'),
                    'full_name' => $Item->getChildDataAt($ret . ' FullName'),
                    'type' => $type,
                    'is_active' => $Item->getChildDataAt($ret . ' IsActive'),
                    'parent_list_id' => $Item->getChildDataAt($ret . ' ParentRef ListID'),
                    'parent_full_name' => $Item->getChildDataAt($ret . ' ParentRef FullName'),
                    'manufacturer_part_number' => $Item->getChildDataAt($ret . ' ManufacturerPartNumber'),
                    'sales_taxCode_list_id' => $Item->getChildDataAt($ret . ' SalesTaxCodeRef ListID'),
                    'sales_tax_code_full_name' => $Item->getChildDataAt($ret . ' SalesTaxCodeRef FullName'),
                    'build_point' => $Item->getChildDataAt($ret . ' BuildPoint'),
                    'reorder_point' => $Item->getChildDataAt($ret . ' ReorderPoint'),
                    'quantity_on_hand' => $Item->getChildDataAt($ret . ' QuantityOnHand'),
                    'average_cost' => $Item->getChildDataAt($ret . ' AverageCost'),
                    'quantity_on_order' => $Item->getChildDataAt($ret . ' QuantityOnOrder'),
                    'quantity_on_sales_order' => $Item->getChildDataAt($ret . ' QuantityOnSalesOrder'),
                    'tax_rate' => $Item->getChildDataAt($ret . ' TaxRate'),
                );

                $look_for = array(
                    'sales_price' => array( 'SalesOrPurchase Price', 'SalesAndPurchase SalesPrice', 'SalesPrice' ),
                    'sales_desc' => array( 'SalesOrPurchase Desc', 'SalesAndPurchase SalesDesc', 'SalesDesc' ),
                    'purchase_cost' => array( 'SalesOrPurchase Price', 'SalesAndPurchase PurchaseCost', 'PurchaseCost' ),
                    'purchase_desc' => array( 'SalesOrPurchase Desc', 'SalesAndPurchase PurchaseDesc', 'PurchaseDesc' ),
                    'pref_vendor_list_id' => array( 'SalesAndPurchase PrefVendorRef ListID', 'PrefVendorRef ListID' ),
                    'pref_vendor_full_name' => array( 'SalesAndPurchase PrefVendorRef FullName', 'PrefVendorRef FullName' ),
                );

                if(isset($shelf_info['id'])) {
                    $arr['shelf_id_extra'] = $shelf_info['shelf_id_extra'];
                    $arr['weight'] = $shelf_info['weight'];
                    $arr['weight_unit'] = $shelf_info['weight_units'];
                    $arr['length'] = $shelf_info['length'];
                    $arr['width'] = $shelf_info['width'];
                    $arr['height'] = $shelf_info['height'];
                }

                foreach ($look_for as $field => $look_here)
                {
                    if (!empty($arr[$field]))
                    {
                        break;
                    }

                    foreach ($look_here as $look)
                    {
                        $arr[$field] = $Item->getChildDataAt($ret . ' ' . $look);
                    }
                }

                // ext. data
                foreach ($Item->children() as $ext_data) {
                    if ($ext_data->getChildDataAt('DataExtRet DataExtName') == "Package") {
                        $arr['package'] = $ext_data->getChildDataAt('DataExtRet DataExtValue');

                        break;
                    }
                }

                \QuickBooks_Utilities::log($this->_dsn, 'Importing ' . $type . ' Item ' . $arr['full_name'] . ': ' . print_r($arr, true));

                //print_r(array_keys($arr));
                //trigger_error(print_r(array_keys($arr), true));

                // Store the customers in MySQL
                $MysqlDb->prepare_vars['list_id'] = $arr['list_id'];
                if(!$MysqlDb->record_exist("quickbooks_item", "list_id = '{{list_id}}'")) {
                    $MysqlDb->insert("quickbooks_item", $arr);
                }
                else{
                    $MysqlDb->update("quickbooks_item", $arr, "list_id = '{{list_id}}'");
                }

                // update wc_items quantity
                $wc_items = $Qb->get_mapped_items_qty($arr);
                foreach ($wc_items as $sku => $qty) {
                    $MysqlDb->prepare_vars['sku'] = $sku;
                    if ($MysqlDb->record_exist("wc_items", "sku = '{{sku}}' AND store_qty != ".(float)$qty)) {
                        $MysqlDb->update("wc_items", ['store_qty' => $qty, 'synced' => 0, 'qb_sync_time' => current_time()], "sku = '{{sku}}'");
                    }
                }
            }
        }

        return true;
    }

    function syncWcQb() {
        global $MysqlDb;

        ini_set('max_execution_time', 3600);
        $Qb = new Qb();

        $result = $MysqlDb->select("quickbooks_item");
        while($row = $MysqlDb->get_result($result)) {
            $wc_items = $Qb->get_mapped_items_qty($row);
            foreach ($wc_items as $sku => $qty) {
                $MysqlDb->prepare_vars['sku'] = $sku;
                $wc_item_info = $MysqlDb->get_first_row("wc_items", "store_qty", "sku = '{{sku}}'");
                if ($wc_item_info) {
                    $fields = [
                        'store_qty' => $qty,
                        'synced' => (int)$wc_item_info['store_qty'] !== (int)$qty ? 0 : 1,
                        'qb_sync_time' => current_time()
                    ];
                    $MysqlDb->update("wc_items", $fields, "sku = '{{sku}}'");
                }
            }
        }
    }

    private function transfer_exist() {
        global $MysqlDb;

        return $MysqlDb->record_exist("wc_orders", "qb_status = 0 AND cancelled = 0 AND completed = 1");
    }

    private function invoice_exist() {
        global $MysqlDb;

        return $MysqlDb->record_exist("wc_orders", "qb_status = 2 AND shipped = 1 AND cancelled = 0");
    }

    function _quickbooks_transfer_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
    {
        global $Html, $MysqlDb, $Router;
        // Iterator support (break the result set into small chunks)
        $attr_iteratorID = '';
        $attr_iterator = ' iterator="Start" ';
        if (empty($extra['iteratorID']))
        {
            // This is the first request in a new batch
            $last = $this->_quickbooks_get_last_run($user, $action);
            $this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()

            // Set the current run to $last
            $this->_quickbooks_set_current_run($user, $action, $last);
        }

        $Qb = new Qb();
        $result = $MysqlDb->select("wc_orders", "*", "qb_status = 0 AND cancelled = 0 AND completed = 1", "qb_ref_number ASC", "0, 10");

        while ($order_info = $MysqlDb->get_result($result))
        {
            if(strlen($order_info['qb_ref_number']) > 9){
                continue;
            }
            $requested_order_ids[] = $order_info['id'];
            $customer = $Qb->get_qb_customer($order_info['source_id'], substr($order_info['order_create_time'], 0, 10));

            $source_config = $Qb->get_source_config($order_info['source_id']);

            if($customer) {
                if ($order_info['billing_address'] == "") {
                    $order_info['billing_address'] = $order_info['shipping_address'];
                }

                $Html->content_data['orders'][] = [
                    'sales_template' => $source_config['parameters']['qb_sales_template'],
                    'attr_iterator' => $attr_iterator,
                    'attr_iteratorID' => $attr_iteratorID,
                    'requestID' => $requestID,
                    'customer' => $customer,
                    'add_date' => substr($order_info['order_create_time'], 0, 10),
                    'order_id' => $order_info['qb_ref_number'],
                    'shipping_price' => $order_info['shipping_price'],
                    'shipping_address' => $this->qb_formatted_address($order_info['shipping_address']),
                    'billing_address' => $this->qb_formatted_address($order_info['billing_address']),
                    'memo' => json_decode($this->clear_odd_symbols($order_info['shipping_address']), true),
                    'order_items' => json_decode($order_info['qb_items'], true),
                    'tax' => json_decode($order_info['tax'], true),
                    'discount' => $order_info['discount'],
                ];
            }
        }

        if(isset($Html->content_data['orders'])){
            // mark transfer order as "transfer started"
            $MysqlDb->update("wc_orders", ['qb_status' => 1], "id IN (".implode(",", $requested_order_ids).")");

            $Router->page = "transfer";
            parent::_html();

            // Log
            if (config('qb_log')) {
                $Qb->error_log("Transfer:\n\n".$Html->output, "QB");
            }
            return $Html->output;
        }
        else{
            return true;
        }
    }

    /**
     * Handle a response from QuickBooks
     */
    function _quickbooks_transfer_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
    {
        global $MysqlDb;

        $Qb = new Qb();

        if (!empty($idents['iteratorRemainingCount']))
        {
            // Queue up another request
            $Queue = new \QuickBooks_WebConnector_Queue($this->_dsn);
            $Queue->enqueue(QUICKBOOKS_TRANSFER, null, 2, array( 'iteratorID' => $idents['iteratorID'] ), $user);
        }

        if (isset($xml)) {
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($xml);

            $response_array = obj_to_array($xml);
            if (isset($response_array['QBXMLMsgsRs']['SalesOrderAddRs'])) {
                if (isset($response_array['QBXMLMsgsRs']['SalesOrderAddRs']['@attributes'])) {
                    $SalesOrderAddRs[] = $response_array['QBXMLMsgsRs']['SalesOrderAddRs'];
                }
                else{
                    $SalesOrderAddRs = $response_array['QBXMLMsgsRs']['SalesOrderAddRs'];
                }

                foreach ($SalesOrderAddRs as $r) {
                    if(isset($r['@attributes']) && $r['@attributes']['statusMessage'] == "Status OK") {
                        $MysqlDb->prepare_vars['qb_ref_number'] = $r['SalesOrderRet']['RefNumber'];
                        $update_fields = [
                            'qb_status' => 2,
                            'qb_txn_id' => $r['SalesOrderRet']['TxnID'],
                            'sync_qb_time' => current_time()
                        ];
                        $MysqlDb->update("wc_orders", $update_fields, "qb_ref_number = '{{qb_ref_number}}'");
                    }
                    else {
                        $Qb->error_log(json_encode($r));
                    }
                }
            }
        }

        return true;
    }

    function _quickbooks_invoice_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
    {
        global $Html, $MysqlDb, $Router;
        // Iterator support (break the result set into small chunks)
        $attr_iteratorID = '';
        $attr_iterator = ' iterator="Start" ';
        if (empty($extra['iteratorID']))
        {
            // This is the first request in a new batch
            $last = $this->_quickbooks_get_last_run($user, $action);
            $this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()

            // Set the current run to $last
            $this->_quickbooks_set_current_run($user, $action, $last);
        }

        $Qb = new Qb();
        $result = $MysqlDb->select("wc_orders", "*", "qb_status = 2 AND completed = 1 AND shipped = 1 AND cancelled = 0", "", "0, 10");

        while ($order_info = $MysqlDb->get_result($result))
        {
            $requested_order_ids[] = $order_info['id'];
            $customer = $Qb->get_qb_customer($order_info['source_id'], substr($order_info['order_create_time'], 0, 10));

            $source_config = $Qb->get_source_config($order_info['source_id']);

            if($customer) {
                $Html->content_data['orders'][] = [
                    'invoice_template' => $source_config['parameters']['qb_invoice_template'],
                    'customer' => $customer,
                    'shipped_time' => substr($order_info['shipped_time'], 0, 10),
                    'order_create_time' => substr($order_info['order_create_time'], 0, 10),
                    'qb_txn_id' => $order_info['qb_txn_id'],
                ];
            }
        }

        if(isset($Html->content_data['orders'])){
            // mark transfer order as "transfer started"
            $MysqlDb->update("wc_orders", ['qb_status' => 3], "id IN (".implode(",", $requested_order_ids).")");

            $Router->page = "invoice";
            parent::_html();
            // Log
            if (config('qb_log')) {
                $Qb->error_log("Invoice:\n\n".$Html->output, "QB");
            }
            return $Html->output;
        }
        else{
            return true;
        }
    }

    /**
     * Handle a response from QuickBooks
     */
    function _quickbooks_invoice_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
    {
        global $MysqlDb;

        $Qb = new Qb();

        if (!empty($idents['iteratorRemainingCount']))
        {
            // Queue up another request
            $Queue = new \QuickBooks_WebConnector_Queue($this->_dsn);
            $Queue->enqueue(QUICKBOOKS_TRANSFER, null, 2, array( 'iteratorID' => $idents['iteratorID'] ), $user);
        }

        if (isset($xml)) {

            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($xml);

            $response_array = obj_to_array($xml);
            if (isset($response_array['QBXMLMsgsRs']['InvoiceAddRs'])) {
                if (isset($response_array['QBXMLMsgsRs']['InvoiceAddRs']['@attributes'])) {
                    $InvoiceAddRs[] = $response_array['QBXMLMsgsRs']['InvoiceAddRs'];
                }
                else{
                    $InvoiceAddRs = $response_array['QBXMLMsgsRs']['InvoiceAddRs'];
                }

                foreach ($InvoiceAddRs as $r) {
                    if(isset($r['@attributes']) && $r['@attributes']['statusMessage'] == "Status OK") {
                        $MysqlDb->prepare_vars['qb_txn_id'] = $r['InvoiceRet']['LinkedTxn']['TxnID'];
                        $MysqlDb->update("wc_orders", ['qb_status' => 4, 'sync_qb_time' => current_time()], "qb_txn_id = '{{qb_txn_id}}'");
                    }
                    else {
                        $Qb->error_log(json_encode($r));
                    }
                }
            }
        }

        return true;
    }

    /**
     * Handle a 500 not found error from QuickBooks
     *
     * Instead of returning empty result sets for queries that don't find any
     * records, QuickBooks returns an error message. This handles those error
     * messages, and acts on them by adding the missing item to QuickBooks.
     */
    function _quickbooks_error_e500_notfound($requestID, $user, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg)
    {
        //file_put_contents("D:\\qb.txt", "_quickbooks_error_e500_notfound\n", FILE_APPEND);
        $Queue = \QuickBooks_WebConnector_Queue_Singleton::getInstance();

        if ($action == QUICKBOOKS_IMPORT_ITEM)
        {
            return true;
        }
        if ($action == QUICKBOOKS_TRANSFER)
        {
            return true;
        }

        return false;
    }


    /**
     * Catch any errors that occur
     *
     * @param string $requestID
     * @param string $action
     * @param mixed $ID
     * @param mixed $extra
     * @param string $err
     * @param string $xml
     * @param mixed $errnum
     * @param string $errmsg
     * @return void
     */
    function _quickbooks_error_catchall($requestID, $user, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg)
    {
        $Qb = new Qb();

        $message = '';
        $message .= 'Request ID: ' . $requestID . "\r\n";
        $message .= 'User: ' . $user . "\r\n";
        $message .= 'Action: ' . $action . "\r\n";
        $message .= 'ID: ' . $ID . "\r\n";
        $message .= 'Extra: ' . print_r($extra, true) . "\r\n";
        //$message .= 'Error: ' . $err . "\r\n";
        $message .= 'Error number: ' . $errnum . "\r\n";
        $message .= 'Error message: ' . $errmsg . "\r\n";

        $Qb->error_log($message, "QB");
    }


    function _html() {
        global $Router, $Html, $MysqlDb, $UserInfo;

        $Qb = new Qb();

        $map = array(
            QUICKBOOKS_IMPORT_ITEM => [
                [$this, '_quickbooks_item_import_request'],
                [$this, '_quickbooks_item_import_response'],
            ],
            QUICKBOOKS_TRANSFER => [
                [$this, '_quickbooks_transfer_request'],
                [$this, '_quickbooks_transfer_response'],
            ],
            QUICKBOOKS_INVOICE => [
                [$this, '_quickbooks_invoice_request'],
                [$this, '_quickbooks_invoice_response'],
            ],
        );

        try {
            $Server = new \QuickBooks_WebConnector_Server(
                $this->_dsn,
                $map,
                $this->errmap,
                $this->hooks,
                $this->log_level,
                $this->soapserver,
                QUICKBOOKS_WSDL,
                $this->soap_options,
                $this->handler_options,
                $this->driver_options,
                $this->callback_options
            );
            $response = $Server->handle(true, true);
        }
        catch (\Exception $e){
            $Qb->error_log($e, "QB");
        }
        //parent::_html();
    }
}
