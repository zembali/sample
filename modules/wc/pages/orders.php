<?php

namespace controller\wc;

use \model\wc\Qb;

class orders extends \Controller {

    function __construct() {
        global $Acl;

        $Acl->permission_redirect("orders_view");
    }

    function save_order() {
        global $Router, $MysqlDb;

        $Orders = new \model\cron\Orders();
        $Qb = new \model\wc\Qb();

        $order = $MysqlDb->get_first_row("wc_orders", "*", "id = ".$Router->get_int('order_id'));

        // order items
        $order_items = [];
        foreach (json_decode($order['order_items'], true) as $key => $i) {
            $part_num = $Router->post('item_' . md5($i['sku']));
            if ($part_num == false) {
                echo request_callback([
                    'status' => "error",
                    'errors' => ['item_' . md5($i['sku']) => "SKU is empty"],
                ]);
                exit;
            }

            $MysqlDb->prepare_vars['part_num'] = $part_num;
            if (!$MysqlDb->record_exist("wc_items", "source_id = ".(int)$order['source_id']." AND source_sku = '{{part_num}}'")) {
                echo request_callback([
                    'status' => "error",
                    'errors' => ['item_' . md5($i['sku']) => "SKU doesn't exist"],
                ]);
                exit;
            }

            $order_items[$key] = $i;
            $order_items[$key]['sku'] = $Router->post('item_' . md5($i['sku']));
        }

        // shipping address
        $shipping_update = [];
        foreach (json_decode($order['shipping_address'], true) as $key => $value) {
            $shipping_update[$key] = $Router->post('sh_addr_' . $key);
        }

        // billing address
        $billing_update = [];
        if($billing_address = json_decode($order['billing_address'], true)) {
            foreach ($billing_address as $key => $value) {
                $billing_update[$key] = $Router->post('b_addr_' . $key);
            }
        }

        //save the order
        $qb_items = $Qb->get_mapped_items($order_items);

        $fields = [
            'source_id' => $order['source_id'],
            'order_id' => $order['order_id'],
            'order_items' => $order_items,
            'shipping_address' => $shipping_update,
            'billing_address' => $billing_update,
            'qb_items' => $qb_items,
            'qb_status_force' => $Router->post_int('qb_status'),
            'shipped' => $Router->post_int('shipped'),
            'completed' => $Router->post_int('completed'),
            'cancelled' => $Router->post_int('cancelled'),
            'order_update_time' => current_time()
        ];

        $Orders->save_order($fields);

        echo request_callback([
            'status' => "success",
        ]);
        exit;
    }

    function order_details() {
        global $Html, $MysqlDb, $Router;

        $order_info = $MysqlDb->get_first_row("wc_orders", "*", "id = ".$Router->get_int('order_id'));
        $order_statuses = $MysqlDb->get_list("wc_order_statuses", "id", "name");
        array_unshift($order_statuses, 'Ready to Transfer');
        $sources = $MysqlDb->get_list("wc_sources", "id", "name");

        $Html->content_data = [
            'sources' => $sources,
            'order' => $order_info,
            'order_statuses' => $order_statuses
        ];

        $Router->page = "order_details";
        parent::_html();
    }

    function print_order() {
        global $Html, $MysqlDb, $Router;

        $Qb = new Qb();

        // get source configs
        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = " . $Router->get_int('source_id'));
        $config['parameters'] = json_decode($config['parameters'], true);

        $where = "source_id = " . $Router->get_int('source_id') . " AND completed = 1 AND cancelled = 0";
        $where .= $Router->get_int('id') !== 0 ? " AND id = " . $Router->get_int('id') : " AND (print_time IS NULL OR print_time = '0000-00-00 00:00:00')";
        $result_orders = $MysqlDb->select("wc_orders", "*", $where, "qb_ref_number DESC");

        $orders = [];
        $printed_orders = [];
        while ($row_o = $MysqlDb->get_result($result_orders)) {
            $same_customer = $MysqlDb->get_list("wc_orders", "id", "qb_ref_number",
                "id != " . (int)$row_o['id'] . " AND shipping_hash = '" . $row_o['shipping_hash'] . "' AND cancelled = 0 AND print_time IS NULL");

            $orders[] = [
                'id' => $row_o['id'],
                'source_id' => $row_o['source_id'],
                'qb_ref_number' => $row_o['qb_ref_number'],
                'order_id' => $row_o['order_id'],
                'order_create_time' => $row_o['order_create_time'],
                'print_time' => $Router->get_int('printed') == 1 ? current_time() : $row_o['print_time'],
                'address' => json_decode($row_o['shipping_address'], true),
                'products' => json_decode($row_o['order_items'], true),
                'shipping_price' => $row_o['shipping_price'],
                'same_customer' => $same_customer,
            ];

            $printed_orders[] = $row_o['id'];
        }

        // mark as printed
        if ($Router->get_int('printed') == 1 && count($printed_orders) > 0) {
            $MysqlDb->update("wc_orders", ['printed' => 1, 'print_time' => current_time()],
                "id IN (" . implode(",", $printed_orders) . ")");
        }

        $Html->content_data = [
            'orders' => $orders,
            'config' => $config,
            'Qb' => $Qb
        ];
        $Router->page = "print_page";
        parent::_html();
    }

    function _html() {
        global $Html, $MysqlDb, $Router;

        $results_on_page = 20;
        $where = "source_id = " . $Router->get_int('source_id') . " AND completed = 1";
        // Search
        if ($Router->get('qb_status_id') !== false) {
            $where .= " AND qb_status = ".$Router->get_int('qb_status_id');
        }
        if ($Router->get('qb_number')) {
            $MysqlDb->prepare_vars['qb_number'] = $Router->get('qb_number');
            $where .= " AND qb_ref_number = '{{qb_number}}'";
        }
        if ($Router->get('order_id')) {
            $MysqlDb->prepare_vars['order_id'] = $Router->get('order_id');
            $where .= " AND order_id = '{{order_id}}'";
        }

        $pg = $Router->get_int('pg') < 1 ? 1 : $Router->get_int('pg');
        $result_orders = $MysqlDb->select("wc_orders", "*", $where, "printed ASC, qb_ref_number DESC",
            (($pg - 1) * $results_on_page) . ", " . $results_on_page);

        $orders = [];
        while ($row_o = $MysqlDb->get_result($result_orders)) {
            $address = json_decode($row_o['shipping_address'], true);
            $customer = $address['firstName'] ?? "";
            $customer .= isset($address['lastName']) ? " " . $address['lastName'] : "";
            $orders[] = [
                'id' => $row_o['id'],
                'source_id' => $row_o['source_id'],
                'qb_ref_number' => $row_o['qb_ref_number'],
                'order_id' => $row_o['order_id'],
                'qb_status' => $row_o['qb_status'],
                'printed' => (int)$row_o['printed'],
                'cancelled' => $row_o['cancelled'],
                'customer' => $customer,
                'order_create_time' => date("m/d/Y H:i:s", date_to_unix($row_o['order_create_time'], "Y-m-d H:i:s")),
            ];
        }

        $result_count = $MysqlDb->count("wc_orders", $where);
        $Html->set_pager_info(
            [
                'orders' => [
                    'result_count' => $result_count,
                    'items_on_page' => $results_on_page
                ]
            ]);

        $qb_statuses = $MysqlDb->get_list("wc_order_statuses");
        $qb_statuses[''] = "All";
        $qb_statuses[0] = "Ready to Transfer";
        ksort($qb_statuses);

        $Html->content_data = [
            'stores' => $MysqlDb->get_rows("wc_sources", "*", "print_orders = 1", "sort ASC"),
            'orders' => $orders,
            'qb_statuses' => $qb_statuses,
            'split_page' => $Html->pager("orders", $pg,
                "/" . $Router->module . "/orders/?source_id=" . $Router->get_int('source_id')),
        ];
        parent::_html();
    }
}
