<?php

namespace controller\wc;

class updatePrice extends \Controller {
    private $update_price_methods = [
        //1 => ['controller' => "pex", 'method' => "updateStock"],
        2 => ['controller' => "ebay", 'method' => "updatePriceEbay"],
        3 => ['controller' => "ebay", 'method' => "updatePriceEbay315"],
        //4 => ['controller' => "amazon", 'method' => "updateItemQtyAmazon"],
        //5 => ['controller' => "amazon", 'method' => "updateItemQtyAmazon315"],
        //6 => ['controller' => "opencart", 'method' => "syncStoreNW"],
        //7 => ['controller' => "opencart", 'method' => "syncStoreCanarsee"],
    ];

    function __construct() {
        global $Acl;

        $Acl->permission_redirect("change_price");
    }

    function preview() {
        global $Form, $MysqlDb, $Router;

        $file_pref = "import_prices_" . time();
        $upload_folder = 'uploads/temp';

        // Check the file extension
        if (!isset($_FILES['import_file']) || $Form->file_extention($_FILES['import_file']['name']) !== "csv") {
            echo request_callback([
                'status' => "error",
                'errors' => ["The import file must be in csv format"]
            ]);
            exit;
        }

        // check source_id
        if ($Router->post_int('source_id') == 0) {
            echo request_callback([
                'status' => "error",
                'errors' => ["source_id" => "Choose the Store"],
            ]);
            exit;
        }

        $file_name = $Form->file_upload('import_file', $file_pref, $upload_folder);

        $out_data = [];
        $part_nums = [];
        if (($handle = fopen($upload_folder . "/" . $file_name, "r")) !== false) {
            $n = 0;
            while (($data = fgetcsv($handle, 100, ",")) !== false) {
                $n++;
                $data = array_map("trim", $data);
                // Check the titles
                if ($n == 1) {
                    if (strtolower(trim($data[0])) !== "part #" || strtolower(trim($data[1])) !== "price") {
                        echo request_callback([
                            'status' => "error",
                            'errors' => ["The fields names must be: part #, price"]
                        ]);
                        exit;
                    }

                    continue;
                }

                if (trim($data[0]) !== "" && trim($data[1]) !== "") {
                    $MysqlDb->prepare_vars['sku'] = $data[0];
                    $out_data[] = [
                        'part_num' => $data[0],
                        'part_num_hash' => md5($data[0]),
                        'price' => $data[1],
                        'exists' => $MysqlDb->record_exist("wc_items",
                            "source_id = " . $Router->post_int('source_id') . " AND source_sku = '{{sku}}'"),
                    ];
                }
            }
            fclose($handle);

            $_SESSION['update_price_products'][$Router->post('upload_hash')] = $out_data;

            @unlink($upload_folder . "/" . $file_name);
        }

        echo request_callback([
            'status' => "ok",
            'callback_parameters' => $out_data
        ]);
        exit;
    }

    function start_sync() {
        global $Router, $MysqlDb;

        $upload_hash = $Router->post('upload_hash');

        if (isset($_SESSION['update_price_products']) && isset($_SESSION['update_price_products'][$upload_hash])) {
            $upload_id = $MysqlDb->max_value("wc_price_change", "upload_id") + 1;
            $products = [];
            $source_id = $Router->post_int('source_id');
            foreach ($_SESSION['update_price_products'][$upload_hash] as $p) {
                $MysqlDb->prepare_vars['sku'] = $p['part_num'];
                $item_info = $MysqlDb->get_first_row("wc_items", "item_source_id",
                    "source_id = " . $source_id . " AND source_sku = '{{sku}}'");

                $insert_fields = [
                    'upload_id' => $upload_id,
                    'source_id' => $source_id,
                    'item_source_id' => $item_info['item_source_id'],
                    'sku' => $p['part_num'],
                    'price' => $p['price'],
                    'add_time' => current_time()
                ];
                $MysqlDb->insert("wc_price_change", $insert_fields);

                $MysqlDb->prepare_vars['sku'] = $p['part_num'];
                $item_info = $MysqlDb->get_first_row("wc_items", "*",
                    "source_id = " . $source_id . " AND source_sku = '{{sku}}'");
                $products[] = [
                    'upload_id' => $upload_id,
                    'sku' => $p['part_num'],
                    'price' => $p['price'],
                    'item_source_id' => $item_info['item_source_id'],
                    'id' => $item_info['id']
                ];
            }

            // sync to ebay
            if (isset($this->update_price_methods[$source_id])) {
                $stock_methods = $this->update_price_methods[$source_id];
                $Cron = new \model\cron\Cron();
                $_SERVER['HTTP_CHECKSUM'] = $Cron->gen_checksum();
                $class_path = "\\controller\\cron\\" . $stock_methods['controller'];
                $Store = new $class_path();

                $Store->{$stock_methods['method']}($products);
            }

            echo request_callback([
                'status' => "ok",
                'upload_id' => $upload_id
            ]);
        }
        else {
            echo request_callback([
                'status' => "error",
                'errors' => ['Nothing to sync']
            ]);
        }
    }

    function get_sync_status() {
        global $Router, $MysqlDb;

        $synced_result = $MysqlDb->select("wc_price_change", "*",
            "upload_id = " . $Router->get_int('upload_id') . " AND sync_status != 0");
        $unfinished = $MysqlDb->record_exist("wc_price_change",
            "upload_id = " . $Router->get_int('upload_id') . " AND sync_status = 0");

        $synced_items = [];
        while ($row = $MysqlDb->get_result($synced_result)) {
            $response = json_decode($row['response'], true);
            $synced_items[] = [
                'sku' => $row['sku'],
                'sku_hash' => md5($row['sku']),
                'status' => $row['sync_status'],
                'response' => isset($response['LongMessage']) ? $response['LongMessage'] : "",
            ];
        }

        echo request_callback([
            'status' => $unfinished > 0 ? "in_progress" : "done",
            'part_numbers' => $synced_items
        ]);
    }

    function get_products() {
        global $Router, $Html, $MysqlDb;

        $last_upload_id = $MysqlDb->get_first_row("wc_price_change ", "upload_id",
            "source_id = " . $Router->get_int('source_id'));

        $Html->content_data = [
            'ajax_load' => 1,
            'products' => $MysqlDb->get_rows("wc_price_change ", "*", "upload_id = " . (int)$last_upload_id)
        ];

        $Router->page = "updatePrice";
        parent::_html();
    }

    function _html() {
        global $Router, $Html, $MysqlDb;

        $Html->content_data = [
            'ajax_load' => $Router->get_int('ajax_load'),
            'upload_id' => "",
            'products' => $MysqlDb->get_rows("wc_bulk_update_prices", "*",
                "upload_id = " . $Router->get_int('upload_id')),
        ];

        parent::_html();
    }
}
