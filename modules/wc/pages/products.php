<?php

namespace controller\wc;

use \model\wc\Qb;

class products extends \Controller {
    private $update_stock_methods = [
        1 => ['controller' => "pex", 'method' => "updateStock"],
        2 => ['controller' => "ebay", 'method' => "updateItemQtyEbay"],
        3 => ['controller' => "ebay", 'method' => "updateItemQtyEbay315"],
        4 => ['controller' => "amazon", 'method' => "updateItemQtyAmazon"],
        5 => ['controller' => "amazon", 'method' => "updateItemQtyAmazon315"],
        6 => ['controller' => "opencart", 'method' => "syncStoreNW"],
        7 => ['controller' => "opencart", 'method' => "syncStoreCanarsee"],
    ];

    function __construct() {
        global $Acl;

        $Acl->permission_redirect("products_view" );
    }

    function add_map_product() {
        global $Router, $MysqlDb;

        if (!$Router->post('sku') || !$Router->post('qty') || $Router->post('price_rate') === false) {
            echo request_callback([
                'status' => "error",
                'errors' => _lang("fill_required_fields"),
            ]);
            exit;
        }

        $MysqlDb->prepare_vars['sku'] = $Router->post('sku');
        $qb_info = $MysqlDb->get_first_row("quickbooks_item", "list_id", "name = '{{sku}}'");

        if (!$qb_info) {
            echo request_callback([
                'status' => "error",
                'errors' => "SKU was not found",
            ]);
            exit;
        }

        $MysqlDb->prepare_vars['map_sku'] = $Router->post('map_sku');
        $map_info = $MysqlDb->get_first_row("wc_map", "id", "sku = '{{map_sku}}'");

        if (!$map_info) {
            $map_info['id'] = $MysqlDb->insert("wc_map", ['sku' => $Router->post('map_sku')]);
        }

        if ($MysqlDb->record_exist("wc_map_items",
            "map_id = " . (int)$map_info['id'] . " AND qb_list_id = '" . $qb_info['list_id'] . "' AND id != " . $Router->post_int('edit_id'))) {
            echo request_callback([
                'status' => "error",
                'errors' => "SKU already exist",
            ]);
            exit;
        }

        $insert_fields = [
            'map_id' => $map_info['id'],
            'qb_list_id' => $qb_info['list_id'],
            'qty' => $Router->post('qty'),
            'price_rate' => $Router->post('price_rate')
        ];

        if ($Router->post_int('edit_id') == 0) {
            $insert_fields['add_time'] = current_time();

            $MysqlDb->insert("wc_map_items", $insert_fields);
        }
        else {
            $insert_fields['update_time'] = current_time();

            $MysqlDb->update("wc_map_items", $insert_fields, "id = " . $Router->post_int('edit_id'));
        }

        echo request_callback([
            'status' => "ok_message",
        ]);
        exit;
    }

    function delete_map_product() {
        global $MysqlDb, $Router;

        $MysqlDb->delete("wc_map_items", "id = " . $Router->get_int('edit_id'));

        if (!$MysqlDb->record_exist("wc_map_items", "map_id = " . $Router->get_int('map_id'))) {
            $MysqlDb->delete("wc_map", "id = " . $Router->get_int('map_id'));
        }

        echo "ok";
        exit;
    }

    function map_products() {
        global $Router, $Html, $MysqlDb;

        $MysqlDb->prepare_vars['sku'] = $Router->get('sku');
        $map_info = $MysqlDb->get_first_row("wc_map", "id", "sku = '{{sku}}'");

        $result_map_p = $MysqlDb->select("wc_map_items", "*", "map_id = " . (int)$map_info['id']);
        $map_products = [];
        while ($row = $MysqlDb->get_result($result_map_p)) {
            $qb_item = $MysqlDb->get_first_row("quickbooks_item", "name", "list_id = '" . $row['qb_list_id'] . "'");
            $map_products[] = [
                'id' => $row['id'],
                'sku' => $qb_item['name'],
                'qty' => $row['qty'],
                'price_rate' => $row['price_rate']
            ];
        }

        $Html->content_data = [
            'map_id' => $map_info['id'],
            'sku' => $Router->get('sku'),
            'map_products' => $map_products
        ];

        $Router->page = "map_products";
        parent::_html();
    }

    function product_details() {
        global $Router, $MysqlDb, $Html;

        $Qb = new Qb();

        $product_info = $MysqlDb->get_first_row("wc_items", "*", "id = " . $Router->get_int('item_id'));

        $MysqlDb->prepare_vars['sku'] = $product_info['sku'];
        $qb_info = $MysqlDb->get_first_row("quickbooks_item", "*", "name = '{{sku}}'");

        if (!$qb_info) {
            $wc_items = $Qb->get_mapped_items([['sku' => $product_info['sku'], 'qty' => 1, 'price' => 1]]);
            if (count($wc_items) == 1) {
                $MysqlDb->prepare_vars['sku'] = $wc_items[0]['sku'];
                $qb_info = $MysqlDb->get_first_row("quickbooks_item", "*", "name = '{{sku}}'");
            }
        }

        $Html->content_data = [
            'sources' => $MysqlDb->get_list("wc_sources"),
            'qb_info' => $qb_info,
            'product_info' => $product_info,
        ];

        $Router->page = "product_details";
        parent::_html();
    }

    function sync_product_now() {
        global $Router, $MysqlDb;

        $Qb = new Qb();

        if (isset($this->update_stock_methods[$Router->get_int('store_id')])) {
            $stock_methods = $this->update_stock_methods[$Router->get_int('store_id')];
            $Cron = new \model\cron\Cron();
            $_SERVER['HTTP_CHECKSUM'] = $Cron->gen_checksum();
            $class_path = "\\controller\\cron\\" . $stock_methods['controller'];
            $Store = new $class_path();

            // update wc_items quantity
            $MysqlDb->prepare_vars['item_sku'] = $Router->get('sku');
            $item = $MysqlDb->get_first_row("quickbooks_item", "*", "name = '{{item_sku}}'");
            $wc_items = $Qb->get_mapped_items_qty($item);
            foreach ($wc_items as $sku => $qty) {
                $MysqlDb->prepare_vars['sku'] = $sku;
                if ($MysqlDb->record_exist("wc_items", "sku = '{{sku}}' AND store_qty != ".(float)$qty)) {
                    $MysqlDb->update("wc_items", ['store_qty' => $qty, 'synced' => 0, 'qb_sync_time' => current_time()], "sku = '{{sku}}'");
                }
            }

            return $Store->{$stock_methods['method']}($Router->get('sku'));
        }
    }

    function _html() {
        global $Router, $Html, $MysqlDb;

        $results_on_page = 20;
        $where = 1;
        // Search
        if ($Router->get_int('source_id') !== 0) {
            $where .= " AND source_id = " . $Router->get_int('source_id');
        }
        if ($Router->get('sku')) {
            $MysqlDb->prepare_vars['sku'] = $Router->get('sku');

            $where .= " AND sku = '{{sku}}'";
        }
        if ($Router->get('source_sku')) {
            $MysqlDb->prepare_vars['source_sku'] = $Router->get('source_sku');

            $where .= " AND source_sku = '{{source_sku}}'";
        }

        $pg = $Router->get_int('pg') < 1 ? 1 : $Router->get_int('pg');
        $result = $MysqlDb->select("wc_items", "*", $where, "id DESC",
            (($pg - 1) * $results_on_page) . ", " . $results_on_page);

        $products = [];
        $n = 0;
        while ($row = $MysqlDb->get_result($result)) {
            $products[$n] = $row;

            $MysqlDb->prepare_vars['sku'] = $row['sku'];
            $products[$n]['has_map'] = $MysqlDb->record_exist("wc_map", "sku = '{{sku}}'");

            $n++;
        }

        $result_count = $MysqlDb->count("wc_items", $where);
        $Html->set_pager_info(
            [
                'orders' => [
                    'result_count' => $result_count,
                    'items_on_page' => $results_on_page
                ]
            ]);

        $Html->content_data = [
            'sources' => $MysqlDb->get_list("wc_sources"),
            'products' => $products,
            'split_page' => $Html->pager("orders", $pg),
        ];

        parent::_html();
    }
}
