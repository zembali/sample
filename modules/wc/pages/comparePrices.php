<?php

namespace controller\wc;

class comparePrices extends \Controller
{
    function compare()
    {
        global $Html, $MysqlDb, $Router;

        $sources = $MysqlDb->get_list("wc_sources");
        $s1_id = $Router->get_int('source_1');
        $s2_id = $Router->get_int('source_2');
        $price_diff = $Router->get_int('price_diff');

        $result = $MysqlDb->select("wc_items", "*", "source_id = ".$s1_id." AND active = 1");

        $products = [];
        while ($s1 = $MysqlDb->get_result($result)) {
            $MysqlDb->prepare_vars['sku'] = $s1['sku'];
            $s2 = $MysqlDb->get_first_row("wc_items", "*", "source_id = ".$s2_id." AND sku = '{{sku}}' AND mqty = ".(float)$s1['mqty']." AND active = 1");
            // if there is no product with the same mqty, get check the product with diff mqty
            if(!$s2){
                $s2 = $MysqlDb->get_first_row("wc_items", "*", "source_id = ".$s2_id." AND sku = '{{sku}}' AND active = 1");
            }
            if($s2) {
                $s2['mqty'] = (float)$s2['mqty'] == 0 ? 1 : $s2['mqty'];
                $s2['price'] = ($s2['price'] / $s2['mqty']) * $s1['mqty'];

                if (round($s1['price'], 2) < round($s2['price'] + $s2['price'] * $price_diff / 100, 2)) {
                    $products[] = [
                        'sku' => $s1['source_sku'],
                        'mqty' => $s1['mqty'] = (float)$s1['mqty'] == 0 ? 1 : $s1['mqty'],
                        's1_price' => $s1['price'],
                        's2_price' => $s2['price'],
                    ];
                }
            }
        }

        $Html->content_data = [
            'ajax' => 1,
            'result_amount' => count($products),
            'source_1' => $sources[$s1_id],
            'source_2' => $sources[$s2_id],
            'products' => $products,
            'price_diff' => $price_diff
        ];

        if ($Router->get_int('export')) {
            header("Content-Type: application/csv-tab-delimited-table");
            header("Content-disposition: filename=compare-prices.csv");

            $Router->page = "comparePricesExport";
            parent::_html();
        }
        else {
            $Router->page = "comparePrices";
            parent::_html();
        }
    }

    function _html()
    {
        global $Html, $MysqlDb;

        $Html->content_data = [
            'ajax' => 0,
            'sources' => $MysqlDb->get_list("wc_sources")
        ];

        parent::_html();
    }
}
