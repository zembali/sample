<?php

namespace controller\wc;

use \model\wc\Qb;

class services extends \Controller {

    function __construct() {
        global $Acl, $Html;

    }

    function storeUpdates() {
        global $Router, $MysqlDb;

        if(!validate_date($Router->get('from'), 'Y-m-d H:i:s')) {
            echo "time error";
        }
        else{
            $MysqlDb->prepare_vars['time_from'] = $Router->get('from');
            $items = $MysqlDb->get_rows("quickbooks_item",
                        "
                        ListID, 
                        TimeModified, 
                        `Name` AS PartNumber, 
                        QuantityOnHand,
                        QuantityOnSalesOrder,
                        shelf_id,
                        shelf_id_extra,
                        weight,
                        weight_unit,
                        length,
                        width,
                        height
                        ",
                        "`Type` = 'Inventory' AND TimeModified >= '{{time_from}}'"
            );

            return json_encode($items, JSON_UNESCAPED_UNICODE);
        }

    }


    function _html() {
        global $Router, $Html, $MysqlDb, $UserInfo;


        parent::_html();
    }
}
