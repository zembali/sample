<div class="modal-header">
    <h4 class="modal-title">SKU: <?= $product_info['sku'] ?></h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <table class="table">
        <tr>
            <td style="width: 30%">Source</td>
            <td style="width: 70%"><?= $sources[$product_info['source_id']] ?></td>
        </tr>
        <tr>
            <td>Source ID</td>
            <td><?= $product_info['item_source_id'] ?></td>
        </tr>
        <tr>
            <td>SKU</td>
            <td><?= $product_info['sku'] ?></td>
        </tr>
        <tr>
            <td>Source SKU</td>
            <td><?= $product_info['source_sku'] ?></td>
        </tr>
        <tr>
            <td>Title</td>
            <td><?= $product_info['title'] ?></td>
        </tr>
        <tr>
            <td>Last Sync Time</td>
            <td><?= $product_info['sync_time'] ?></td>
        </tr>
        <? if ($qb_info): ?>
            <tr>
                <td>QB List Id</td>
                <td><?= $qb_info['list_id'] ?></td>
            </tr>
            <tr>
                <td>QB Name</td>
                <td><?= $qb_info['name'] ?></td>
            </tr>
            <tr>
                <td>Quantity On Hand</td>
                <td><?= $qb_info['quantity_on_hand'] ?></td>
            </tr>
            <tr>
                <td>Quantity On Order</td>
                <td><?= $qb_info['quantity_on_order'] ?></td>
            </tr>
            <tr>
                <td>Quantity On Sales Order</td>
                <td><?= $qb_info['quantity_on_sales_order'] ?></td>
            </tr>
        <? endif; ?>
        <tr>
            <td>
                <button id="sync_now" class="btn btn-primary btn-sm">
                    Sync now <i class="fa fa-spinner fa-spin" style="display: none"></i>
                </button>
            </td>
            <td>
                <span id="ok" class="alert alert-success" style="display: none">Done!</span>
                <span id="error" class="alert alert-danger" style="display: none">Unknown Error</span>
            </td>
        </tr>
    </table>
</div>

<script>
    $("#sync_now").click(function () {
        $(this).attr('disabled', true);
        $(this).children(".fa-spinner").show();

        $.get('/post/<?= $module ?>/products/sync_product_now', {
            store_id: '<?= $product_info['source_id'] ?>',
            sku: '<?= $product_info['sku'] ?>'
        })
            .done(function (result) {
                if (result == "Success") {
                    $("#error").hide();
                    $("#ok").show();
                } else {
                    $("#error").show();
                    $("#ok").hide();
                }

                $("#sync_now").attr('disabled', false);
                $("#sync_now").children(".fa-spinner").hide();
            })
    })
</script>
