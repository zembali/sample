<?= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" ?>
<?= "<?qbxml version=\"13.0\"?>\n" ?>
<QBXML>
    <QBXMLMsgsRq onError="stopOnError">
        <? foreach ($orders as $o): ?>
        <InvoiceAddRq>
            <InvoiceAdd>
                <CustomerRef>
                    <FullName><?= $o['customer'] ?></FullName>
                </CustomerRef>
                <TemplateRef>
                    <FullName><?= $o['invoice_template'] ?></FullName>
                </TemplateRef>
                <TxnDate><?= $o['shipped_time'] ?></TxnDate>
                <DueDate><?= $o['shipped_time'] ?></DueDate>
                <ShipDate><?= $o['order_create_time'] ?></ShipDate>
                <LinkToTxnID><?= $o['qb_txn_id'] ?></LinkToTxnID>
            </InvoiceAdd>
        </InvoiceAddRq>
        <? endforeach; ?>
    </QBXMLMsgsRq>
</QBXML>
