<div class="modal-header">
    <h4 class="modal-title">Edit Order</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <form id="save_order_from" method="post" action="/post/<?= $module ?>/orders/save_order/?order_id=<?= $order['id'] ?>" onsubmit="return false">
        <table class="table">
            <tr>
                <td style="width: 150px">Source</td>
                <td><?= $sources[$order['source_id']] ?></td>
            </tr>
            <tr>
                <td>Order #</td>
                <td><?= $order['qb_ref_number'] ?></td>
            </tr>
            <tr>
                <td>Source Order #</td>
                <td><?= $order['order_id'] ?></td>
            </tr>
            <tr>
                <td>Customer</td>
                <td><?= $order['customer'] ?></td>
            </tr>
            <tr>
                <td>Created At</td>
                <td><?= $order['order_create_time'] ?></td>
            </tr>
            <tr>
                <td>Status</td>
                <td>
                    <?= $Form->input_form("qb_status", "select", $order['qb_status'], $order_statuses, "","form-control") ?>
                </td>
            </tr>
            <tr>
                <td>Completed</td>
                <td>
                    <?= $Form->input_form("completed", "radio", $order['completed'], [0 => "No", 1 => "Yes"], "", "ml-2") ?>
                </td>
            </tr>
            <tr>
                <td>Shipped</td>
                <td>
                    <?= $Form->input_form("shipped", "radio", $order['shipped'], [0 => "No", 1 => "Yes"], "","ml-2") ?>
                </td>
            </tr>
            <tr>
                <td>Cancelled</td>
                <td>
                    <?= $Form->input_form("cancelled", "radio", $order['cancelled'], [0 => "No", 1 => "Yes"], "", "ml-2") ?>
                </td>
            </tr>
            <tr>
                <td>Order Products</td>
                <td>
                    <table class="table table-borderless">
                        <? foreach (json_decode($order['order_items'], true) as $i): ?>
                            <tr>
                                <td style="width: 30%; padding-right: 0">
                                    <?= $Form->input_form("item_" . md5($i['sku']), "text", $i['sku'], "", "", "form-control") ?>
                                </td>
                                <td style="padding-left: 2px">x<?= $i['qty'] ?></td>
                                <td><?= $i['title'] ?></td>
                            </tr>
                        <? endforeach; ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td>QB Products</td>
                <td>
                        <? foreach (json_decode($order['qb_items'], true) as $qbi): ?>
                            <div>
                                <?= $qbi['sku'] ?> x <?= $qbi['qty'] ?>
                            </div>
                        <? endforeach; ?>
                </td>
            </tr>
            <? if(json_decode($order['shipping_address'])): ?>
                <tr>
                    <td>Shipping Address</td>
                    <td>
                        <table class="table table-borderless">
                            <? foreach (json_decode($order['shipping_address'], true) as $key => $value): ?>
                                <tr>
                                    <td style="width: 30%; padding-right: 0">
                                        <?= $key ?>
                                    </td>
                                    <td style="padding-left: 2px">
                                        <?= $Form->input_form("sh_addr_" . $key, "text", $value, "", "", "form-control") ?>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        </table>
                    </td>
                </tr>
            <? endif ?>
            <? if(json_decode($order['billing_address'])): ?>
                <tr>
                    <td>Billing Address</td>
                    <td>
                        <table class="table table-borderless">
                            <? foreach (json_decode($order['billing_address'], true) as $key => $value): ?>
                                <tr>
                                    <td style="width: 30%; padding-right: 0">
                                        <?= $key ?>
                                    </td>
                                    <td style="padding-left: 2px">
                                        <?= $Form->input_form("b_addr_" . $key, "text", $value, "", "", "form-control") ?>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        </table>
                    </td>
                </tr>
            <? endif ?>
            <tr>
                <td>
                    <button id="save_order" class="btn btn-primary">Save</button>
                </td>
            </tr>
        </table>
    </form>
</div>

<script>
    $("#save_order_from").submit(function () {
        post_call(this, function (data) {
            if (data == "success") {
                $("#qb_status_<?= $order['id'] ?>").html($("#qb_status option:selected" ).text());

                $('#popup_lg').modal('toggle');
            }
        })
    })
</script>
