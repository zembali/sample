<?= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" ?>
<?= "<?qbxml version=\"13.0\"?>\n" ?>
<QBXML>
    <QBXMLMsgsRq onError="stopOnError">
        <? foreach ($orders as $o): ?>
            <SalesOrderAddRq>
                <SalesOrderAdd>
                    <CustomerRef>
                        <FullName><?= $o['customer'] ?></FullName>
                    </CustomerRef>
                    <TemplateRef>
                        <FullName><?= $o['sales_template'] ?></FullName>
                    </TemplateRef>
                    <TxnDate><?= $o['add_date'] ?></TxnDate>
                    <RefNumber><?= $o['order_id'] ?></RefNumber>
                    <BillAddress>
                        <? foreach ($o['billing_address'] as $index => $addr): ?>
                            <?= "<Addr" . ($index + 1) . ">" . $addr . "</Addr" . ($index + 1) . ">\n"; ?>
                        <? endforeach; ?>
                    </BillAddress>
                    <ShipAddress>
                        <? foreach ($o['shipping_address'] as $index => $addr): ?>
                            <?= "<Addr" . ($index + 1) . ">" . $addr . "</Addr" . ($index + 1) . ">\n"; ?>
                        <? endforeach; ?>
                    </ShipAddress>
                    <Memo><?= trim($o['memo']['state'] . ", " . $o['memo']['zip'] . " " . htmlentities(isset($o['memo']['firstName']) ? $o['memo']['firstName'] . " " . ($o['memo']['lastName'] ?? "") : "")) ?></Memo>
                    <? foreach ($o['order_items'] as $item): ?>
                        <SalesOrderLineAdd>
                            <ItemRef>
                                <FullName><?= $item['sku'] ?></FullName>
                            </ItemRef>
                            <Quantity><?= number_format($item['qty'], 3, '.', '') ?></Quantity>
                            <Amount><?= number_format($item['price'] * $item['qty'], 2, '.', '') ?></Amount>
                            <SalesTaxCodeRef>
                                <FullName>Non</FullName>
                            </SalesTaxCodeRef>
                        </SalesOrderLineAdd>
                    <? endforeach; ?>
                    <? if((float)$o['tax']['rate'] > 0): ?>
                        <SalesOrderLineAdd>
                            <ItemRef>
                                <FullName>Sales Tax</FullName>
                            </ItemRef>
                            <Desc>Tax <?= number_format($o['tax']['rate'], 3, '.', '') . "% " . $o['memo']['state'] ?></Desc>
                            <Quantity>1.000</Quantity>
                            <Amount><?= number_format($o['tax']['amount'], 2, '.', '') ?></Amount>
                            <SalesTaxCodeRef>
                                <FullName>Non</FullName>
                            </SalesTaxCodeRef>
                        </SalesOrderLineAdd>
                    <? endif; ?>
                    <? if((float)$o['shipping_price'] > 0): ?>
                        <SalesOrderLineAdd>
                            <ItemRef>
                                <FullName>S&amp;H</FullName>
                            </ItemRef>
                            <Desc>Shipping and handling</Desc>
                            <Quantity>1.000</Quantity>
                            <Amount><?= $o['shipping_price'] ?></Amount>
                            <SalesTaxCodeRef>
                                <FullName>Non</FullName>
                            </SalesTaxCodeRef>
                        </SalesOrderLineAdd>
                    <? elseif((float)$o['shipping_price'] < 0): ?>
                        <SalesOrderLineAdd>
                            <ItemRef>
                                <FullName>Discount</FullName>
                            </ItemRef>
                            <Desc>Pickup Discount</Desc>
                            <Quantity>1.000</Quantity>
                            <Amount><?= number_format($o['shipping_price'], 2, '.', '') ?></Amount>
                            <SalesTaxCodeRef>
                                <FullName>Non</FullName>
                            </SalesTaxCodeRef>
                        </SalesOrderLineAdd>
                    <? endif; ?>
                    <? if((float)$o['discount'] < 0): ?>
                        <SalesOrderLineAdd>
                            <ItemRef>
                                <FullName>Discount</FullName>
                            </ItemRef>
                            <Desc>Discount</Desc>
                            <Quantity>1.000</Quantity>
                            <Amount><?= number_format($o['discount'], 2, '.', '') ?></Amount>
                            <SalesTaxCodeRef>
                                <FullName>Non</FullName>
                            </SalesTaxCodeRef>
                        </SalesOrderLineAdd>
                    <? endif; ?>
                    </SalesOrderAdd>
            </SalesOrderAddRq>
        <? endforeach; ?>
    </QBXMLMsgsRq>
</QBXML>
