<?php if ($ajax_load == 0): ?>
    <div class="card">
        <div class="card-header">
            <h5>Change ebay prices</h5>
        </div>
        <div class="card-body">
            <form id="upload_form" action="/post/<?= $module ?>/updatePrice/preview" class="form-inline" method="post" enctype="multipart/form-data" style="display: block">
                <div class="clearfix"></div>
                <div class="form-group">
                    <?= $Form->input_form("upload_id", "hidden", $upload_id) ?>
                    <input type="file" name="import_file" class="form-control">
                    <?= $Form->input_form("source_id", "select", "", [2 => "Ebay", 3 => "Ebay315"], "width: 100px", "form-control") ?>
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </form>
            <div class="clearfix"></div>
            <div id="products">
<? endif; ?>
                <h5 class="card-title mt-4">Products</h5>
                <table id="product_table" class="table w-50">
                    <tr>
                        <th>Part #</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    <? foreach ($products as $p): ?>
                        <tr>
                            <td><?= $p['part_num'] ?></td>
                            <td><?= $p['price'] ?></td>
                            <td>
                                <i class="fa <?= (int)$p['synced'] == 0 ? "fa-spinner fa-spin" : "fa-check text-success" ?>"></i>
                            </td>
                            <td>
                                <button class="btn btn-primary btn-sm" id="submit" style="display: <?= (int)$p['synced'] == 0 ? "block" : "none" ?>">Sync</button>
                            </td>
                        </tr>
                    <? endforeach; ?>
                </table>

                <button id="start_sync" type="button" class="btn btn-primary mt-2" style="display: none">Start Sync</button>
                <button id="working" type="button" class="btn btn-info mt-2" disabled style="display: none">
                    <i class="fas fa-spinner fa-spin"></i> Working...
                </button>
                <a href="" id="done" style="display: none" class="btn btn-outline-success mt-2">Done!</a>
<?php if ($ajax_load == 0): ?>
            </div>
        </div>
    </div>
<? endif; ?>

<script>
    let intervalId = 0;

    function loadProducts(data) {
        let errors = 0;

        $("#product_table").empty();
        if(typeof data !== "undefined" && data !== "error") {
            $("#product_table").append("<tr>" +
                "<th>Part #</th>" +
                "<th>Price</th>" +
                "<th class='text-center'>Status</th>" +
                "<th></th>"
            );

            for (i = 0; i < data.length; i++) {
                $("#product_table").append("<tr>" +
                    "<td>" + data[i].part_num + "</td>" +
                    "<td>" + data[i].price + "</td>" +
                    "<td class='text-center' id='status_" + data[i].part_num_hash + "'>" +
                        (data[i].exists ? "<span class='text-info'>Ready</span>" : "<span class='text-danger'>N/A</span>") +
                    "</td>" +
                    "<td></td>"
                );

                errors = errors == 1 || !data[i].exists ? 1 : 0;
            }

            if(errors == 0) {
                $("#start_sync").show();
            }
        }
    }

    // upload file
    $('#upload_form').ajaxForm({
        beforeSend: function () {
            $("#submit").attr("disabled", true);
            $("#start_sync").show();
            $("#done").hide();
            $("#working").hide();

            return false;
        },
        success: function (data) {
            post_action(data, $('#upload_form'), "loadProducts");
        },
        complete: function (xhr) {
            $("#submit").attr("disabled", false);
        }
    });

    // check sync progress
    function checkSyncProgress() {
        $.get('/post/<?= $module ?>/updatePrice/get_sync_status/', {'upload_id': $("#upload_id").val()})
            .done(function (result) {
                let data = JSON.parse(result);

                for (var index in data.part_numbers) {
                    //console.log(index + ' - ' + data.part_numbers[index]);
                    let item = data.part_numbers[index];
                    if (item.status == 1) {
                        $("#status_" + item.sku_hash).html('<i class="far fa-check-circle text-success"></i>');
                    } else if (item.status == 2) {
                        $("#status_" + item.sku_hash).html(
                            '<i class="fas fa-exclamation-triangle text-warning" ' +
                            '     data-trigger="hover" ' +
                            '     data-toggle="popover" ' +
                            '     data-html="true"' +
                            '     data-content="' + item.response + '"></i>'
                        );
                    }
                }

                $(document).ready(function () {
                    $('[data-toggle="popover"]').popover({html: true});
                });

                if (data.status == "done") {
                    clearInterval(intervalId);

                    $("#start_sync").hide();
                    $("#done").show();
                    $("#working").hide();
                }
            })
    }

    // start Sync
    $("#start_sync").click(function () {
        $("#start_sync").hide();
        $("#done").hide();
        $("#working").show();

        $.post('/post/<?= $module ?>/updatePrice/start_sync', {
            upload_hash: $("#upload_hash").val(),
            source_id: $("#source_id").val()
        })
            .done(function (result) {
                let data = JSON.parse(result);

                if(data.status == "ok"){
                    $("#upload_id").val(data.upload_id);

                    intervalId = setInterval(checkSyncProgress, 5000);
                }
            })
    })

</script>
