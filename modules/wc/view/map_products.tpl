<div class="modal-header">
    <h4 class="modal-title">SKU: <?= $sku ?></h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <form id="add_map_product_form" method="post" action="/post/<?= $module ?>/products/add_map_product" onsubmit="return false">
        <table class="table">
            <tr>
                <th>Mapped SKU</th>
                <th>Quantity</th>
                <th>Price Rate</th>
                <th colspan="2"></th>
            </tr>
            <? foreach ($map_products as $mp): ?>
                <tr id="row_<?= $mp['id'] ?>">
                    <td id="sku_<?= $mp['id'] ?>">
                        <?= $mp['sku'] ?>
                    </td>
                    <td id="qty_<?= $mp['id'] ?>">
                        <?= (float)$mp['qty'] ?>
                    </td>
                    <td id="price_rate_<?= $mp['id'] ?>">
                        <?= (float)$mp['price_rate'] ?>
                    </td>
                    <td>
                        <i class="fa fa-pen pointer" data-id="<?= $mp['id'] ?>"></i>
                    </td>
                    <td>
                        <i class="fa fa-trash text-danger" data-id="<?= $mp['id'] ?>"></i>
                    </td>
                </tr>
            <? endforeach; ?>
            <tr>
                <th>
                    <input type="text" name="sku" id="sku" class="form-control">
                </th>
                <th>
                    <input type="text" name="qty" id="qty" class="form-control">
                </th>
                <th>
                    <input type="text" name="price_rate" id="price_rate" class="form-control">
                </th>
                <th colspan="2">
                    <input type="hidden" name="map_sku" id="map_sku" value="<?= $Router->get('sku') ?>">
                    <input type="hidden" name="edit_id" id="edit_id">
                    <button type="submit" class="btn btn-success" id="add_new">Save</button>
                </th>
            </tr>
        </table>
    </form>
</div>

<script>
    function reload_page(){
        $.get("/body/<?= $module ?>/products/map_products/?sku=<?= $Router->get('sku') ?>")
        .done(function (data) {
            $("#popup .ajax_modal").html(data);
        })
    }

    $("#popup").on("click", ".fa-pen", function (){
        let edit_id = $(this).data('id');
        $("#edit_id").val(edit_id);
        $("#sku").val($("#sku_" + edit_id).html().trim());
        $("#qty").val($("#qty_" + edit_id).html().trim());
        $("#price_rate").val($("#price_rate_" + edit_id).html().trim());
    })

    $("#popup").on("click", ".fa-trash", function (){
        if(confirm("Are you sure?")) {
            let edit_id = $(this).data('id');
            $.get("/body/<?= $module ?>/products/delete_map_product/",
                {
                    edit_id: edit_id,
                    map_id: '<?= $map_id ?>'
                })
                .done(function (data) {
                    if (data == "ok") {
                        $("#row_" + edit_id).remove();
                    }
                })
        }
    })

    $("#add_map_product_form").submit(function () {
        post_call(this, function(data){
            if(data !== "error"){
                reload_page();
            }
        })
    })
</script>
