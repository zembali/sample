<? if (!$ajax): ?>
    <div class="modal-header">
        <h4 class="modal-title">Compare Prices</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
<div class="modal-body">
    <form id="compare_prices_form" method="post" action="/post/<?= $module ?>/products/compare" onsubmit="return false">
        <table class="table">
            <tr>
                <th>Source 1</th>
                <th>Source 2</th>
                <th>Price Diff.</th>
                <th></th>
            </tr>
            <tr>
                <td>
                    <?= $Form->input_form("source_1", "select", "", $sources, "", "form-control") ?>
                </td>
                <td>
                    <?= $Form->input_form("source_2", "select", "", $sources, "", "form-control") ?>
                </td>
                <td>
                    <div class="input-group">
                        <?= $Form->input_form("price_diff", "text", "", "", "", "form-control", "autocomplete=\"off\"") ?>
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon1">%</span>
                        </div>
                    </div>
                </td>
                <td>
                    <button type="submit" class="btn btn-primary" id="compare">Compare</button>
                </td>
            </tr>
        </table>
        <? endif; ?>

        <div id="products">
            <? if ($ajax): ?>

                <div class="mt-2">
                    <div class="float-left">Found <?= $result_amount ?> products</div>
                    <div class="float-right text-success pointer" id="export">
                        <i class="fas fa-file-excel"></i> Export
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="mt-2">
                    <table class="table">
                        <tr>
                            <th>SKU (<?= $source_1 ?>)</th>
                            <th>Mqty</th>
                            <th><?= $source_1 ?></th>
                            <th><?= $source_2 ?></th>
                            <th>Price Diff.</th>
                            <th>Price Suggestion</th>
                        </tr>
                        <? foreach ($products as $p): ?>
                            <tr>
                                <td><?= $p['sku'] ?></td>
                                <td><?= $p['mqty'] ?></td>
                                <td>$<?= $p['s1_price'] ?></td>
                                <td>$<?= $p['s2_price'] ?></td>
                                <td><?= round(($p['s1_price'] - $p['s2_price']) * 100 / $p['s1_price'], 2) ?>%</td>
                                <td>$<?= round($p['s2_price'] + ($p['s2_price'] * $price_diff / 100), 2) ?></td>
                            </tr>
                        <? endforeach; ?>
                    </table>
                </div>
            <? endif; ?>
        </div>
        <? if (!$ajax): ?>
    </form>
</div>

    <script>
        $("#compare_prices_form").submit(function () {
            $("#compare").attr('disabled', true);
            $("#products").html('<i class="fa fa-spinner fa-spin"></i> Loading...');

            $.get('/body/<?= $module ?>/comparePrices/compare', {
                'source_1': obj_value('source_1'),
                'source_2': obj_value('source_2'),
                'price_diff': obj_value('price_diff'),
            })
            .done(function (result) {
                $("#compare").attr('disabled', false);

                $("#products").html(result);
            })
        });

        $("#products").on("click", "#export", function () {
            redirect('/body/<?= $module ?>/comparePrices/compare/?export=1' +
                '&source_1=' + obj_value('source_1') +
                '&source_2=' + obj_value('source_2') +
                '&price_diff=' + obj_value('price_diff'));
        });
    </script>
<? endif; ?>
