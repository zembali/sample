<?= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" ?>
<?= "<?qbxml version=\"".$version."\"?>\n" ?>
<QBXML>
    <QBXMLMsgsRq onError="stopOnError">
        <ItemQueryRq <?= $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID ."\""?>>
        <MaxReturned>100</MaxReturned>
        <FromModifiedDate><?= $last ?></FromModifiedDate>
        <OwnerID>0</OwnerID>
        </ItemQueryRq>
    </QBXMLMsgsRq>
</QBXML>
