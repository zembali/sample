<? if($Router->get_int('printed') == 0): ?>
    <style>
        @media print {
            body{
                display: none;
            }
        }
    </style>
<? endif; ?>
<style>

</style>

<div class="text-center noprint m-t-10">
    <button id="mark_printed" class="btn btn-success" onclick="markAsPrinted()">Print</button>
</div>

<div class="container">
<?php foreach ($orders as $o): ?>
    <div style="min-height: 1200px; padding-top: 50px">
        <div class="float-left">
            <img src="<?= $config['parameters']['logo'] ?>" style="height: 30px">
            <div><?= $config['name'] ?></div>
        </div>
        <div class="float-right text-right">
            <h3><strong>Order #: <?= $o['qb_ref_number'] ?></strong></h3>
            <h5>Source order #: <?= $o['order_id'] ?></h5>
        </div>
        <div class="clearfix-20"></div>

        <table class="table table-bordered" style="font-size: 20px">
            <tr>
                <td class="w-50">
                    <strong>Ship to:</strong>
                    <div class="clearfix-10"></div>
                    <? foreach ($o['address'] as $a): ?>
                        <div><?= $a ?></div>
                    <? endforeach; ?>
                </td>
                <td class="w-50 text-right" style="vertical-align: top">
                    <div><h4><strong>Order time: <?= date("m/d/Y H:i:s", date_to_unix($o['order_create_time'], "Y-m-d H:i:s")) ?></strong></h4></div>
                    <div class="mt-4">Print time: <?= $o['print_time'] ? date("m/d/Y H:i:s", date_to_unix($o['print_time'], "Y-m-d H:i:s")) : "--/--/----" ?></div>

                    <? if (count($o['same_customer']) > 0): ?>
                    <div class="m-t-40 float-right p-2" style="border: 1px dashed #000">
                        <strong>Same customer's orders:</strong> <?= implode(",", $o['same_customer']) ?>
                    </div>
                    <? endif; ?>
                </td>
            </tr>
        </table>
        <div class="clearfix-10"></div>

        <table class="table table-bordered" style="font-size: 20px">
            <tr>
                <th>SID</th>
                <th>SKU</th>
                <th>Qty</th>
                <th>Description</th>
                <th>Price/ea</th>
                <th>Cost</th>
            </tr>
            <? $subtotal = 0 ?>
            <? foreach ($o['products'] as $p): ?>
                <tr>
                    <td><?= $Qb->get_shelf_id_by_sku($p['sku']) ?></td>
                    <td><?= $p['sku'] ?></td>
                    <td class="text-center">
                        <?= $p['qty'] ?>
                        <? if (isset($p['item_id']) && $Qb->get_product_package($p['item_id'], $p['title'])): ?>
                            <div class="small" style="opacity: 0.7">(<?= $Qb->get_product_package($p['item_id'], $p['title']) ?>/bag)</div>
                        <? endif; ?>
                    </td>
                    <td>
                        <?= $Qb->fix_hex($p['title']) ?>
                        <div style="opacity: 0.8"><?= isset($p['details']) ? str_replace(";", "; ", $Qb->fix_hex($p['details'])) : "" ?></div>
                    </td>
                    <td><?= $p['price'] ?></td>
                    <td><?= $p['price'] * $p['qty'] ?></td>
                </tr>
                <? $subtotal += $p['price'] * $p['qty'] ?>
            <? endforeach; ?>
            <tr style="border-top: 2px solid gray">
                <td colspan="5" class="text-right pr-5">Subtotal:</td>
                <td class="text-right"><?= $subtotal ?></td>
            </tr>
            <tr>
                <td colspan="5" class="text-right pr-5">Shipping:</td>
                <td class="text-right"><?= $o['shipping_price'] ?></td>
            </tr>
            <tr>
                <td colspan="5" class="text-right pr-5"><h4><strong>Total:</strong></h4></td>
                <td class="text-right">
                    <h4><strong><?= $subtotal + $o['shipping_price'] ?></strong></h4>
                </td>
            </tr>
        </table>

        <div class="clearfix"></div>
    </div>
    <div>
        <table id="labels" border="0" cellspacing="0" cellpadding="0" style="width: 100%; height:94px;" >
            <tr>
                <? for($i = 1; $i <= 3; $i++): ?>
                <td>
                    <table>
                        <tr>
                            <td colspan="2" class="heading" style="width:243px;font-size:25px;line-height:24px;padding-top:5px;padding-left:8px;">
                                <?= $o['address']['firstName']." " ?>
                                <?= $o['address']['lastName'] ?? "" ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:19px;padding-left:8px;padding-top:10px;width:65%;font-weight:bold;">Order #:<?= $o['qb_ref_number'] ?></td>
                            <td style="font-size:19px;padding-left:5px;padding-top:10px;font-weight:bold;">Box # <?= $i ?></td>
                        </tr>
                    </table>
                </td>
                <? endfor; ?>
            </tr>
        </table>
    </div>
    <div class="page-break"></div>
<?php endforeach; ?>
</div>


<script type="text/javascript">
    <? if($Router->get_int('printed') == 1): ?>
        setTimeout(function () {
            window.print();
        }, 1000);
    <? endif; ?>

    function markAsPrinted() {
        // if request is from "Backorder"
        document.getElementById('mark_printed').disabled = true;

        if (location.href.indexOf('&printed=1') == -1) {
            location = location.href + '&printed=1';
        } else {
            location.reload();
        }
    }

</script>
