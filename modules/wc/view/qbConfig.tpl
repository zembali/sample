<a href="/body/<?= $module ?>/qbConfig/add_form" data-toggle="modal" data-target="#popup" class="float-right">
    <h5><i class="fa fa-plus-circle text-success"></i> Add New</h5>
</a>
<div class="clearfix"></div>
<table class="table mt-3">
    <? foreach ($qb_customers as $s_id => $customer): ?>
        <tr>
            <th colspan="5" class="bg-light border-dark">
                <h4><?= $sources[$s_id] ?></h4>
            </th>
        </tr>
        <tr>
            <th>Date from</th>
            <th>Date To</th>
            <th>Customer</th>
            <th colspan="2"></th>
        </tr>
        <? foreach ($customer as $c): ?>
            <tr id="item_<?= $c['id'] ?>">
                <td><?= $c['date_from'] ?></td>
                <td><?= $c['date_to'] ?></td>
                <td><?= $c['customer'] ?></td>
                <td class="text-right">
                    <a href="/body/<?= $module ?>/qbConfig/add_form/?edit_id=<?= $c['id'] ?>" data-toggle="modal" data-target="#popup">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
                <td class="text-right">
                    <i class="fa fa-trash pointer delete_customer"
                       data-id="<?= $c['id'] ?>"
                       data-toggle="confirmation"
                       data-confirmation-event="myevent"></i>
                </td>
            </tr>
        <? endforeach; ?>
        <tr>
            <th colspan="5"></th>
        </tr>
    <? endforeach; ?>
</table>

<script>
    $(".delete_customer").confirmation({
        btnOkClass: "btn btn-sm btn-danger mr-2",
        onConfirm  : function(e){
            let id = $(this).data('id');
            $.get('/post/<?=$module?>/qbConfig/delete_qb_customer/', {id: id})
                .done(function(data){
                    if(data == "ok"){
                        $("#item_" + id).hide();
                    }
                })
        },
    });
</script>
