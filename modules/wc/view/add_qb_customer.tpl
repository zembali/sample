<div class="modal-header">
    <h4 class="modal-title">QB customer</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <form id="add_customer_form" method="post" action="/post/<?= $module ?>/qbConfig/save_customer/?edit_id=<?= $Router->get('edit_id') ?>" onsubmit="return false">
        <table class="table">
            <tr>
                <td>Source</td>
                <td>
                    <?= $Form->input_form("source_id", "select", $edit_value, $sources, "", "form-control") ?>
                </td>
            </tr>
            <tr>
                <td>Date From</td>
                <td>
                    <?= $Form->input_form("date_from", "text", $edit_value, "", "", "form-control") ?>
                </td>
            </tr>
            <tr>
                <td>Date To</td>
                <td>
                    <?= $Form->input_form("date_to", "text", $edit_value, "", "", "form-control") ?>
                </td>
            </tr>
            <tr>
                <td>Customer</td>
                <td>
                    <?= $Form->input_form("customer", "text", $edit_value, "", "", "form-control") ?>
                </td>
            </tr>
        </table>

        <button type="submit" id="add_customer" class="btn btn-primary">Save</button>
    </form>
</div>

<script>
    $("#date_from, #date_to").datepicker({
        dateFormat: 'yy-mm-dd',
    })

    $("#add_customer_form").submit(function () {
        post_call(this, function (data) {
            if (data == "success") {
                reload();
            }
        })
    })
</script>

