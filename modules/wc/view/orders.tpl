<nav class="navbar navbar-expand-sm bg-light navbar-light">
    <ul class="navbar-nav">
        <? foreach ($stores as $s): ?>
            <li class="nav-item <?= (int)$s['id'] == $Router->get_int('source_id') ? "active" : "" ?>">
                <a class="nav-link" href="/<?= $module ?>/orders/?source_id=<?= $s['id'] ?>">
                    <?= $s['name'] ?>
                </a>
            </li>
        <? endforeach; ?>
    </ul>
</nav>

<form id="search" method="get" action="/<?= $module ?>/orders/">
    <input type="hidden" name="source_id" value="<?= $Router->get_int('source_id') ?>">
    <div class="row mt-3 mb-1">
        <div class="col-md-2">
            <input type="text" name="qb_number" placeholder="QB number" class="form-control"
                   autocomplete="off" value="<?= $Router->get('qb_number') ?>">
        </div>
        <div class="col-md-2">
            <input type="text" name="order_id" placeholder="Order ID" class="form-control"
                   autocomplete="off" value="<?= $Router->get('order_id') ?>">
        </div>
        <div class="col-md-2">
            <?= $Form->input_form("qb_status_id", "select", $Router->get_data, $qb_statuses, "", "form-control") ?>
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </div>
</form>

<table class="table table-striped m-t-15">
    <tr>
        <th>
            <a target="_blank" href="/print/<?= $module ?>/orders/print_order/?source_id=<?= $Router->get_int('source_id') ?>">
                <i class="fa fa-print fa-lg text-primary"></i>
            </a>
        </th>
        <th>Order #</th>
        <th>Source Order #</th>
        <th>Status#</th>
        <th>Customer</th>
        <th>Created At</th>
        <? if ($Acl->has_permission("orders_admin")): ?>
            <th></th>
        <? endif; ?>
    </tr>
    <? foreach ($orders as $o): ?>
        <tr>
            <td>
                <a target="_blank" href="/print/<?= $module ?>/orders/print_order/?source_id=<?= $o['source_id'] ?>&id=<?= $o['id'] ?>">
                    <i class="fa fa-print text-<?= (int)$o['cancelled'] == 1 ? "danger" : ((int)$o['printed'] == 1 ? "warning" : "success") ?>"></i>
                </a>
            </td>
            <td><?= $o['qb_ref_number'] ?></td>
            <td><?= $o['order_id'] ?></td>
            <td id="qb_status_<?= $o['id'] ?>">
                <?= isset($qb_statuses[$o['qb_status']]) ? $qb_statuses[$o['qb_status']] : "N/A" ?>
            </td>
            <td><?= $o['customer'] ?></td>
            <td><?= $o['order_create_time'] ?></td>
            <? if ($Acl->has_permission("orders_admin")): ?>
                <td>
                    <a href="/body/<?= $module ?>/orders/order_details/?order_id=<?= $o['id'] ?>" data-toggle="modal" data-target="#popup_lg">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
</table>
<div>
    <?= $split_page ?>
</div>
