<form id="search" method="get" action="/<?= $module ?>/products/">
    <div class="row mt-3 mb-1">
        <div class="col-md-2">
            <?= $Form->input_form("source_id", "select", $Router->get('source_id'), $sources, "", "form-control") ?>
        </div>
        <div class="col-md-2">
            <input type="text" name="sku" placeholder="SKU" class="form-control"
                   autocomplete="off" value="<?= $Router->get('sku') ?>">
        </div>
        <div class="col-md-2">
            <input type="text" name="source_sku" placeholder="Source SKU" class="form-control"
                   autocomplete="off" value="<?= $Router->get('source_sku') ?>">
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
        <div class="col-md-4 text-right">
            <a href="/body/<?= $module ?>/comparePrices"  class="btn btn-info" data-toggle="modal" data-target="#popup_lg">
                <i class="fas fa-dollar-sign"></i> Compare prices
            </a>
        </div>
    </div>
</form>

<table class="table">
    <tr>
        <th>Source</th>
        <th>Source ID</th>
        <th>SKU</th>
        <th>Source SKU</th>
        <th style="max-width: 300px">Title</th>
        <th>Status</th>
        <th>Qty</th>
        <th colspan="3"></th>
    </tr>
    <? foreach ($products as $p): ?>
        <tr>
            <td><?= $sources[$p['source_id']] ?></td>
            <td><?= $p['item_source_id'] ?></td>
            <td><?= $p['sku'] ?></td>
            <td><?= $p['source_sku'] ?></td>
            <td><?= $p['title'] ?></td>
            <td><?= (int)$p['active'] ? "<i class=\"fa fa-solid fa-check\"></i>" : "<i class=\"fa fa-solid fa-ban\"></i>" ?></td>
            <td><?= (int)$p['store_qty'] ?></td>
            <td>
                <a href="/body/<?= $module ?>/products/map_products/?sku=<?= $p['sku'] ?>" data-toggle="modal" data-target="#popup">
                    <i class="fas fa-sitemap text-info" title="Map" style="opacity: <?= (int)$p['has_map'] ? 1 : 0.5 ?>"></i>
                </a>
            </td>
            <td>
                <a href="#">
                    <a href="/body/<?= $module ?>/products/product_details/?item_id=<?= $p['id'] ?>" data-toggle="modal" data-target="#popup">
                        <i class="fas fa-info-circle" title="Details"></i>
                    </a>
                </a>
            </td>
        </tr>
    <? endforeach; ?>
</table>
<div>
    <?= $split_page ?>
</div>
