<?php

namespace controller\labels;

class main extends \Controller {
    private $string_font_sizes = [
        'dl' => 77,
        6 => 80,
        7 => 75,
        8 => 70,
        9 => 65,
        10 => 60,
        11 => 55,
        12 => 50,
        13 => 47,
        14 => 45,
        15 => 43
    ];

    private $special_chars = [
        "." => "___",
        "/" => "---",
        " " => "-_-"
    ];

    function __construct() {
        global $Acl;

        $Acl->permission_redirect("admin");

        require ROOT_DIR . '/vendor/autoload.php';
    }

    function split_string($str, $split_position, $font_size) {
        $bcode = new \Picqer\Barcode\BarcodeGeneratorPNG();

        $str_len = strlen($str);
        $result['part_number'] = $str;
        $result['part_number_clean'] = $str;

        $result['font_size'] = isset($this->string_font_sizes[$str_len]) ? $this->string_font_sizes[$str_len] : $this->string_font_sizes[6];
        $result['line_height'] = 130;
        $result['barcode'] = base64_encode($bcode->getBarcode($str, $bcode::TYPE_CODE_128, 2, 26));
        $result['barcode_height'] = 45;

        if (($str_len >= 9 && $split_position == 0) || ($split_position !== 0 && $split_position !== (int)$str_len)) {
            $split_position = (int)$split_position == 0 ? (int)$str_len / 2 : $split_position;

            $result['part_number'] = "<div style='font-size: {{top_font_size}}px'>";
            $result['part_number'] .= rtrim(substr($str, 0, $split_position), "-");
            $result['part_number'] .= "</div>";
            $result['part_number'] .= "<div>";
            $result['part_number'] .= ltrim(substr($str, $split_position), "-");
            $result['part_number'] .= "</div>";

            $result['line_height'] = 70;
            $result['barcode_height'] = 26;
            $result['font_size'] = $this->string_font_sizes['dl'];
        }
        elseif (isset($string_font_sizes[$str_len])) {
            $result['font_size'] = $string_font_sizes[$str_len];
        }
        $result['top_font_size'] = $result['font_size'];

        // font size
        if ($font_size !== "") {
            $f_sizes = explode("|", $font_size);
            $result['font_size'] = $f_sizes[0];
            $result['top_font_size'] = isset($f_sizes[1]) ? $f_sizes[1] : $f_sizes[0];
        }

        $result['part_number'] = str_replace('{{top_font_size}}', $result['top_font_size'], $result['part_number']);

        $result['split_position'] = $split_position;

        return $result;
    }

    function save_positions() {
        global $MysqlDb, $Router;

        // single update
        if ($Router->post('part_number')) {
            $MysqlDb->prepare_vars['part_number'] = str_replace($this->special_chars, array_keys($this->special_chars),
                $Router->post('part_number'));

            $Router->post_data['split_position_' . $Router->post('part_number')] = $Router->post('split_position');
            $Router->post_data['font_size_' . $Router->post('part_number')] = $Router->post('font_size');
            $Router->post_data['top_font_size_' . $Router->post('part_number')] = $Router->post('top_font_size');
            $Router->post_data['margin_top_' . $Router->post('part_number')] = $Router->post('margin_top');

            $result = $MysqlDb->select("labels", "*", "part_number = '{{part_number}}'");
        }
        else {
            $MysqlDb->prepare_vars['manufacturer'] = base64_decode($Router->post('manufacturer'));
            $result = $MysqlDb->select("labels", "*", "manufacturer = '{{manufacturer}}'");
        }

        while ($row = $MysqlDb->get_result($result)) {
            $part_n = $row['part_number'];
            $part_n_for_post = str_replace(array_keys($this->special_chars), $this->special_chars,
                $part_n); // when '.' converts to '~~'
            $MysqlDb->prepare_vars['part_number'] = $part_n;

            $update_fields = [
                'split_position' => $Router->post('split_position_' . $part_n_for_post),
                'font_size' => $Router->post('font_size_' . $part_n_for_post) . "|" . $Router->post('top_font_size_' . $part_n_for_post),
                'margin_top' => $Router->post('margin_top_' . $part_n_for_post)
            ];
            $MysqlDb->update("labels", $update_fields, "part_number = '{{part_number}}'");
        };

        echo request_callback([
            'status' => "success",
            'redirect_url' => "/" . $Router->module . "/main/printPage/?manufacturer=" . $Router->post('manufacturer') . "&print=1'"
        ]);
    }

    function printPage() {
        global $Html, $MysqlDb, $Router;

        $MysqlDb->prepare_vars['manufacturer'] = base64_decode($Router->get('manufacturer'));

        $need_split = $MysqlDb->record_exist("labels",
            "manufacturer = '{{manufacturer}}' AND LENGTH(part_number) >= 1");
        $data = [];

        if ($need_split && !$Router->get('print')) {
            $result = $MysqlDb->select("labels", "*", "manufacturer = '{{manufacturer}}' AND LENGTH(part_number) >= 1",
                "shelf_id ASC, part_number ASC");
            $n = 0;
            while ($row = $MysqlDb->get_result($result)) {
                $data[$n] = $this->split_string($row['part_number'], (int)$row['split_position'], $row['font_size']);
                $data[$n]['margin_top'] = $row['margin_top'];
                $n++;
            }

            $Html->content_data = [
                'data' => $data,
                'string_font_sizes' => $this->string_font_sizes,
                'special_chars' => $this->special_chars,
            ];

            $Router->page = "printPage";
            parent::_html();
        }
        else {
            if ($Router->get('manufacturer')) {
                $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
                $fontData = $defaultFontConfig['fontdata'];
                $mpdf = new \Mpdf\Mpdf([
                    'format' => [100, 58], //mm
                    'margin_left' => 3,
                    'margin_right' => 3,
                    'margin_top' => 13,
                    'margin_bottom' => 0,
                    'margin_header' => 0,
                    'margin_footer' => 0,
                    'fontdata' => $fontData + [
                            'nimbusan' => [
                                'R' => 'arial-monospaced-mt.ttf',
                                'I' => 'arial-monospaced-mt.ttf',
                                'B' => 'arial-monospaced-mt-bold.ttf',
                            ],
                        ],
                    'default_font' => 'nimbusan',
                    'default_font_size' => 65,
                ]);

                $result = $MysqlDb->select("labels", "*", "manufacturer = '{{manufacturer}}'",
                    "shelf_id ASC, part_number ASC");
                while ($row = $MysqlDb->get_result($result)) {

                    $part_n = $row['part_number'];
                    $part_n_formatted = $this->split_string($part_n, (int)$row['split_position'], $row['font_size']);

                    $font_size = $part_n_formatted['font_size'];
                    $line_height = (int)$part_n_formatted['line_height'] < 130
                        ? $part_n_formatted['line_height']
                        : $part_n_formatted['line_height'] - $row['margin_top'];
                    $barcode = $part_n_formatted['barcode'];
                    $barcode_height = $part_n_formatted['barcode_height'];

                    $out = "<div style='padding-top: " . $row['margin_top'] . "px'>";
                    $out .= "<div style='text-align: center; 
                                    font-weight: bold; 
                                    line-height: " . $line_height . "px; 
                                    height: " . (130 - $row['margin_top']) . "px; 
                                    font-size: " . $font_size . "px'>
                            " . $part_n_formatted['part_number'] . "
                        </div>";
                    $out .= "</div>";
                    $out .= "<div style='position: absolute; bottom: 0; width: 100%; text-align: center'>
                            <img src=\"data:image/png;base64," . $barcode . "\" style=\"width: 60%; height: " . $barcode_height . "px\">
                        </div>";

                    $title = "Label";
                    $mpdf->SetTitle($title);
                    $mpdf->useFixedNormalLineHeight = true;
                    $mpdf->useFixedTextBaseline = true;
                    $mpdf->normalLineheight = 1;

                    $mpdf->AddPage();
                    $mpdf->WriteHTML($out);
                }
                $file_path = "uploads/temp/Labels.pdf";
                $mpdf->Output($file_path, \Mpdf\Output\Destination::FILE);

                // set "printed" flag
                $MysqlDb->update("labels", ['printed' => 1], "manufacturer = '{{manufacturer}}'");

                //$download_file_name = str_replace("Labels", "Labels_".rand(1000, 9999), basename($file_path));
                $download_file_name = preg_replace('/^[a-z0-9-]+$/', "_", base64_decode($Router->get('manufacturer')));
                header('Content-Type: application/pdf');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $download_file_name . ".pdf\"");
                readfile($file_path);

                exit;
            }
        }

    }

    function _html() {
        global $Html, $MysqlDb, $Router;

        $manufacturers = $MysqlDb->get_rows("labels", "manufacturer, COUNT(*) AS amount, printed", 1,
            "manufacturer ASC", "",
            "manufacturer");

        $Html->content_data = [
            'manufacturers' => $manufacturers
        ];

        parent::_html();
    }
}
