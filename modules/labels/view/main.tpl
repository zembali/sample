<h3>Manufacturers</h3>
<div class="row m-t-40">
    <? foreach($manufacturers as $m): ?>
        <div class="col-md-6 m-b-20">
            <a href="/<?= $module ?>/main/printPage/?manufacturer=<?= base64_encode($m['manufacturer']) ?>"
                class="<?= (int)$m['printed'] == 1 ? "text-success" : "" ?>">
                <?= $m['manufacturer'] == "" ? "Other" : $m['manufacturer'] ?>
                <span class="text-dark">(<?= $m['amount'] ?>)</span>
            </a>
        </div>
    <? endforeach; ?>
</div>
