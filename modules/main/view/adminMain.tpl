<?
$custom_icons['users'] = "fa fa-user";
$custom_icons['text'] = "fa fa-file-alt";
$custom_icons['groups'] = "fa-solid fa-user-group";
?>
<div class="container">
    <div class="row">
        <? foreach($modules as $module): ?>
        <div class="col-md-2 text-center">
            <a href="/admin/<?=$module?>">
                <? $icon = isset($custom_icons[$module]) ? $custom_icons[$module] : "fa-folder" ?>
                <i class="<?=$icon?> fa-3x" aria-hidden="true"></i>
                <div><?=$module?></div>
            </a>
        </div>
        <? endforeach ?>
    </div>
</div>
