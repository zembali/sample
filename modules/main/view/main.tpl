<div class="container">
    <div class="row">
        <? if($Acl->has_permission("orders_view", "wc")): ?>
            <div class="col-md-2 text-center">
                <a href="/wc/orders">
                    <i class="fas fa-bars fa-3x" aria-hidden="true"></i>
                    <div>Orders</div>
                </a>
            </div>
        <? endif; ?>

        <? if($Acl->has_permission("products_view", "wc")): ?>
            <div class="col-md-2 text-center">
                <a href="/wc/products">
                    <i class="fas fa-cubes fa-3x" aria-hidden="true"></i>
                    <div>Products</div>
                </a>
            </div>
        <? endif; ?>

        <? if($Acl->has_permission("", "labels")): ?>
            <div class="col-md-2 text-center">
                <a href="/labels">
                    <i class="fas fa-receipt fa-3x" aria-hidden="true"></i>
                    <div>Labels</div>
                </a>
            </div>
        <? endif; ?>

        <? if($Acl->has_permission("change_price", "wc")): ?>
            <div class="col-md-2 text-center">
                <a href="/wc/updatePrice">
                    <i class="fas fa-hand-holding-usd fa-3x" aria-hidden="true"></i>
                    <div>Price Update</div>
                </a>
            </div>
        <? endif; ?>
    </div>
</div>
