<?php
namespace controller\main;

class adminMain extends \Controller {

	function __construct() {
		global $Acl;
		parent::__construct();

		$Acl->permission_redirect("admin");
	}

	function _html() {
		global $Html, $MysqlDb;

		$Html->content_data['modules'] = $MysqlDb->get_list("modules", "id", "name", "show_in_admin = 1");

		parent::_html();
	}
}
