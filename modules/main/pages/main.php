<?php
namespace controller\main;

class main extends \Controller {

	function __construct() {
        global $UserInfo;

        if(!$UserInfo->auth_status()){
            @header("Location: /users/login/");
            exit;
        }
	}

	function change_lang(){
		global $Router;

		if(in_array($Router->get('lang'), config('languages'))){
			@session_start();
			$_SESSION['lang'] = $Router->get('lang');
		}

		echo "ok";
		exit;
	}

	function change_currency(){
		global $Router, $Cars;

		if(in_array($Router->get('currency'), $Cars->currencies)){
			@session_start();
			$_SESSION['currency'] = $Router->get('currency');
		}

		echo "ok";
		exit;
	}

    function _html() {
        global $Html, $MysqlDb;

        parent::_html();
    }
}
