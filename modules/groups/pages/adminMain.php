<?php
namespace controller\groups;

class adminMain extends \Controller {

    function __construct() {
        global $Acl;
        parent::__construct();

        $Acl->permission_redirect("admin");
    }

    function save_group() {
        global $MysqlDb, $Router;

        // root admin is untouchable :)
        if($Router->get_int('group_id') == 1){
            exit;
        }

        if($Router->post('name') == false) {
            echo request_callback([
                'status' => "error",
                'errors' => "The name is empty"
            ]);
            exit;
        }

        $insert_fields['name'] = $Router->post('name');

        if($Router->get_int('group_id') == 0) {
            $MysqlDb->insert("groups", $insert_fields);
        }
        else{
            $MysqlDb->update("groups", $insert_fields, "id = ".$Router->get_int('group_id'));
        }

        echo request_callback([
            'status' => "success",
            'redirect_url' => "/admin/".$Router->module."/adminMain"
        ]);
        exit;
    }

    function add_group() {
        global $Html, $MysqlDb, $Router;

        $edit_value = [];
        if($Router->get_int('group_id')) {
            $edit_value = $MysqlDb->get_first_row("groups", "*", "id = ".$Router->get_int('group_id'));
        }

        $Html->content_data = [
            'edit_value' => $edit_value
        ];

        $Router->page = "add_group";
        parent::_html();
    }

    function save_permissions() {
        global $MysqlDb, $Router;

        // root admin is untouchable :)
        if($Router->get_int('group_id') == 1){
            exit;
        }

        $result_module_permissions = $MysqlDb->select("modules_permissions");

        $group_permissions = [];
        while($row = $MysqlDb->get_result($result_module_permissions)) {
            if($Router->post_int('module_permission_'.$row['id'])) {
                $group_permissions[] = $row['id'];

                if(!$MysqlDb->record_exist("groups_permissions", "group_id = ".$Router->get_int('group_id')." AND permission_id = ".(int)$row['id'])) {
                    $insert_fields = [
                        'group_id' => $Router->get_int('group_id'),
                        'module_id' => $row['module_id'],
                        'permission_id' => $row['id']
                    ];

                    $MysqlDb->insert("groups_permissions", $insert_fields);
                }
            }
        }

        $where = count($group_permissions) ? " AND permission_id NOT IN (".implode(",", $group_permissions).")" : "";
        $MysqlDb->delete("groups_permissions", "group_id = ".$Router->get_int('group_id').$where);

        $MysqlDb->update("groups", ['last_update_time' => time()], "id = ".$Router->get_int('group_id'));

        echo request_callback([
            'status' => "success",
            'redirect_url' => "/admin/".$Router->module."/adminMain/details/?group_id=".$Router->get_int('group_id')
        ]);
        exit;
    }

    function details() {
        global $Html, $MysqlDb, $Router;

        $result_module_permissions = $MysqlDb->select("modules_permissions");
        $module_permissions = [];
        while($row = $MysqlDb->get_result($result_module_permissions)) {
            $module_permissions[$row['module_id']][] = $row;
        }

        $group_permission_raw = $MysqlDb->get_rows("groups_permissions", "*", "group_id = ".$Router->get_int('group_id'));
        $group_permission = [];
        foreach ($group_permission_raw as $gp) {
            $group_permission['module_permission_'.$gp['permission_id']] = 1;
        }

        $Html->content_data = [
            'group_info' => $MysqlDb->get_first_row("groups", "*", "id = ".$Router->get_int('group_id')),
            'modules' => $MysqlDb->get_rows("modules"),
            'module_permissions' => $module_permissions,
            'group_permission' => $group_permission,
        ];

        $Router->page = "details";
        parent::_html();
    }

    function _html() {
        global $Html, $MysqlDb;

        $Html->content_data['groups'] = $MysqlDb->get_rows("groups");

        parent::_html();
    }
}
