<div class="container bg-white">
    <h3 class="p-t-15">
        <a href="/<?= $module ?>/adminMain">Groups</a>
        <i class="fa-solid fa-angle-right"></i> <?= $group_info['name'] ?></h3>

    <div class="clearfix-20"></div>

    <form method="post"
          id="save_permissions_form"
          onsubmit="return false"
          action="/post/<?= $module ?>/adminMain/save_permissions/?group_id=<?= $Router->get_int('group_id') ?>">
        <table class="table">
            <tr>
                <th>Module Name</th>
                <th>Permissions</th>
            </tr>
            <? foreach ($modules as $m): ?>
                <? if (isset($module_permissions[$m['id']])): ?>
                    <tr class="<?= isset($module_permissions[$m['id']]) ? "" : "disabled" ?>">
                        <td><?= $m['name'] ?></td>
                        <td>

                            <? foreach ($module_permissions[$m['id']] as $mp): ?>
                                <div>
                                    <?= $Form->input_form("module_permission_" . $mp['id'], "checkbox",
                                        $group_permission) ?>
                                    <label for="module_permission_<?= $mp['id'] ?>">
                                        <?= $mp['name'] . ($mp['description'] !== "" ? "(" . $mp['description'] . ")" : "") ?>
                                    </label>
                                </div>
                            <? endforeach; ?>

                        </td>
                    </tr>
                <? endif; ?>
            <? endforeach; ?>
        </table>

        <div class="mt-3 text-center">
            <button class="btn btn-primary">Save</button>
        </div>
    </form>
</div>

<script>
    $("#save_permissions_form").submit(function () {
        post_call(this);
    })
</script>
