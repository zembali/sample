<div class="modal-header">
    <h4 class="modal-title">Add/Edit Group</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <form id="add_group_form" class="form-horizontal" method="post"
          action="/post/<?= $module ?>/adminMain/save_group/?group_id=<?= $Router->get_int('group_id') ?>"
          onsubmit="return false">
        <table class="table table-borderless">
            <tr>
                <td>Name</td>
                <td>
                    <?= $Form->input_form("name", "text", $edit_value, "", "", "form-control") ?>
                </td>
            </tr>
        </table>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10 text-center">
                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>

<script>
    $("#add_group_form").submit(function () {
        post_call(this);
    })
</script>
