<div class="container bg-white">
    <h4 class="float-left p-t-15">Groups</h4>
    <a class="float-right btn btn-success" href="/body/<?= $module ?>/adminMain/add_group" data-toggle="modal" data-target="#popup">
        <i class="fa fa-plus"></i> Add Group
    </a>
    <div class="clear"></div>

    <table class="table">
        <tr>
            <th>Name</th>
            <th colspan="2"></th>
        </tr>
        <? foreach ($groups as $g): ?>
            <tr>
                <td>
                    <a href="/admin/<?= $module ?>/adminMain/details/?group_id=<?= $g['id'] ?>"><?= $g['name'] ?></a>
                </td>
                <td>
                    <? if((int)$g['id'] !== 1): ?>
                        <a href="/body/<?= $module ?>/adminMain/add_group/?group_id=<?= $g['id'] ?>" data-toggle="modal" data-target="#popup">
                            <i class="fa fa-edit text-info"></i>
                        </a>
                    <? endif; ?>
                </td>
                <td>
                    <? if((int)$g['id'] !== 1): ?>
                        <i class="fa fa-trash delete_user"
                           data-group_id="<?= $g['id'] ?>"
                           data-toggle="confirmation"
                           data-confirmation-event="myevent"></i>
                    <? endif; ?>
                </td>
            </tr>
        <? endforeach; ?>
    </table>
</div>
