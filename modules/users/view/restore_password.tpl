<div class="modal-header">
    <h4 class="modal-title pull-left"><?= _lang('restore_password') ?></h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <div class="row d-flex justify-content-center">
        <div class="col-md-10">
            <?= _lang('enter_restore_email') ?>
            <form id="restore_form" class="form-horizontal" role="form" action="/post/<?= $module ?>/login/restore_password_send"
                  onsubmit="return false">

                <div class="form-group m-t-20">
                    <div class="required">
                        <input id="mail" type="text" class="form-control" name="mail"
                               placeholder="<?= _lang('email') ?>">
                    </div>
                </div>

                <div style="margin-top:10px" class="form-group">
                    <div class="controls">
                        <button class="btn btn-brand" type="submit" id="submit"><?= _lang('send') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#restore_form").submit(function () {
        post_call(this, function(data){
            if(data == "ok_message"){
                $("#mail").val('')
            }
        });
    })
</script>
