<div class="container-fluid bg-white">
    <form id="search_form" action="/admin/<?= $module ?>/" method="get">
        <table  class="table">
            <tr>
                <th><?= _lang('name') ?></th>
                <th><?= _lang('email') ?></th>
                <th></th>
            </tr>
            <tr>
                <td>
                    <input name="name" type="text" value="<?= $Router->get('name') ?>" class="form-control">
                </td>
                <td>
                    <input name="email" type="text" value="<?= $Router->get('email') ?>" class="form-control">
                </td>
                <td>
                    <button type="submit" class="btn btn-primary">Search</button>
                </td>
            </tr>
        </table>
    </form>

    <a class="btn btn-success" href="/body/<?= $module ?>/adminMain/add_user" data-toggle="modal" data-target="#popup">
        <i class="fa fa-plus"></i> Add User
    </a>
    <div class="clearfix-10"></div>

    <div class="m-t-20">
        <table class="table table-hover">
            <tr>
                <th><?= _lang('name') ?></th>
                <th><?= _lang('email') ?></th>
                <th style='width: 90px' colspan="4"><b>action</b></th>
            </tr>
            <? foreach ($users as $id => $user): ?>
                <tr id="user_<?= $user['id'] ?>">
                    <td><?= $user['first_name'] . " " . $user['last_name'] ?></td>
                    <td><?= $user['email'] ?></td>
                    <td>
                        <a href="/body/<?= $module ?>/adminMain/add_user/?user_id=<?= $user['id'] ?>" data-toggle="modal" data-target="#popup">
                            <i class="fa fa-edit text-info"></i>
                        </a>
                    </td>
                    <td>
                        <i class="fa fa-tools text-secondary"></i>
                    </td>
                    <td>
                        <? if((int)$user['id'] !== 1): ?>
                            <i class="fa fa-trash delete_user"
                               data-user_id="<?= $user['id'] ?>"
                               data-toggle="confirmation"
                               data-confirmation-event="myevent"></i>
                        <? endif; ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </table>
    </div>
</div>

<script>
    $(".delete_user").confirmation({
        btnOkClass: "btn btn-sm btn-danger mr-2",
        onConfirm  : function(e){
            let user_id = $(this).data('user_id');
            $.get('/post/<?=$module?>/adminMain/delete_user/', {'user_id': user_id})
                .done(function(data){
                    if(data == "ok"){
                        $("#user_" + user_id).remove();
                    }
                })
        },
    });
</script>
