<? if ($Router->only_body !== 1): ?>
<div class="container">
    <div class="bg-white p-20 m-t-50">
        <? endif; ?>
        <div class="modal-header">
            <h4 class="modal-title pull-left"><?= _lang('sign_up') ?></h4>
            <? if ($Router->only_body == 1): ?>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <? endif; ?>
        </div>
        <div class="modal-body">
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#individual"><?= _lang('individual') ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#organization"><?= _lang('organization') ?></a>
                </li>
            </ul>

            <div class="p-10 m-t-30" class="registration">

                <div class="tab-content">
                    <!-- Individual -->
                    <div id="individual" class="container tab-pane active">
                        <form id="reg_ind_form" action="/post/users/registration/register/?user_type=ind&merchant=<?= $Router->get_int('merchant') ?>" method="post"
                              data-pl-form
                              onsubmit="return false">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("first_name", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('first_name')."\"") ?>
                                        <label for="first_name"><?= _lang('first_name') ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("last_name", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('last_name')."\"") ?>
                                        <label for="last_name"><?= _lang('last_name') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("email", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('email')."\"") ?>
                                        <label for="email"><?= _lang('email') ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("phone", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('phone')."\"") ?>
                                        <label for="phone"><?= _lang('phone') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("password", "password", "", "", "", "form-control transparent", "placeholder=\""._lang('password')."\"") ?>
                                        <label for="password"><?= _lang('password') ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("re_password", "password", "", "", "", "form-control transparent", "placeholder=\""._lang('re_password')."\"") ?>
                                        <label for="re_password"><?= _lang('re_password') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="m-t-20" id="terms">
                                <input class="reg-checkbox m-r-10" type="checkbox" id="agreement" name="agreement"
                                       value="1">
                                <span></span>
                                <label for="agreement" class="black">
                                    <?= _lang('accept_terms_and') ?>
                                    <span class="brand"><?= _lang('privacy_policy') ?></span>
                                </label>
                            </div>
                            <button class="btn btn-brand w-100 f-18 m-t-30 m-b-30"
                                    type="submit"><?= _lang('register') ?></button>
                        </form>
                    </div>

                    <!-- Organization -->
                    <div id="organization" class="container tab-pane fade">
                        <form id="reg_org_form" action="/post/users/registration/register/?user_type=org&merchant=<?= $Router->get_int('merchant') ?>" method="post"
                              data-pl-form
                              onsubmit="return false">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("org_name", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('first_name')."\"") ?>
                                        <label for="org_name"><?= _lang('org_name') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("org_code", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('first_name')."\"") ?>
                                        <label for="org_code"><?= _lang('org_code') ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("first_name", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('first_name')."\"") ?>
                                        <label for="first_name">
                                            <?= _lang('contact_person') ?>
                                            <span class="small"><?= " ("._lang('first_name').", "._lang('last_name').")" ?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("email", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('email')."\"") ?>
                                        <label for="email"><?= _lang('contact_email') ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("phone", "text", "", "", "", "form-control transparent", "placeholder=\""._lang('phone')."\"") ?>
                                        <label for="phone"><?= _lang('contact_phone') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("password", "password", "", "", "", "form-control transparent", "placeholder=\""._lang('password')."\"") ?>
                                        <label for="password"><?= _lang('password') ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-label-group">
                                        <?= $Form->input_form("re_password", "password", "", "", "", "form-control transparent", "placeholder=\""._lang('re_password')."\"") ?>
                                        <label for="re_password"><?= _lang('re_password') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="m-t-20" id="terms">
                                <input class="reg-checkbox m-r-10" type="checkbox" id="agreement_org" name="agreement"
                                       value="1">
                                <span></span>
                                <label for="agreement_org" class="black">
                                    <?= _lang('accept_terms_and') ?>
                                    <span class="brand"><?= _lang('privacy_policy') ?></span>
                                </label>
                            </div>
                            <button class="btn btn-brand w-100 f-18 m-t-30 m-b-30"
                                    type="submit"><?= _lang('register') ?></button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <? if ($Router->only_body !== 1): ?>
    </div>
</div>
<? endif; ?>

<script>
    //*** submit form
    $("#reg_ind_form").submit(function () {
        post_call(this)
    })
    $("#reg_org_form").submit(function () {
        post_call(this)
    })
</script>
