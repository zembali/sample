<div class="card">
    <div class="card-header"><?= _lang('profile') ?></div>
    <div class="card-body p-0">
        <article class="card-group-item">
            <div class="filter-content">
                <div class="list-group list-group-flush">
                    <a href="/<?= $module ?>/profile/" class="list-group-item text-secondary"><?= _lang('personal_info') ?></a>
                    <a href="/<?= $module ?>/favorites" class="list-group-item text-secondary"><?= _lang('my_favorites') ?></a>
                    <? if($Acl->has_permission("admin", "eventals")): ?>
                        <a href="/<?= $module ?>/allOffers/" class="list-group-item text-secondary"><?= _lang('all_orders') ?></a>
                    <? endif; ?>
                    <? if($UserInfo->is_provider()): ?>
                        <a href="/<?= $module ?>/customerOrders/" class="list-group-item text-secondary"><?= _lang('customer_orders') ?></a>
                    <? endif; ?>
                    <a href="/<?= $module ?>/profile/my_orders/" class="list-group-item text-secondary"><?= _lang('booked_spaces') ?></a>
                    <a href="/<?= $module ?>/myOffers/" class="list-group-item text-secondary"><?= _lang('listed_spaces') ?></a>
                </div>
            </div>
        </article>
    </div>
</div>
