<div class="row">
    <div class="col-md-3">
        <? include "../modules/".$module."/view/sidebar.tpl" ?>
    </div>

    <div class="col-md-9">
        <div class="card">
            <div class="card-header"><?= _lang('personal_info') ?></div>
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#individual"><?= _lang('individual') ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#organization"><?= _lang('organization') ?></a>
                    </li>
                </ul>

                <div class="p-10 m-t-30 registration">

                    <div class="tab-content">
                        <!-- Individual -->
                        <div id="individual" class="container tab-pane <?= $user_info['user_type'] !== "o" ? "active" : "fade" ?>">
                            <form
                                id="change_ind_user_info_form"
                                action="/post/<?= $module ?>/profile/change_user_info/?user_type=ind&merchant=<?= $user_info['merchant'] ?>"
                                method="post"
                                data-pl-form
                                onsubmit="return false">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("first_name", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('first_name')."\"") ?>
                                            <label for="first_name"><?= _lang('first_name') ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("last_name", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('last_name')."\"") ?>
                                            <label for="last_name"><?= _lang('last_name') ?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("email", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('email')."\"") ?>
                                            <label for="email"><?= _lang('email') ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("phone", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('phone')."\"") ?>
                                            <label for="phone"><?= _lang('phone') ?></label>
                                        </div>
                                    </div>
                                </div>

                                <? if ((int)$user_info['merchant'] == 1): ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("bank_id", "select", $user_info, $banks, "", "transparent") ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("bank_account", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('bank_account')."\"") ?>
                                            <label for="re_password"><?= _lang('bank_account') ?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("org_code", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('pid')."\"") ?>
                                            <label for="org_code"><?= _lang('pid') ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("leg_address", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('leg_address')."\"") ?>
                                            <label for="leg_address"><?= _lang('leg_address') ?></label>
                                        </div>
                                    </div>
                                </div>
                                <? endif; ?>

                                <button class="btn btn-brand" type="submit"><?= _lang('save') ?></button>
                            </form>
                        </div>

                        <!-- Organization -->
                        <div id="organization" class="container tab-pane <?= $user_info['user_type'] == "o" ? "active" : "fade" ?>">
                            <form id="change_org_user_info_form" action="/post/<?= $module ?>/profile/change_user_info/?user_type=org&merchant=1" method="post"
                                  data-pl-form
                                  onsubmit="return false">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("org_name", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('first_name')."\"") ?>
                                            <label for="org_name"><?= _lang('org_name') ?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("org_code", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('org_code')."\"") ?>
                                            <label for="org_code"><?= _lang('org_code') ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("first_name", "text", trim($user_info['first_name']." ".$user_info['last_name']), "", "", "form-control transparent", "placeholder=\""._lang('first_name')."\"") ?>
                                            <label for="first_name">
                                                <?= _lang('contact_person') ?>
                                                <span class="small"><?= " ("._lang('first_name').", "._lang('last_name').")" ?></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("email", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('email')."\"") ?>
                                            <label for="email"><?= _lang('contact_email') ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("phone", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('phone')."\"") ?>
                                            <label for="phone"><?= _lang('contact_phone') ?></label>
                                        </div>
                                    </div>
                                </div>

                                <? if ((int)$user_info['merchant'] == 1): ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("bank_id", "select", $user_info, $banks, "", "transparent") ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("bank_account", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('bank_account')."\"") ?>
                                            <label for="re_password"><?= _lang('bank_account') ?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-label-group">
                                            <?= $Form->input_form("leg_address", "text", $user_info, "", "", "form-control transparent", "placeholder=\""._lang('leg_address')."\"") ?>
                                            <label for="leg_address"><?= _lang('leg_address') ?></label>
                                        </div>
                                    </div>
                                </div>
                                <? endif; ?>

                                <button class="btn btn-brand" type="submit"><?= _lang('save') ?></button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="card m-t-30">
            <div class="card-header">Password</div>
            <div class="card-body">
                <form id="change_password_form" method="post"
                      action="/post/<?= $module ?>/profile/change_password/" onsubmit="return false">
                    <div class="form-group">
                        <label for="email">Current Password</label>
                        <?= $Form->input_form("old_password", "password", "", "", "", "form-control") ?>
                    </div>
                    <div class="form-group">
                        <label for="pwd">New Password</label>
                        <?= $Form->input_form("new_password", "password", "", "", "", "form-control") ?>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Reenter new Password</label>
                        <?= $Form->input_form("re_password", "password", "", "", "", "form-control") ?>
                    </div>
                    <button class="btn btn-brand" type="submit"><?= _lang('save') ?></button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    $("#change_ind_user_info_form").submit(function () {
        post_call(this)
    })
    $("#change_org_user_info_form").submit(function () {
        post_call(this)
    })

    $("#change_password_form").submit(function () {
        post_call(this, function (data) {
            if(data == "ok"){
                $("#old_password, #new_password, #re_password").val('');
            }
        })
    })
</script>
