<div class="modal-header">
    <h4 class="modal-title">Add/Edit User</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <form id="add_user_form" class="form-horizontal" method="post"
          action="/post/<?= $module ?>/adminMain/save_user/?user_id=<?= $Router->get_int('user_id') ?>"
          onsubmit="return false">
        <table class="table">
            <tr>
                <td>First Name</td>
                <td>
                    <?= $Form->input_form("first_name", "text", $edit_value, "", "", "form-control") ?>
                </td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>
                    <?= $Form->input_form("last_name", "text", $edit_value, "", "", "form-control") ?>
                </td>
            </tr>
            <tr>
                <td>Username</td>
                <td>
                    <?= $Form->input_form("email", "text", $edit_value, "", "", "form-control") ?>
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td>
                    <?= $Form->input_form("password", "password", $edit_value, "", "", "form-control") ?>
                </td>
            </tr>
        </table>

        <div class="clearfix"></div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Groups</h5>
                <div class="card-text" style="display: block; max-height: 100px; overflow: auto">
                    <? foreach ($groups as $g): ?>
                        <div>
                            <?= $Form->input_form("group_".$g['id'], "checkbox", $edit_value, "", "", "", $g['id'] == 1 ? "disabled" : "") ?>
                            <label for="group_<?= $g['id'] ?>"><?= $g['name'] ?></label>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>

<script>
    $("#add_user_form").submit(function () {
        post_call(this);
    })
</script>
