<div class="container">
    <div class="row mt-5">
        <div class="col-md-4 offset-4">
            <h2 class="m-b-30">
                Login to System
            </h2>
            <form class="brand-form" id="login_form"
                  action="/post/<?= $module ?>/login/login/?referrer=<?= $referrer ?>" method="post" data-pl-form
                  onsubmit="return false">
                <div class="form-label-group">
                    <input type="text" id="email" name="email" placeholder="email" class="form-control" autofocus>
                    <label for="email">Username</label>
                </div>

                <div class="form-label-group">
                    <input type="password" id="password" name="password" placeholder="Password" class="form-control">
                    <label for="password">Password</label>
                </div>

                <div class="form-group">
                    <input type="checkbox" id="save_login" name="save_login" value="1">
                    <label for="save_login">Remember</label>
                </div>

                <input type="hidden" name="login" value="1">
                <button id="submit" type="submit" class="btn btn-success">Login</button>

            </form>
        </div>
    </div>
</div>

<div class="clearfix-20"></div>

<script>
    $("#email").focus();

    $("#login_form").submit(function () {
        post_call(this);
    })
</script>
