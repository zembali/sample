<div class="card m-t-30">
    <div class="card-header"><?= _lang('restore_password') ?></div>
    <div class="card-body">
        <? if ($UserInfo->check_restore_code($Router->get('code'))): ?>
            <form id="change_password_form" method="post"
                  action="/post/<?= $module ?>/login/set_new_password_action/?code=<?= $Router->get('code') ?>" onsubmit="return false">
                <div class="form-group">
                    <label for="pwd"><?= _lang('new_password') ?></label>
                    <?= $Form->input_form("new_password", "password", "", "", "", "form-control") ?>
                </div>
                <div class="form-group">
                    <label for="pwd"><?= _lang('re_password') ?></label>
                    <?= $Form->input_form("re_password", "password", "", "", "", "form-control") ?>
                </div>
                <button class="btn btn-brand" type="submit"><?= _lang('save') ?></button>
            </form>
        <? else: ?>
            <div class="alert alert-danger"><?= _lang('restore_code_error') ?></div>
        <? endif; ?>
    </div>
</div>

<script>
    $("#change_password_form").submit(function () {
        post_call(this, function(data){
            if(data == "ok"){
                $("#old_password, #new_password, #re_password").val('');
            }
        });
    })
</script>
