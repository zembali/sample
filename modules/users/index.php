<?php

$accepted_pages = [
	"main",
	"registration",
	"login",
	"logout",
	"profile",
    "adminMain"
];

$Router->load_module_page($accepted_pages);
