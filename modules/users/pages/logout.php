<?php
@session_start();
session_unset();
unset($_SESSION);
setcookie('login_hash', "", time() - 3600, "/");
header("Location: /");
exit;