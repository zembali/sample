<?php

namespace controller\users;

class adminMain extends \Controller {

    function __construct() {
        global $Acl;

        $Acl->permission_redirect("admin");
    }

    function save_user()
    {
        global $Router, $MysqlDb, $UserInfo, $Form;

        $required_fields = [
            'first_name',
            'last_name',
            'email',
        ];

        // check if fields are filled
        $ok_fields = $fields = [];
        foreach ($required_fields as $field_name) {
            if ($Router->post($field_name) == false) {
                echo request_callback([
                    'status' => "error",
                    'errors' => [$field_name => _lang('FILL_REQUIRED_FIELDS')],
                    'ok_fields' => $ok_fields
                ]);
                exit;
                $ok_fields[] = $field_name;
            }
            $fields[$field_name] = $Router->post($field_name);
        }

        // validate username
        $user_info = $UserInfo->get_user_info_by_email($Router->post('mail'));
        if ($user_info !== false && (int)$user_info['id'] !== (int)$Router->get_int('user_id')) {
            echo request_callback([
                'status' => "error",
                'errors' => ['mail' => _lang('email_exists')],
            ]);
            exit;
        }

        // validate password if it's a new user, or it's a password update
        if ($Router->post('password') || $Router->get_int('user_id') == 0) {
            if ($Form->password_strength_checker($Router->post('password')) < 20) {
                echo request_callback([
                    'status' => "error",
                    'errors' => ['password' => lang('password_is_too_week')],
                    'ok_fields' => $ok_fields
                ]);
                exit;
            }

            $fields['password'] = password_hash($Router->post('password'), PASSWORD_DEFAULT);
        }

        // insert user
        if($Router->get_int('user_id') == 0){
            $fields['add_time'] = current_time();
            $user_id = $MysqlDb->insert("users", $fields);

            $MysqlDb->insert("users_other_info", ['user_id' => $user_id]);
        }
        else{
            $user_id = $Router->get_int('user_id');
            $fields['update_time'] = current_time();
            $MysqlDb->update("users", $fields, "id = ".$user_id);

        }

        // user groups
        $groups = $MysqlDb->select("groups", "*", "id != 1");
        $user_groups = [];
        foreach ($groups as $g) {
            if((int)$g['id'] == 1) continue; // skip root admin group
            if($Router->post('group_'.$g['id'])) {
                if(!$MysqlDb->record_exist("users_groups", "user_id = ".$user_id." AND group_id = ".(int)$g['id'])){
                    $MysqlDb->insert("users_groups", ['user_id' => $user_id, 'group_id' => $g['id']]);
                }
                $user_groups[] = $g['id'];
            }
        }

        // delete extra groups
        $user_groups[] = 1;
        $MysqlDb->delete("users_groups", "user_id = ".(int)$user_id." AND group_id NOT IN (".implode(",", $user_groups).")");

        echo request_callback([
            'status' => "success",
            'redirect_url' => "/admin/".$Router->module."/adminMain"
        ]);
        exit;
    }

    function delete_user() {
        global $MysqlDb, $Router;

        if ($Router->get_int('user_id') !== 1) {
            $MysqlDb->delete("users", "id = ".$Router->get_int('user_id'));
        }

        echo "ok";
        exit;
    }

    function add_user() {
        global $Router, $Html, $UserInfo, $MysqlDb;

        $edit_value = [];
        if($Router->get_int('user_id') !== 0) {
            $edit_value = $UserInfo->get_user_info_by_id($Router->get_int('user_id'));
        }

        $groups = $MysqlDb->select("groups");
        $user_groups = $MysqlDb->get_rows("users_groups", "*", "user_id = ".$Router->get_int('user_id'));

        foreach ($user_groups as $ug) {
            $edit_value['group_'.$ug['group_id']] = 1;
        }

        $Html->content_data = [
            'edit_value' => $edit_value,
            'groups' => $groups,
        ];

        $Router->page = "add_user";
        parent::_html();
    }

    function _html() {
        global $Router, $Html, $MysqlDb, $Form;

        $where = 1;
        if($Router->get('email')){
            $MysqlDb->prepare_vars['email'] = $Router->get('email');
            $where .= " AND email = '{{email}}'";
        }

        if($Router->get('name')){
            $names = explode(" ", $Router->get('name'));
            $MysqlDb->prepare_vars['name1'] = $names[0];
            if (isset($names[1])) {
                $MysqlDb->prepare_vars['name2'] = $names[1];
                $where .= " AND ((first_name LIKE '%{{name1}}%' AND last_name LIKE '%{{name2}}%') OR
						(first_name LIKE '%{{name2}}%' AND last_name LIKE '%{{name1}}%'))";
            }
            else {
                $where .= " AND (first_name LIKE '%{{name1}}%' OR last_name LIKE '%{{name1}}%')";
            }
        }

        $users = $MysqlDb->get_rows("users", "*", $where);

        $Html->content_data = ['users' => $users];

        parent::_html();
    }
}
