<?php

namespace controller\users;

class login extends \Controller {

    private $referrer = "";

    function __construct() {
        global $UserInfo, $MysqlDb, $Router;

        $this->referrer = $Router->get('referrer') ? base64_decode($Router->get('referrer')) : "/";

        if($UserInfo->auth_status() == true){
            header("Location: ".$this->referrer);
            exit;
        }

        //*** check saved login
        $Login = new \model\users\Login();

        if(isset($_COOKIE['login_hash']) && strlen($_COOKIE['login_hash']) == 32){
            $MysqlDb->prepare_vars['login_hash'] = $_COOKIE['login_hash'];
            $parameters = $MysqlDb->get_first_row("users", "id as user_id", "login_hash = '{{login_hash}}'");
            $parameters['save_login'] = 1;
            $Login->set_login_sessions($parameters);

            header("Location: ".$this->referrer);
            exit;
        }
    }

    function login(){
        global $Router, $UserInfo;

        $Login = new \model\users\Login();

        if ($Router->post('email') == false || $Router->post('password') == false) {
            echo request_callback([
                                      'status' => "error",
                                      'errors' => _lang('enter_email_and_password')
                                  ]);
            exit;
        }

        $user_info = $UserInfo->get_user_info_by_email($Router->post('email'));
        if($user_info == false || password_verify($Router->post('password'), $user_info['password']) == false){
            echo request_callback([
                                      'status' => "error",
                                      'errors' => _lang('email_or_password_is_incorrect')
                                  ]);
            exit;
        }

        $parameters['save_login'] = $Router->post_int('save_login');
        $parameters['user_id'] = $user_info['user_id'];
        $Login->set_login_sessions($parameters);

        echo request_callback([
                                  'status' => "ok",
                                  'redirect_url' => $this->referrer
                              ]);
        exit;
    }

    /**
     * login with social media
     */
    function social_login(){
        global $Router, $UserInfo, $MysqlDb;

        $social_networks = $UserInfo->social_logins;
        if (in_array($Router->get('social_login'), $social_networks)) {
            require_once ROOT_DIR."tools/oauth/login_with_".$Router->get('social_login').".php";
        }
        else{
            header("Location: /");
            exit;
        }

        $fields['facebook'] = array(
            "social_user_id" => "id",
            "email" => "email",
            "first_name" => "first_name",
            "last_name" => "last_name",
            "gender" => "gender",
            "social_account_link" => "link",
            "social_timezone" => "timezone",
            "social_update_time" => "updated_time",
            "verified" => "verified");

        $fields['linkedin'] = array(
            "social_user_id" => "id",
            "email" => "emailAddress",
            "first_name" => "firstName",
            "last_name" => "lastName",
            "social_account_link" => "publicProfileUrl");

        $fields['google'] = array(
            "social_user_id" => "id",
            "email" => "email",
            "first_name" => "given_name",
            "last_name" => "family_name",
            "gender" => "gender",
            "social_account_link" => "link",
            "verified" => "verified_email");

        // init classes
        $Login = new \model\users\Login();
        $Registration = new \model\users\Registration();

        // check if user already registered by this social network
        $user_info = $UserInfo->get_user_info_by_social_user_id($user->{$fields[$Router->get('social_login')]['social_user_id']});

        if($user_info) {
            
            $Login->set_login_sessions($user_info);
            $Login->update_social_user_profile_picture($user);
            header("Location: ".$this->referrer);
            exit;
        }
        // register user by social network
        else{
            foreach ($fields[$Router->get('social_login')] as $local_field_name => $social_field_name){
                $user_insert_fields[$local_field_name] = $user->{$fields[$Router->get('social_login')][$local_field_name]};
            }
            $user_insert_fields['social_network_id'] = array_search($Router->get('social_login'), $social_networks);

            // check require fields
            $registration_fields = array("first_name", "email", "social_network_id", "social_user_id");
            $registration_error = 0; // if require field is empty
            foreach ($registration_fields as $field_name){
                if(empty($user_insert_fields[$field_name]) || strlen($user_insert_fields[$field_name]) == 0){
                    session_unset();
                    header("Location: ".$this->referrer);
                    exit;
                }
            }

            // check if user by this email already exists
            $user_info = $UserInfo->get_user_info_by_email($user_insert_fields['email']);
            if($user_info){
                $user_insert_fields['user_id'] = $user_info['id'];
                $insert_id = $user_info['id'];
                $Registration->insert_login_user($user_insert_fields);
            }
            else{
                $user_insert_fields['user_type'] = "i"; // individual
                $insert_id = $Registration->save_user_info($user_insert_fields);
            }

            if($insert_id !== false){
                $parameters['user_id'] = $insert_id;
                $Login->set_login_sessions($parameters);
                $Login->update_social_user_profile_picture($user);
                header("Location: ".$this->referrer);
                exit;
            }
            else{
                session_unset();
                header("Location: ".$this->referrer);
                exit;
            }
        }
    }

    function restore_password_send(){
        global $UserInfo, $Router, $MysqlDb, $Html;

        $user_info = $UserInfo->get_user_info_by_email($Router->post('mail'));

        if((int)$user_info['id'] == 0){
            echo request_callback([
                                      'status' => "error",
                                      'errors' => lang('user_not_found')
                                  ]);
            exit;
        }

        $restore_code = md5($user_info['id'] . rand() . time());
        $fields['restore_password_code'] = $restore_code;
        $fields['restore_password_code_time'] = time();
        $MysqlDb->update("users_other_info", $fields, "user_id = " . (int)$user_info['id']);

        $restore_link = config('site_url') . "/" . $Router->module . "/login/set_new_password/?code=" . $restore_code;

        $send_text = $Html->get_mail_template("RESTORE_PASSWORD", ['restore_link' => $restore_link]);

        send_mail($Router->post('mail'), $send_text['title'], $send_text['template']);

        echo request_callback([
                                  'status'  => "ok_message",
                                  'ok_message' => lang('restore_instruction_sent')
                              ]);
        exit;
    }

    function restore_password(){
        global $Router;

        $Router->page = "restore_password";
        parent::_html();
    }

    function set_new_password_action(){
        global $Router, $MysqlDb, $Form, $UserInfo;

        $user_id = $UserInfo->check_restore_code($Router->get('code'));
        if($user_id == false){
            echo request_callback([
                                      'status' => "error",
                                      'errors' => [_lang('restore_code_error')],
                                  ]);
            exit;
        }

        // validate new password
        if ($Form->password_strength_checker($Router->post('new_password')) < 50) {
            echo request_callback([
                                      'status' => "error",
                                      'errors' => ['new_password' => _lang('password_is_too_week')],
                                  ]);
            exit;
        }
        $ok_fields[] = "new_password";

        // validate retype password
        if ($Router->post('new_password') !== $Router->post('re_password')) {
            echo request_callback([
                                      'status' => "error",
                                      'errors' => ['re_password' => _lang('passwords_doesnt_match')],
                                      'ok_fields' => $ok_fields
                                  ]);
            exit;
        }

        $fields['password'] = password_hash($Router->post('new_password'), PASSWORD_DEFAULT);
        $MysqlDb->update("users", $fields, "id = ".(int)$user_id);

        echo request_callback([
                                  'status' => "ok",
                                  'ok_message' => _lang('password_changed'),
                              ]);
        exit;
    }

    function set_new_password(){
        global $Router, $UserInfo;

        $Router->page = "set_new_password";
        parent::_html();
    }

    function _html() {
        global $Router, $Html, $MysqlDb, $Form;

        $Html->content_data['referrer'] = $Router->get('referrer');
        parent::_html();
    }
}
