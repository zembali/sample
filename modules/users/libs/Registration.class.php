<?php
namespace model\users;

class Registration{
	/**
	 * insert new user to db
	 * @param array $parameters (first_name, last_name, email, password)
	 * @return array    status => 'ok' and insert_id => insert_id, or status => 'error' and error_message => error_message
	 */
	function save_user_info($parameters){
		global $MysqlDb, $UserInfo;

        $user_fields = [
            "user_type",
            "merchant",
            "first_name",
            "last_name",
            "email",
            "phone",
            "password"
        ];

        $user_other_fields = [
            "leg_address",
            "org_name",
            "org_code",
            "bank_id",
            "bank_account"
        ];

        $insert_fields = [];
		foreach($user_fields as $field_name){
			if(isset($parameters[$field_name])){
				$insert_fields[$field_name] = $parameters[$field_name];
			}
		}

        $insert_other_fields = [];
		foreach($user_other_fields as $field_name){
			if(isset($parameters[$field_name])){
				$insert_other_fields[$field_name] = $parameters[$field_name];
			}
		}

		if($UserInfo->user_id == 0) {
            // insert user
            $insert_id = $MysqlDb->insert("users", $insert_fields);

            // insert user other info
            $insert_other_fields['user_id'] = $insert_id;
            $MysqlDb->insert("users_other_info", $insert_other_fields);

            // insert login user
            if ((int)$insert_id !== 0) {
                $parameters['user_id'] = $insert_id;
                $this->insert_login_user($parameters);
            }
        }
		else{
            // update user
            $insert_id = $MysqlDb->update("users", $insert_fields, "id = ".(int)$UserInfo->user_id);

            // update user other info
            if(count($insert_other_fields)){
                $MysqlDb->update("users_other_info", $insert_other_fields, "user_id = ".(int)$UserInfo->user_id);
            }

            $insert_id = $UserInfo->user_id;
        }
		
		return (int)$insert_id !== 0 ? $insert_id : false;
	}

	/**
	 * insert login parameters for existing user
	 * @param array $parameters (social_network_id, social_user_id)
	 * @return bool
	 */
	function insert_login_user($parameters){
		global $MysqlDb;

		$user_fields = array("social_network_id", "social_user_id");

		foreach($user_fields as $field_name){
			if(isset($parameters[$field_name])){
				$insert_fields[$field_name] = $parameters[$field_name];
			}
		}

		// insert login user
		$insert_fields['user_id'] = $parameters['user_id'];
		$insert_id = $MysqlDb->insert("users_login_social_users", $insert_fields);

		return (int)$insert_id !== 0 ? true : false;
	}

}
