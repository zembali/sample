<?php
namespace model\users;

class Login{
	public $user_id = 0;
	public $last_login = "";
	
	function auth_status(){
		return "ok";
	}

	/**
	 * set login sessions
	 */
	function set_login_sessions($parameters){
		global $MysqlDb, $UserInfo;

		@session_start();
		$_SESSION['user'] = $parameters;
		$this->user_id = $parameters['user_id'];

		// get user permission last update
		$user_perm_info = $MysqlDb->get_first_row("users_permissions", "last_update_time", "user_id = ".(int)$this->user_id);
		$_SESSION['user']['permission_last_update'] = $user_perm_info['last_update_time'];

        //*** save login
        if(isset($parameters['save_login']) && (int)$parameters['save_login'] == 1){
            $user_info = $UserInfo->get_user_info_by_id($parameters['user_id']);
            $login_hash = $user_info['login_hash'] == "" ? generatePassword(32, 3) : $user_info['login_hash'];
            $fields['login_hash'] = $login_hash;
            setcookie('login_hash', $login_hash, time() + 315360000, "/");
        }
        else{
            setcookie('login_hash', "", time() - 3600, "/");
        }

        $fields['last_login_time'] = time();
		$MysqlDb->update("users", $fields, "id = ".(int)$parameters['user_id']);
	}

	/**
	 * update user image, if auth by social network
	 * @param array $social_network_data
	 */
	function update_social_user_profile_picture($social_network_data){
		global $MysqlDb;

		if(isset($social_network_data->profile_picture) && $social_network_data->profile_picture !== ""){
			// for ssl connection to save image
			$arrContextOptions=array(
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			);

			$image_info = $MysqlDb->get_first_row("users_profile_images", "id, image_src", "user_id = ".(int)$this->user_id);
			// if images changed get image data
			if($image_info['image_src'] !== $social_network_data->profile_picture){
				$fields['image'] = base64_encode(file_get_contents($social_network_data->profile_picture, false, stream_context_create($arrContextOptions)));
				$fields['image_src'] = $social_network_data->profile_picture;
			}
			// insert or update image
			if((int)$image_info['id'] !== 0){
				if($image_info['image_src'] !== $social_network_data->profile_picture){
					$MysqlDb->update("users_profile_images", $fields, "user_id = ".(int)$this->user_id);
				}
			}
			else{
				$fields['user_id'] = $this->user_id;
				$MysqlDb->insert("users_profile_images", $fields);
			}
		}
	}
}