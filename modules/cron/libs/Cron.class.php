<?php

namespace model\cron;

use \MysqlDb;

class Cron
{
    function validate_access(){
        global $Acl;

        if (
            $Acl->is_root_admin() ||
            php_sapi_name() === "cli" ||
            (isset($_SERVER['HTTP_CHECKSUM']) && $_SERVER['HTTP_CHECKSUM'] === $this->gen_checksum())
        ) {
            return true;
        }
        else{
            return false;
        }
    }

    function gen_checksum() {
        global $global_config;

        return sha1(serialize($global_config));
    }
}
