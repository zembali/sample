<?php

namespace model\cron;

use model\wc\Qb;
use \MysqlDb;

class Orders
{
    function save_order($data)
    {
        $MysqlDb = new MysqlDb();
        $Qb = new Qb();

        // json fields
        foreach ($data as $key => $value) {
            $value = str_replace(["’", "'"], "`", $value);
            if (is_array($value)) {
                $fields[$key] = json_encode($value, JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES);
            }
            else{
                $fields[$key] = $value;
            }
        }

        $fields['shipping_hash'] = md5(serialize($fields['shipping_address']));

        $MysqlDb->prepare_vars['order_id'] = $data['order_id'];
        $order_info = $MysqlDb->get_first_row("wc_orders", "id", "source_id = ".(int)$data['source_id']." AND order_id = '{{order_id}}'");
        if (!$order_info) {
            $source_config = $Qb->get_source_config($data['source_id']);

            if ($source_config['parameters']['qb_order_number_type'] == "original") {
                $ref_number = $source_config['parameters']['qb_order_number_prefix'].$data['order_id'];
            }
            elseif (isset($data['qb_ref_number'])) {
                $ref_number = $source_config['parameters']['qb_order_number_prefix'].$data['qb_ref_number'];
            }
            else {
                $max_ref_number = $MysqlDb->max_value("wc_orders", "qb_ref_number", "source_id = ".(int)$data['source_id']);
                $max_ref_number = ltrim($max_ref_number, $source_config['parameters']['qb_order_number_prefix']);
                $ref_number = $source_config['parameters']['qb_order_number_prefix'].($max_ref_number + 1);
            }

            $fields['qb_ref_number'] = $ref_number;
            $fields['add_time'] = current_time();
            $MysqlDb->insert("wc_orders", $fields);
        } else {
            if(isset($fields['qb_status_force'])){
                $fields['qb_status'] = $fields['qb_status_force'];
                unset($fields['qb_status_force']);
            }
            else{
                unset($fields['qb_status']);
            }
            $fields['update_time'] = current_time();
            $MysqlDb->update("wc_orders", $fields, "id = " . (int)$order_info['id']);
        }
    }

    function last_sync_time($source_id) {
        $MysqlDb = new MysqlDb();

        $last_order = $MysqlDb->get_first_row("wc_orders", "order_update_time", "source_id = ".(int)$source_id, "order_update_time DESC");

        return $last_order ? $last_order['order_update_time'] : current_date()." 00:00:00";
    }
}
