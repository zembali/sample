<?php

namespace controller\cron;

use \model\cron\Cron;


class common extends \Controller
{

    function __construct()
    {
        $Cron = new Cron();

        // check permission
        if (!$Cron->validate_access()) {
            exit;
        }
    }

    function clearLogs(){
        global $MysqlDb;

        $MysqlDb->force_delete("wc_error_log", "add_time < '".current_time(strtotime(" -15 days"))."'");

        $MysqlDb->table_prefix = "";
        $MysqlDb->force_delete("quickbooks_log", "log_datetime < '".current_time(strtotime(" -15 days"))."'");
        $MysqlDb->force_delete("quickbooks_queue", "enqueue_datetime < '".current_time(strtotime(" -15 days"))."'");
        $MysqlDb->force_delete("quickbooks_ticket", "write_datetime < '".current_time(strtotime(" -15 days"))."'");

        return "Success";
    }
}
