<?php

namespace controller\cron;

use \model\cron\Cron;
use \model\cron\Orders;
use \model\wc\Qb;

class amazon extends \Controller {
    /**
     * @var array action => url
     */
    private $service_url = [
        'SubmitFeed' => "/Feeds/",
        'ListOrders' => "/Orders/",
        'ListOrderItems' => "/Orders/",
    ];

    function __construct() {
        global $MysqlDb;

        $Cron = new Cron();

        // check permission
        if(!$Cron->validate_access()){
            exit;
        }
    }

    function callApi($action, $parameters, $config) {
        $param = [
            'AWSAccessKeyId' => $config['access_key'],
            'Action' => $action,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp' => gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version' => $parameters['version'],
        ];

        if(isset($config['mws_auth_token'])) {
            $param['MWSAuthToken'] = $config['mws_auth_token'];
        }

        $param = array_merge($param, $parameters['get_fields']);
        $url = array();
        foreach($param as $key => $val){
            $key = rawurlencode($key);
            $val = rawurlencode($val);
            $url[] = "{$key}={$val}";
        }
        sort($url);

        $arr   = implode("&", $url);

        $sign  = "POST\n";
        $sign .= "mws.amazonservices.com\n";
        $sign .= $this->service_url[$action]."{$param['Version']}\n";
        $sign .= $arr;

        $signature = hash_hmac("sha256", $sign, $config['secret_key'], true);

        $signature = urlencode(base64_encode($signature));

        $link  = "https://mws.amazonservices.com".$this->service_url[$action]."{$param['Version']}?";
        $link .= $arr."&Signature=".$signature;

        $ch = curl_init($link);
        if(isset($parameters['headers'])){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $parameters['headers']);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 1);
        if(isset($parameters['post_fields'])){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters['post_fields']);
        }
        $response = curl_exec($ch);
        $errors = curl_error($ch);
        curl_close($ch);

        if(strlen($errors) !== 0) {
            $Qb = new Qb();
            $Qb->error_log($parameters['name'].": ".print_r($errors, true), "Amazon API");

            return false;
        }
        else {
            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($response);

            return obj_to_array($xml);
        }
    }

    function formatAddress($address) {
        $map = [
            'City' => "city",
            'StateOrRegion' => "state",
            'PostalCode' => "zip",
            'order_id' => "streetLine1"
        ];
        $formatted_address = [];
        foreach ((array)$address as $key => $value) {
            if (key_exists($key, $map)) {
                $formatted_address[$map[$key]] = (string)$value;
            }
        }

        return $formatted_address;
    }

    function getItemsAmazon() {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "name = 'Amazon'");

        $this->getItems($config);
    }

    function getItemsAmazon315() {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "name = 'Amazon315'");

        $this->getItems($config);
    }

    private function getItems($config) {
        global $MysqlDb;

    }

    function updateItemQtyAmazon($sku = "") {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "name = 'Amazon'");

        $this->updateItemQty($config, $sku);
    }

    function updateItemQtyAmazon315($sku = "") {
        global $MysqlDb;


        $config = $MysqlDb->get_first_row("wc_sources", "*", "name = 'Amazon315'");

        $this->updateItemQty($config, $sku);
    }

    private function updateItemQty($parameters, $sku = ""){
        global $MysqlDb;

        if($sku == "") {
            $result = $MysqlDb->select("wc_items", "*", "source_id = ".(int)$parameters['id']." AND synced = 0 AND qb_sync_time IS NOT NULL");
        }
        else {
            $MysqlDb->prepare_vars['sku'] = $sku;
            $result = $MysqlDb->select("wc_items", "*", "source_id = ".(int)$parameters['id']." AND sku = '{{sku}}'");
        }

        $n = 0;
        $products = [];
        while ($row = $MysqlDb->get_result($result)) {
            $n ++;
            $products[] = [
                'id' => $row['id'],
                'sku' => $row['sku'],
                'qty' => (float)$row['store_qty'] < 0 ? 0 : (int)($row['store_qty'] / $row['mqty'])
            ];

            if($n == 100) {
                $this->sendItemsQty($products, $parameters);

                $n = 0;
                $products = [];
                sleep(10);
            }
        }

        if(count($products) > 0){
            $this->sendItemsQty($products, $parameters);
        }

        return "Success";
    }

    private function sendItemsQty($products, $parameters) {
        global $Html, $Router, $MysqlDb;

        $config = json_decode($parameters['parameters'], true);

        $param = [
            'AWSAccessKeyId' => $config['access_key'],
            'Action' => "SubmitFeed",
            'Merchant' => $config['seller_id'],
            'FeedType' => "_POST_INVENTORY_AVAILABILITY_DATA_",
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp' => gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version' => "2009-01-01",
            'MarketplaceIdList.Id.1' => $config['marketplace_id'],
            'PurgeAndReplace' => "false",
        ];
        if(isset($config['mws_auth_token'])) {
            $param['MWSAuthToken'] = $config['mws_auth_token'];
        }

        $url = array();
        foreach($param as $key => $val){
            $key = rawurlencode($key);
            $val = rawurlencode($val);
            $url[] = "{$key}={$val}";
        }
        sort($url);

        $arr   = implode("&", $url);

        $sign  = "POST\n";
        $sign .= "mws.amazonservices.com\n";
        $sign .= "/Feeds/{$param['Version']}\n";
        $sign .= $arr;

        $Html->content_data = [
            'seller_id' => $config['seller_id'],
            'products' => $products
        ];

        $Router->page = "amazonUpdateItems";
        $amazon_feed = $Html->generate_module_html();

        $signature = hash_hmac("sha256", $sign, $config['secret_key'], true);
        $httpHeader = [
            "Transfer-Encoding: chunked",
            "Content-Type: application/xml",
            "Content-MD5: " . base64_encode(md5($amazon_feed, true)),
            "Expect:",
            "Accept:",
        ];

        $signature = urlencode(base64_encode($signature));

        $link  = "https://mws.amazonservices.com/Feeds/{$param['Version']}?";
        $link .= $arr."&Signature=".$signature;

        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $amazon_feed);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        $errors = curl_error($ch);
        curl_close($ch);

        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($response);
        $Qb = new Qb();
        if($xml){
            if(
                isset($xml->children()->SubmitFeedResult) &&
                $xml->children()->SubmitFeedResult->
                children()->FeedSubmissionInfo->
                children()->FeedProcessingStatus == "_SUBMITTED_"
            ) {
                $products_id = [];
                foreach($products as $p) {
                    $products_id[] = $p['id'];
                }
                if(count($products_id) > 0) {
                    $fields = ['synced' => 1, 'sync_time' => current_time()];
                    $MysqlDb->update("wc_items", $fields, "id IN (" . implode(",", $products_id) . ")");
                }
            }
            else{
                $Qb->error_log($parameters['name'].": ".$response, $parameters['name']);
            }
        }

        if(strlen($errors) !== 0) {
            $Qb->error_log($parameters['name'].": ".print_r($errors, true), $parameters['name']);
        }
    }

    function getOrdersAmazon() {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 4");

        $this->getOrders($config);
    }

    function getOrdersAmazon315() {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 5");

        $this->getOrders($config);
    }

    function getOrders($parameters)
    {
        global $Html, $Router, $MysqlDb;

        $Qb = new Qb();
        $Orders = new Orders();

        $config = json_decode($parameters['parameters'], true);

        $parameters['version'] = "2013-09-01";

        $last_order_time = $Orders->last_sync_time($parameters['id']);

        $parameters['get_fields'] = [
            'LastUpdatedAfter' => gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", date_to_unix($last_order_time, "Y-m-d H:i:s")),
            'MarketplaceId.Id.1' => $config['marketplace_id'],
            'SellerId' => $config['seller_id'],
            'OrderStatus.Status.1' => "Unshipped",
            'OrderStatus.Status.2' => "PartiallyShipped",
            'OrderStatus.Status.3' => "Shipped",
        ];

        $result = $this->callApi("ListOrders", $parameters, $config);

dd($result);
        if ($result) {
            if (isset($result['ListOrdersResult']['Orders']['Order'])) {
                foreach ($result['ListOrdersResult']['Orders']['Order'] as $o) {
                    $items = $this->getOrderItems($o['AmazonOrderId'], $config);
                    $qb_items = $Qb->get_mapped_items($items['items']);

                    $o['ShippingAddress']['order_id'] = $o['AmazonOrderId'];

                    $fields = [
                        'source_id' => $parameters['id'],
                        'order_id' => $o['AmazonOrderId'],
                        'qb_ref_number' => substr($o['AmazonOrderId'], 0, strpos($o['AmazonOrderId'], "-", 8)),
                        'order_items' => $items['items'],
                        'customer' => $o['BuyerEmail'],
                        'discount' => "",
                        'is_pickup' => 0,
                        'shipping_price' => $items['shipping_price'],
                        'tax' => [
                            'rate' => 0,
                            'amount' => 0
                        ],
                        'shipping_address' => $this->formatAddress($o['ShippingAddress']),
                        'billing_address' => $this->formatAddress($o['ShippingAddress']),
                        'qb_items' => $qb_items,
                        'sync_pex' => 0,
                        'qb_status' => 0,
                        'completed' => (int)$o['OrderStatus'] == "Shipped" ? 1 : 0,
                        'order_update_time' => (string)$o['LastUpdateDate']
                    ];

                    $Orders->save_order($fields);

                }
            }
        }
    }

    function getOrderItems($order_id, $config) {
        $parameters['version'] = "2013-09-01";

        $parameters['get_fields'] = [
            'AmazonOrderId' => $order_id,
            'SellerId' => $config['seller_id'],
        ];

        $result = $this->callApi("ListOrderItems", $parameters, $config);

        if (!isset($result['ListOrderItemsResult']['OrderItems']['OrderItem'][0])) {
            $items[] = $result['ListOrderItemsResult']['OrderItems']['OrderItem'];
        }
        else {
            $items = $result['ListOrderItemsResult']['OrderItems']['OrderItem'];
        }

        $result = ['items' => [], 'tax' => 0, 'shipping_price' => 0];
        foreach ($items as $i) {
            $result['items'][] = [
                'sku' => $i['SellerSKU'],
                'title' => $i['Title'],
                'qty' => $i['QuantityOrdered'],
                'price' => $i['ItemPrice']['Amount']
            ];

            $result['tax'] += (float)$i['ShippingTax']['Amount'] + (float)$i['ItemTax']['Amount'];
            $result['shipping_price'] += (float)$i['ShippingPrice']['Amount'];
        }

        return $result;
    }
}
