<?php

namespace controller\cron;

use \model\cron\Cron;
use \model\cron\Orders;
use \model\wc\Qb;

class opencart extends \Controller {
    private $compete_orders_statuses = [
        6 => [5, 9, 12, 15],
        7 => [2, 3, 5, 15, 17]
    ];
    private $shipped_orders_statuses = [
        6 => 5,
        7 => 5
    ];

    function __construct() {
        global $MysqlDb;

        $Cron = new Cron();

        // check permission
        if(!$Cron->validate_access()){
            exit;
        }
    }

    /*
     * get orders from nationswarehouse
     */
    function getOrdersNW(){
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 6");

        return $this->getOrders($config);
    }

    /*
     * get orders from nationswarehouse
     */
    function getOrdersCanarsee(){
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 7");

        return $this->getOrders($config);
    }

    private function callApi($config, $action, $parameters = []) {
        $api_url = [
            $config['orders_api_url']."?",
            "username=".$config['username'],
            "password=".$config['password'],
            "action=".$action,
        ];

        if(count($parameters) > 0) {
            foreach ($parameters as $key => $value)
                $api_url[] = $key."=".$value;
        }

        $options=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        return file_get_contents(implode("&", $api_url), false, stream_context_create($options));
    }

    private function formatAddress($address) {
        $map = [
            'FirstName' => "firstName",
            'LastName' => "lastName",
            'Company' => "company",
            'Street1' => "streetLine1",
            'Street2' => "streetLine2",
            'City' => "city",
            'StateCode' => "state",
            'PostalCode' => "zip",
            'Phone' => "phone",
        ];
        $formatted_address = [];
        foreach ((array)$address as $key => $value) {
            if (key_exists($key, $map)) {
                $formatted_address[$map[$key]] = is_array($value) ? "" : $value;
            }
        }

        return $formatted_address;
    }

    private function getOrders($parameters){
        global $MysqlDb;

        $config = json_decode($parameters['parameters'], true);

        $Orders = new Orders();
        $Qb = new Qb();

        $last_order = $MysqlDb->get_first_row("wc_orders", "order_update_time", "source_id = ".(int)$parameters['id'], "order_update_time DESC");
        $last_order_time = $last_order !== false ? $last_order['order_update_time'] : current_date()." 00:00:00";
        $time_from = str_replace(" ", "T", $last_order_time);

        $order_count = strip_tags($this->callApi($config, "getcount", ['start' => $time_from]));

        if((int)$order_count > 0){
            $content = $this->callApi($config, "GetOrders", ['start' => $time_from]);

            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($content);
            $xml = obj_to_array($xml);

            if(!isset($xml['Orders']['Order'][0])){
                $xml['Orders']['Order'] = [$xml['Orders']['Order']];
            }
            foreach($xml['Orders']['Order'] as $o) {
                $items = [];
                if(!isset($o['Items']['Item'][0])){
                    $o['Items']['Item'] = [$o['Items']['Item']];
                }
                foreach ($o['Items']['Item'] as $i) {
                    $items[] = [
                        'title' => $i['Name'],
                        'sku' => $i['SKU'],
                        'qty' => $i['Quantity'],
                        'price' => $i['UnitPrice']
                    ];
                }
                $qb_items = $Qb->get_mapped_items($items);

                // shipping price and tax
                $shipping_price = 0;
                $tax = 0;
                $sub_total = 0;
                if(isset($o['orderTotals'])) {
                    foreach (json_decode($o['orderTotals'], true) as $ot) {
                        if ($ot['code'] == "shipping") {
                            $shipping_price = (string)$ot['value'];
                        }
                        if ($ot['code'] == "tax") {
                            $tax = (string)$ot['value'];
                        }
                        if ($ot['code'] == "sub_total") {
                            $sub_total = (string)$ot['value'];
                        }
                    }
                }

                $completed = in_array((int)$o['StatusCode'], $this->compete_orders_statuses[$parameters['id']]) ? 1 : 0;
                $shipped =  (int)$o['StatusCode'] == $this->shipped_orders_statuses[$parameters['id']] ? 1 : 0;
                $fields = [
                    'source_id' => $parameters['id'],
                    'order_id' => (int)$o['OrderNumber'],
                    'order_items' => $items,
                    'customer' => $o['CustomerEmail'],
                    'discount' => "",
                    'is_pickup' => 0,
                    'shipping_price' => $shipping_price,
                    'tax' => [
                        'rate' => $sub_total == 0 ? 0 : ($tax * 100) / $sub_total,
                        'amount' => $tax
                    ],
                    'shipping_address' => $this->formatAddress($o['ShippingAddress']),
                    'billing_address' => $this->formatAddress($o['BillingAddress']),
                    'qb_items' => $qb_items,
                    'qb_status' => 0,
                    'completed' => $completed,
                    'cancelled' => (int)$o['StatusCode'] == 7 ? 1 : 0,
                    'shipped' => $shipped,
                    'shipped_time' => (int)$o['StatusCode'] == $this->shipped_orders_statuses[$parameters['id']] ? (string)$o['LastModified'] : "_NULL_",
                    'order_create_time' => (string)$o['OrderDate'],
                    'order_update_time' => (string)$o['LastModified'],
                ];

                $Orders->save_order($fields);
            }
        }

        $this->checkOrdersStatus($parameters);

        return "Success";
    }

    /**
     * check if orders are completed
     * @param $parameters "source parameters"
     * @return orders_id
     */
    function checkOrdersStatus($parameters) {
        global $MysqlDb;

        $config = json_decode($parameters['parameters'], true);

        $orders_id = $MysqlDb->get_list("wc_orders", "order_id", "order_id", "source_id = ".(int)$parameters['id']." AND completed = 0");

        if (count($orders_id) > 0) {
            $content = $this->callApi($config, "getcompletedorders", ['orders_id' => implode(",", $orders_id)]);

            $completed_orders_id = explode(",", $content);
            $completed_orders_id = array_map("intval", $completed_orders_id);
            if(count($completed_orders_id) > 0) {
                $MysqlDb->update("wc_orders", ['completed' => 1], "source_id = ".(int)$parameters['id']." AND order_id IN (".implode(",", $completed_orders_id).")");
            }
        }
    }

    function syncStoreNW($sku = ""){
        $Qb = new Qb();
        $config = $Qb->get_source_config(6);

        return $this->syncStore($config, $sku);
    }

    function syncStoreCanarsee($sku = ""){
        $Qb = new Qb();
        $config = $Qb->get_source_config(7);

        return $this->syncStore($config, $sku);
    }

    /**
     * @param string $sku for "Sync now"
     * @return mixed|string
     */
    function syncStore($config, $sku = "")
    {
        global $MysqlDb;

        if($sku == "") {
            $result = $MysqlDb->select("wc_items", "*", "source_id = " . (int)$config['id'] . " AND synced = 0 AND qb_sync_time IS NOT NULL");
        }
        else {
            $MysqlDb->prepare_vars['sku'] = $sku;
            $result = $MysqlDb->select("wc_items", "*", "source_id = ".(int)$config['id']." AND sku = '{{sku}}'");
        }

        $products = [];
        while ($row = $MysqlDb->get_result($result)) {
            $quantity = (float)$row['store_qty'] < 0 ? 0 : (int)($row['store_qty'] / $row['mqty']);
            $products[] = [
                'part_number' => $row['sku'],
                'quantity' => $quantity
            ];
        }

        $curl = curl_init();
        Curl_setopt($curl, CURLOPT_SSLVERSION, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, [
            'api_key' => $config['parameters']['api_key'],
            'products' => json_encode($products)
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, '0');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, '0');
        curl_setopt($curl, CURLOPT_URL, $config['parameters']['items_api_url']);

        $result = curl_exec($curl);

        if (curl_errno($curl)) {
            $result = curl_errno($curl);
        }

        foreach ($products as $p) {
            $fields = [
                'synced' => 1,
                'sync_time' => current_time(),
                'response' => json_encode($result)
            ];

            $MysqlDb->prepare_vars['sku'] = $p['part_number'];
            $MysqlDb->update("wc_items", $fields, "source_id = ".(int)$config['id']." AND sku = '{{sku}}'");
        }

        curl_close($curl);

        return "Success";
    }

    function getProductsNW(){
        $Qb = new Qb();
        $config = $Qb->get_source_config(6);

        return $this->getProducts($config);
    }

    function getProductsCanarsee(){
        $Qb = new Qb();
        $config = $Qb->get_source_config(7);

        return $this->getProducts($config);
    }

    function getProducts($config) {
        global $MysqlDb;

        $last_update = $MysqlDb->get_first_row("wc_items", "source_update_time", "source_id = ".(int)$config['id'], "source_update_time DESC");
        $last_update_time = $last_update !== false ? $last_update['source_update_time'] : "1967-01-01 00:00:00";
        $time_from = str_replace(" ", "T", $last_update_time);

        $content = $this->callApi($config['parameters'], "GetProducts", ['last_update' => $time_from]);

        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($content);
        $xml = obj_to_array($xml);

        if(is_array($xml['products']) && !isset($xml['products'][0])){
            $xml['products'] = [$xml['products']];
        }
        foreach ($xml['products'] as $p) {
            $fields = [];
            $fields['title'] = $p['name'];
            $fields['sku'] = $p['sku'];
            $fields['source_sku'] = $p['sku'];
            $fields['source_qty'] = $p['quantity'];
            $fields['source_update_time'] = $p['date_modified'];

            if(!$MysqlDb->record_exist("wc_items", "source_id = ".(int)$config['id']." AND item_source_id = ".(int)$p['product_id'])){
                $fields['source_id'] = (int)$config['id'];
                $fields['item_source_id'] = $p['product_id'];
                $fields['add_time'] = current_time();
                $fields['mqty'] = 1;

                $MysqlDb->insert("wc_items", $fields);
            }
            else{
                $fields['update_time'] = current_time();
                $MysqlDb->update("wc_items", $fields, "source_id = ".(int)$config['id']." AND item_source_id = ".(int)$p['product_id']);
            }
        }

        return "success";
    }
}
