<?php

namespace controller\cron;

use \model\cron\Cron;
use \model\cron\Orders;
use \model\wc\Qb;


class pex extends \Controller {
    private $source_id = 1;

    function __construct() {
        $Cron = new Cron();

        // check permission
        if(!$Cron->validate_access()){
            exit;
        }
    }

    private function send_slack_notification($text) {
        $json = json_encode( [
            'username' => "PexServer",
            'text' => $text
        ]);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://hooks.slack.com/services/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",
            CURLOPT_USERAGENT => 'cURL Request',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array('payload' => $json),
        ));
        curl_exec($curl);
    }

    private function insert_server_log($status, $response = "") {
        global $MysqlDb;

        $last_record = $MysqlDb->get_first_row("pex_server_status", "id, server_status", "", "id DESC");

        if(!isset($last_record['server_status']) || $last_record['server_status'] !== $status) {
            $fields = [
                'server_status' => $status,
                'last_result' => $response,
                'start_time' => current_time(),
                'end_time' => current_time(),
                'add_time' => current_time()
            ];

            $MysqlDb->insert("pex_server_status", $fields);

            $this->send_slack_notification("Server is ".$status);
        }
        else {
            $fields = [
                'last_result' => $response,
                'end_time' => current_time(),
                'update_time' => current_time()
            ];

            $MysqlDb->update("pex_server_status", $fields, "id = ".(int)$last_record['id']);
        }
    }

    function checkServer()
    {
        $Qb = new Qb();

        $source_config = $Qb->get_source_config(1);

        for ($i = 0; $i < 5; $i++) {
            $result_json = curl_request(
                rtrim($source_config['parameters']['api_url'], "/qb") . "/cart",
                [
                    'type' => 0,
                    'token' => $source_config['parameters']['api_token']
                ]);

            $result = json_decode($result_json, true);

            if ($result && isset($result['status'])) {
                $status = "up";
                $this->insert_server_log("up");
                break;
            }
            else {
                $status = "down";

                sleep(1);
            }
        }

        if($status == "down") {
            $this->insert_server_log("down", $result);
        }

        return "Success";
    }

    function getItems(){
        global $MysqlDb;

        $Qb = new Qb();

        $source_config = $Qb->get_source_config(1);
        $last_update = $MysqlDb->max_value("wc_items", "source_update_time", "source_id = ".(int)$this->source_id);

        $result_json = curl_request(
            $source_config['parameters']['api_url'] . "/products/get_updates",
            [
                'fetchedAt' => $last_update == 0 ? "1967-01-01 00:00:00" : $last_update,
                'token' => $source_config['parameters']['api_token']
            ]);

        $result = json_decode($result_json, true);

        if(isset($result['status']) && $result['status'] == "OK") {
            foreach($result['products'] as $p) {
                $fields = [];
                $fields['title'] = $p['title'];
                $fields['sku'] = $p['part_number'];
                $fields['source_sku'] = $p['part_number'];
                $fields['price'] = $p['sell_price'];
                if(isset($p['published'])) {
                    $fields['active'] = $p['published'];
                }
                $fields['source_update_time'] = $p['updated_at'];

                if(!$MysqlDb->record_exist("wc_items", "source_id = ".(int)$this->source_id." AND item_source_id = ".(int)$p['id'])){
                    $fields['source_id'] = $this->source_id;
                    $fields['item_source_id'] = $p['id'];
                    $fields['add_time'] = current_time();
                    $fields['mqty'] = 1;

                    $MysqlDb->insert("wc_items", $fields);
                }
                else{
                    $fields['update_time'] = current_time();
                    $MysqlDb->update("wc_items", $fields, "source_id = ".(int)$this->source_id." AND item_source_id = ".(int)$p['id']);
                }

                // update shelf_id
                $MysqlDb->prepare_vars['part_number'] = $p['part_number'];
                $MysqlDb->update("quickbooks_item", ['shelf_id' => $p['shelf_id'], 'shelf_id_extra' => $p['shelf_id_extra']], "name = '{{part_number}}'");
            }
            return "success";
        }
        else {
            $Qb->error_log($result_json, "PEX");
        }

        return "success";
    }

    function getOrders() {
        $Orders = new Orders();
        $Qb = new Qb();

        $source_config = $Qb->get_source_config($this->source_id);

        $result_json = curl_request(
            $source_config['parameters']['api_url'] . "/orders/get",
            [
                'fetchedAt' => $Orders->last_sync_time($this->source_id),
                'token' => $source_config['parameters']['api_token']
            ]);

        $result = json_decode($result_json, true);

        if(is_array($result) && isset($result['orders'])){
            foreach($result['orders'] as $order){
                $qb_items = $Qb->get_mapped_items($order['items']);

                $fields = [
                    'source_id' => 1,
                    'order_id' => $order['id'],
                    'qb_ref_number' => $source_config['parameters']['qb_order_number_prefix'].$order['id'],
                    'order_items' => $order['items'],
                    'customer' => $order['email'],
                    'discount' => isset($order['coupon_amount']) ? $order['coupon_amount'] : "",
                    'is_pickup' => $order['is_pickup'],
                    'shipping_price' => $order['shipping_cost'],
                    'tax' => [
                        'rate' => $order['tax_rate'] + $order['extra_tax_rate'],
                        'amount' => $order['tax_amount'] + $order['extra_tax_amount']
                    ],
                    'shipping_address' => $order['shippingAddr'],
                    'billing_address' => $order['billingAddr'],
                    'qb_items' => $qb_items,
                    'qb_status' => isset($order['shippedAt']) && $order['shippedAt'] ? 4 : 0,
                    'print_time' => $order['shippedAt'],
                    'shipped_time' => isset($order['shippedAt']) ? $order['shippedAt'] : "_NULL_",
                    'shipped' => (isset($order['shippedAt']) && $order['shippedAt']) ||
                                ((int)$order['is_pickup'] == 1 && (int)$order['status_id'] == 10) ? 1 : 0,
                    'completed' => (int)$order['status_id'] >= 9 ? 1 : 0,
                    'cancelled' => (int)$order['status_id'] == 1 ? 1 : 0,
                    'order_create_time' => $order['created_at'],
                    'order_update_time' => $order['updated_at']
                ];

                $Orders->save_order($fields);
            }
        }
        return "Success";
    }

    /**
     * @param string $sku for "Sync now"
     * @return mixed|string
     */
    function updateStock(string $sku = "") {
        global $MysqlDb;

        $Qb = new Qb();

        $source_config = $Qb->get_source_config($this->source_id);

        if($sku == "") {
            $result = $MysqlDb->select("wc_items", "*", "source_id = ".(int)$this->source_id." AND synced = 0 AND qb_sync_time IS NOT NULL");
        }
        else {
            $MysqlDb->prepare_vars['sku'] = $sku;
            $result = $MysqlDb->select("wc_items", "*", "source_id = ".(int)$this->source_id." AND sku = '{{sku}}'");
        }

        $items = [];
        while ($row = $MysqlDb->get_result($result)) {
            $items['skuList'][] = [
                'sku' => $row['sku'],
                'inStock' => (float)$row['store_qty'] > 0 ? 1 : 0
            ];
        }

        if (count($items)) {
            $fields_string = "token=" . $source_config['parameters']['api_token'] . "&";
            $fields_string .= http_build_query($items);

            rtrim($fields_string, '&');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $source_config['parameters']['api_url'] . "/stock/update");
            curl_setopt($ch, CURLOPT_POST, count($items) + 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            $result_json = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($result_json, true);

            if (isset($result['status']) && $result['status'] == "OK") {
                if(is_array($result['items']) && count($result['items'])) {
                    foreach ($result['items'] as $sku => $response) {
                        $fields = [
                            'synced' => 1,
                            'sync_time' => current_time(),
                            'response' => json_encode($response)
                        ];

                        $MysqlDb->prepare_vars['sku'] = $sku;
                        $MysqlDb->update("wc_items", $fields, "source_id = ".(int)$this->source_id." AND sku = '{{sku}}'");
                    }
                }
            }
            else{
                return $result;
            }
        }

        return "Success";
    }

    function refreshQbItems(){
        global $Router, $MysqlDb;

        $Qb = new Qb();

        $order_info = $MysqlDb->get_first_row("wc_orders", "id, order_items", "id = ".$Router->get_int('id'));

        if($order_info) {
            $qb_items = $Qb->get_mapped_items(json_decode($order_info['order_items'], true));

            $MysqlDb->update("wc_orders", ['qb_items' => json_encode($qb_items, JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES)], "id = " . $Router->get_int('id'));

            return "Success";
        }
        else{
            return "Order doesn't exist";
        }
    }

    function getCompletedOrders() {
        global $MysqlDb;

        $Qb = new Qb();

        $unshipped_orders = $MysqlDb->get_list("wc_orders", "order_id", "order_id", "source_id = ".(int)$this->source_id." AND qb_status = 2 AND completed = 1 AND shipped = 0 AND cancelled = 0");

        if(count($unshipped_orders)) {
            $source_config = $Qb->get_source_config($this->source_id);

            $fields_string = "token=" . $source_config['parameters']['api_token'] . "&";
            $fields_string .= "order_ids=".implode(",", $unshipped_orders);

            rtrim($fields_string, '&');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $source_config['parameters']['api_url'] . "/orders/get_completed");
            curl_setopt($ch, CURLOPT_POST, 2);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            $result_json = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($result_json, true);

            if(count($result)) {
                $MysqlDb->update("wc_orders", ['shipped' => 1, 'shipped_time' => current_time()], "source_id = ".(int)$this->source_id." AND order_id IN (".implode(",", $result).")");
            }
        }
    }
}
