<?php

namespace controller\cron;

use \model\cron\Cron;

class run extends \Controller
{
    private $number_limits = [
        0 => [ // Minute
            0 => 0,
            1 => 59
        ],
        1 => [ // Hour
            0 => 0,
            1 => 23
        ],
        2 => [ // Day of the month
            0 => 1,
            1 => 31
        ],
        3 => [ // Month
            0 => 1,
            1 => 12
        ],
        4 => [ // Day of the week
            0 => 0,
            1 => 7
        ],
    ];

    function __construct()
    {
        global $MysqlDb;

        $Cron = new Cron();

        // check permission
        if(!$Cron->validate_access()){
            exit;
        }

        $result = $MysqlDb->select("cron", "*", "active = 1");
        $api_urls = [];
        while ($cron = $MysqlDb->get_result($result)) {
            $validate = $this->validate_schedule($cron['schedule']);

            if ($validate['status'] == "error") {
                $MysqlDb->update("cron", ['status_message' => $validate['message']], "id = " . (int)$cron['id']);
            }
            elseif (
                current_time() >= $this->next_run_time($cron['schedule']) &&
                $this->next_run_time($cron['schedule']) > $cron['last_run_time']
            ) {
                $api_urls[$cron['id']] = $cron['api_url'];
            }
        }

        if(count($api_urls) > 0) {
            $this->call_api($api_urls);
        }
    }

    function validate_schedule($schedule)
    {
        $schedule_parts = explode(' ', $schedule);

        if (count($schedule_parts) !== 5) {
            return ['status' => "error", 'message' => "Wrong schedule format"];
        }

        foreach ($schedule_parts as $index => $part) {
            $commaless_part = str_replace(",", "", $part, $count);
            // check format
            if (
                $part !== "*" &&
                !preg_match('/^[0-9]$/', $part) &&
                !preg_match('/^(\*)(\/)([0-9]){1,}$/', $part) &&
                !preg_match('/^([0-9-]+)$/', $part) &&
                !(is_numeric($commaless_part) && $count !== 0 && count(array_filter(explode(",", $part), "trim")) - 1 == $count)
            ) {
                return ['status' => "error", 'message' => "Wrong schedule format"];
                break;
            }

            // check values
            preg_match('/([0-9]){1,}/', $part, $matches);
            if (count($matches) > 0) {
                if ((int)$matches[0] < $this->number_limits[$index][0] || (int)$matches[0] > $this->number_limits[$index][1]) {
                    return ['status' => "error", 'message' => "Wrong minute format"];
                    break;
                }
            }
        }

        return ['status' => "ok"];
    }

    function next_run_time($schedule)
    {
        $schedule_par = explode(' ', $schedule);

        $schedule_types = [
            0 => ['alias' => "i", 'name' => "minute"],
            1 => ['alias' => "H", 'name' => "hour"],
            2 => ['alias' => "d", 'name' => "day"],
            3 => ['alias' => "m", 'name' => "month"],
        ];

        $future_time = []; // time we need to add to current time
        $time['Y'] = date("Y");
        $time['s'] = "00";

        // calculate next run time
        foreach ($schedule_types as $index => $type) {
            $alias = $type['alias'];
            // if value is '*'
            if ($schedule_par[$index] == "*") {
                $time[$alias] = date($alias);
            }
            // if value is numeric
            elseif (is_numeric($schedule_par[$index])) {
                $time[$alias] = sprintf('%02d', $schedule_par[$index]);
            }
            // if value is in '*/2' format
            elseif (strpos($schedule_par[$index], "/")) {
                $current_value = date($alias);

                // get string (number) after '*/'
                $period = (int)substr($schedule_par[$index], 2);

                $diff = $current_value < $period ? $period - $current_value : $period - ($current_value % $period);

                if ($diff !== $period && $current_value + $diff > $this->number_limits[$index][1]) {
                    $time[$alias] = $period;
                }
                else {
                    $time[$alias] = $current_value;

                    if ($diff !== $period) {
                        $future_time[] = "+" . $diff . " " . $type['name']; // ex: +3 minute
                    }
                }
            }
            // if value is in '1,2,8' format
            elseif (strpos($schedule_par[$index], ",")) {
                $current_value = date($alias);

                $time_array = explode(",", $schedule_par[$index]);
                sort($time_array);

                $time[$alias] = $current_value;

                if(!in_array($current_value, $time_array)) {
                    $time[$alias] = $current_value;

                    // just set time to the future
                    $future_time[] = "+1 " . $type['name']; // ex: +3 minute
                }
            }
            // if value is in 2-5 format
            elseif (strpos($schedule_par[$index], "-")) {
                $current_value = date($alias);

                $time_parts = explode("-", $schedule_par[$index]);
                $time_array = range($time_parts[0], $time_parts[1]);

                sort($time_array);

                $time[$alias] = $current_value;

                if(!in_array($current_value, $time_array)) {
                    $time[$alias] = $current_value;

                    // just set time to the future
                    $future_time[] = "+1 " . $type['name']; // ex: +3 minute
                }
            }
        }

        $date = $time['Y'] . "-" . $time['m'] . "-" . $time['d'] . " " . $time['H'] . ":" . $time['i'] . ":" . $time['s'];

        foreach ($future_time as $ft) {
            $date = current_time(strtotime($ft, date_to_unix($date, "Y-m-d H:i:s")));
        }

        return $date;
    }

    function call_api($api_urls)
    {
        global $MysqlDb;

        $ch = [];
        $mh = curl_multi_init();

        foreach ($api_urls as $i => $url) {
            $Cron = new Cron();

            $ch[$i] = curl_init();

            $url = str_replace('/cron/', "/", $url);
            $url = trim($url, '/');

            curl_setopt($ch[$i], CURLOPT_URL, config('site_url')."/cron/".$url);
            curl_setopt($ch[$i], CURLOPT_HEADER, 0);
            curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                'CHECKSUM: '.$Cron->gen_checksum(),
            ));

            curl_multi_add_handle($mh, $ch[$i]);
        }

        do {
            $status = curl_multi_exec($mh, $active);

          /*  if ($active) {
                // Wait a short time for more activity
                curl_multi_select($mh);
            }*/
        } while ($active && $status == CURLM_OK);

        foreach ($api_urls as $cron_id => $url) {
            $result = curl_multi_getcontent($ch[$cron_id]);

            $fields = [
                'last_run_time' => current_time(),
                'status_message' => $result
            ];
            $MysqlDb->update("cron", $fields, "id = ".(int)$cron_id);

            curl_multi_remove_handle($mh, $ch[$cron_id]);
        }

        curl_multi_close($mh);
    }
}
