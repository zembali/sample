<?php

namespace controller\cron;

use controller\text\text;
use \model\cron\Cron;
use \model\cron\Orders;
use \model\wc\Qb;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;


class ebay extends \Controller {

    function __construct() {
        global $MysqlDb;

        $Cron = new Cron();

        // check permission
        if(!$Cron->validate_access()){
            exit;
        }
    }

    function getItemsEbay() {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 2");

        return $this->getItems($config);
    }

    function getItemsEbay315() {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 3");

        return $this->getItems($config);
    }

    private function getItems($config) {
        global $MysqlDb;

        require ROOT_DIR.'/vendor/autoload.php';

        $service = new Services\TradingService(json_decode($config['parameters'], true));

        $request = new Types\GetMyeBaySellingRequestType();
        $request->ActiveList = new Types\ItemListCustomizationType();
        $request->ActiveList->Include = true;
        $request->ActiveList->Pagination = new Types\PaginationType();
        $request->ActiveList->Pagination->EntriesPerPage = 100;
        $request->ActiveList->Sort = Enums\ItemSortTypeCodeType::C_ITEMID_DESCENDING;
        $response = $service->getMyeBaySelling($request);

        if (isset($response->Errors)) {
            foreach ($response->Errors as $error) {
                printf(
                    "%s: %s\n%s\n\n",
                    $error->SeverityCode === Enums\SeverityCodeType::C_ERROR ? 'Error' : 'Warning',
                    $error->ShortMessage,
                    $error->LongMessage
                );
            }
        }
        $pages = $response->ActiveList->PaginationResult->TotalNumberOfPages;
        $saved_items_all = [];
        for ($p = 1; $p <= $pages; $p ++){
            if($p > 1){
                $request->ActiveList->Pagination->PageNumber = $p;
                $response = $service->getMyeBaySelling($request);
            }
            if ($response->Ack !== 'Failure' && isset($response->ActiveList)) {
                $items = $response->toArray();
                $saved_items = $this->saveItems($config['id'], $items['ActiveList']['ItemArray']['Item']);

                $saved_items_all = array_merge($saved_items_all, $saved_items);
            }
        }

        // mark inactive items
        if (count($saved_items_all) > 0) {
            $MysqlDb->update("wc_items", ['active' => 0], "source_id = ".(int)$config['id']." AND id NOT IN (".implode(",", $saved_items_all).")");
        }

        return "Success";
    }

    private function saveItems($source_id, $items, $item_source_id = false) {
        global $MysqlDb;

        $updated_items = [];
        foreach ($items as $item) {
            if(!isset($item['SKU'])){
                continue;
            }
            $sku_info = $this->skuMqty($item['SKU']);
            $is_id = $item_source_id == false ? $item['ItemID'] : $item_source_id;

            $price = 0;
            if (isset($item['BuyItNowPrice'])) {
                $price = $item['BuyItNowPrice']['value'];
            }
            elseif (isset($item['SellingStatus']) && isset($item['SellingStatus']['CurrentPrice'])) {
                $price = $item['SellingStatus']['CurrentPrice']['value'];
            }
            elseif (isset($item['StartPrice'])) {
                $price = $item['StartPrice']['value'];
            }
            $insert_fields = [
                'source_id' => $source_id,
                'item_source_id' => $is_id,
                'source_sku' => $item['SKU'],
                'sku' => $sku_info['sku'],
                'mqty' => $sku_info['mqty'],
                'price' => $price,
                //'source_qty' => $item['Quantity'],
                'title' => $item['Title'] ?? $item['VariationTitle'],
                'variation' => isset($item['Title']) ? 0 : 1
            ];

            $MysqlDb->prepare_vars['item_source_id'] = $is_id;
            $MysqlDb->prepare_vars['source_sku'] = $item['SKU'];
            $where = "source_id = ".(int)$source_id." AND item_source_id = '{{item_source_id}}'";
            $where .= isset($item['variations']) || isset($item['VariationTitle']) ? " AND source_sku = '{{source_sku}}'" : " AND variation = 0";

            $item_info = $MysqlDb->get_first_row("wc_items", "id", $where);

            if (!isset($item_info['id'])) {
                $insert_fields['add_time'] = current_time();

                $updated_items[] = $MysqlDb->insert("wc_items", $insert_fields);
            }
            else{
                /*$stop_sync = 1;
                break;*/
                $insert_fields['update_time'] = current_time();
                $MysqlDb->update("wc_items", $insert_fields, $where);

                $updated_items[] = $item_info['id'];
            }

            // Variations
            if (isset($item['Variations'])) {
                $variation_items = $this->saveItems($source_id, $item['Variations']['Variation'], $is_id);
                $updated_items = array_merge($updated_items, $variation_items);
            }
        }

        return $updated_items;
    }

    /**
     * @param $sku
     * @return array ['sku', 'mqty']
     */
    private function skuMqty($sku){
        $sku_parts = explode("*", $sku);

        if(count($sku_parts) > 1) {
            return [
                'sku' => $sku_parts[0],
                'mqty' => (int)$sku_parts[1] > 0 ? (int)$sku_parts[1] : 1
            ];
        }
        else{
            $sku_parts = explode("---", $sku);
            return [
                'sku' => $sku_parts[0],
                'mqty' => 1
            ];
        }
    }

    function updateItemQtyEbay($sku = "") {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 2");

        return $this->updateItemQty($config, $sku);
    }

    function updateItemQtyEbay315($sku = "") {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 3");

        return $this->updateItemQty($config, $sku);
    }

    private function sendItemsQty($products, $config) {
        global $MysqlDb;

        $service = new Services\TradingService(json_decode($config['parameters'], true));
        $request = new Types\ReviseInventoryStatusRequestType();

        $products_id = [];
        foreach ($products as $p) {
            $inventoryStatus = new Types\InventoryStatusType();
            $inventoryStatus->ItemID = $p['item_source_id'];
            $inventoryStatus->SKU = $p['source_sku'];
            $inventoryStatus->Quantity = (float)$p['store_qty'] < 0 ? 0 : (int)($p['store_qty'] / $p['mqty']);
            $request->InventoryStatus[] = $inventoryStatus;

            $products_id[] = $p['id'];
        }
        $request->ErrorLanguage = 'en_US';
        $request->WarningLevel = 'High';

        $response = $service->reviseInventoryStatus($request);

        $callback = json_decode($response, true);
        $Qb = new Qb();

        // success
        if(isset($callback['InventoryStatus'])) {
            foreach ($callback['InventoryStatus'] as $i) {
                $MysqlDb->update("wc_items", ['synced' => 1, 'sync_time' => current_time(), 'response' => "Success"], "item_source_id = '".$i['ItemID']."'");
            }
        }

        // errors and warnings
        if(isset($callback['Errors'])) {
            foreach ($callback['Errors'] as $e) {
                $synced = $e['SeverityCode'] == "Warning" ? 1 : 0;
                $MysqlDb->prepare_vars['item_source_id'] = $e['ErrorParameters'][0]['Value'];
                $MysqlDb->update("wc_items", ['synced' => $synced, 'sync_time' => current_time(), 'response' => json_encode($e)], "source_id = ".(int)$config['id']." AND item_source_id = '{{item_source_id}}'");

                if($e['SeverityCode'] !== "Warning") {
                    $Qb->error_log($config['name'].": ".json_encode($e)."\nParameters:".print_r($request->InventoryStatus, true), $config['name']);

                    //*** delete ended item
                    if($e['SeverityCode'] == "Error" && $e['ErrorCode'] == "21916750") {
                        $MysqlDb->delete("wc_items", "source_id = ".(int)$config['id']." AND item_source_id = '".$e['ErrorParameters'][0]['Value']."'");
                    }
                }
            }
        }

        return "Success";
    }

    private function updateItemQty($config, $sku = ""){
        global $MysqlDb;

        require ROOT_DIR.'/vendor/autoload.php';

        if($sku == "") {
            $result = $MysqlDb->select("wc_items", "*", "source_id = ".(int)$config['id']." AND synced = 0 AND qb_sync_time IS NOT NULL");
        }
        else {
            $MysqlDb->prepare_vars['sku'] = $sku;
            $result = $MysqlDb->select("wc_items", "*", "source_id = ".(int)$config['id']." AND sku = '{{sku}}'");
        }
        $n = 0;
        $products = [];
        while ($row = $MysqlDb->get_result($result)) {
            $n++;
            $products[] = $row;
            if ($n == 4) {
                $this->sendItemsQty($products, $config);
                $products = [];
                $n = 0;
            }
        }

        if(count($products) > 0) {
            $this->sendItemsQty($products, $config);
        }

        return "Success";
    }

    private function sendUpdatePrice($products, $config) {
        global $MysqlDb;

        $service = new Services\TradingService(json_decode($config['parameters'], true));
        $request = new Types\ReviseInventoryStatusRequestType();

        $products_id = [];
        $n = 0;
        foreach ($products as $p) {

            $inventoryStatus = new Types\InventoryStatusType();
            $inventoryStatus->ItemID = $p['item_source_id'];
            $inventoryStatus->SKU = $p['sku'];
            $price = new \DTS\eBaySDK\Trading\Types\AmountType();
            $price->currencyID = "USD";
            $price->value = (float)$p['price'];
            $inventoryStatus->StartPrice = $price;
            $request->InventoryStatus[] = $inventoryStatus;

            $products_id[] = $p['id'];

            $n ++;
        }
        $request->ErrorLanguage = 'en_US';
        $request->WarningLevel = 'High';

        $response = $service->reviseInventoryStatus($request);

        $callback = $response->toArray();
        $Qb = new Qb();

        // success
        if(isset($callback['InventoryStatus'])) {
            foreach ($callback['InventoryStatus'] as $i) {
                $MysqlDb->prepare_vars['item_source_id'] = $i['ItemID'];
                $MysqlDb->update("wc_price_change ", [
                    'sync_status' => 1,
                    'sync_time' => current_time(),
                    'response' => "Success"
                ],
                    "upload_id = " . (int)$products[0]['upload_id'] . " AND item_source_id = '{{item_source_id}}'");
            }
        }

        // errors and warnings
        if(isset($callback['Errors'])) {
            foreach ($callback['Errors'] as $e) {
                $MysqlDb->prepare_vars['item_source_id'] = isset($e['ErrorParameters']) ? $e['ErrorParameters'][0]['Value'] : "";
                $MysqlDb->update("wc_price_change", [
                    'sync_status' => 2,
                    'sync_time' => current_time(),
                    'response' => json_encode($e)
                ],
                    "upload_id = " . (int)$products[0]['upload_id'] . " AND item_source_id = '{{item_source_id}}'");

                if($e['SeverityCode'] !== "Warning") {
                    $Qb->error_log($config['name'] . ": " . json_encode($e) . "\nParameters:" . print_r($request->InventoryStatus,
                            true), $config['name']);
                }
            }
        }

        return "Success";
    }

    private function updatePrice($config, $products) {
        global $MysqlDb;

        require ROOT_DIR.'/vendor/autoload.php';

        $n = 0;
        $update_products = [];
        foreach ($products as $p) {
            $n++;
            $update_products[] = $p;
            if ($n == 4) {
                $this->sendUpdatePrice($update_products, $config);
                $update_products = [];
                $n = 0;
            }
        }

        if(count($update_products) > 0) {
            $this->sendUpdatePrice($update_products, $config);
        }

        return "Success";
    }

    function updatePriceEbay($products) {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 2");

        return $this->updatePrice($config, $products);
    }

    function updatePriceEbay315($products) {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 3");

        return $this->updatePrice($config, $products);
    }

    function getOrdersEbay() {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 2");

        return $this->getOrders($config);
    }

    function getOrdersEbay315() {
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 3");

        return $this->getOrders($config);
    }

    private function getOrderItemDetails($variations){
        $details = [];
        foreach ($variations[0]['NameValueList'] as $v) {
            if($v['Name'] == "Qty"){
                continue;
            }
            $details[] = $v['Name'].": ".$v['Value'][0];
        }

        return implode(";", $details);
    }

    private function getOrderItems($raw_items, $type = "") {
        $items = [];
        foreach ($raw_items as $i) {
            $ebay_sku = $i['Variation']['SKU'] ?? $i['Item']['SKU'];
            $sku_mqty = $this->skuMqty($ebay_sku);
            $items[] = [
                'sku' => $type == "qb" ? $sku_mqty['sku'] : $ebay_sku,
                'title' => $type == "qb" && (int)$sku_mqty['mqty'] !== 0
                    ? preg_replace("/^\(".$sku_mqty['mqty']."\)/", "", $i['Item']['Title'])
                    : $i['Item']['Title'],
                'details' => isset($i['Variation']['VariationSpecifics']) ? $this->getOrderItemDetails($i['Variation']['VariationSpecifics']) : "",
                'qty' => $type == "qb" ? $sku_mqty['mqty'] * $i['QuantityPurchased'] : $i['QuantityPurchased'],
                'price' => $type == "qb" ? $i['TransactionPrice']['value'] / $sku_mqty['mqty'] : $i['TransactionPrice']['value'],
                'item_id' => $i['Item']['ItemID'],
                'transaction_id' => $i['TransactionID'],
            ];
        }

        return $items;
    }

    function formatAddress($address) {
        $map = [
            'Name' => "firstName",
            'CityName' => "city",
            'StateOrProvince' => "state",
            'PostalCode' => "zip",
            'Street1' => "streetLine1",
            'Street2' => "streetLine2",
            'Phone' => "phone"
        ];
        $formatted_address = [];
        foreach ((array)$address as $key => $value) {
            if (key_exists($key, $map)) {
                $formatted_address[$map[$key]] = (string)$value;
            }
        }

        return $formatted_address;
    }

    private function getOrders($parameters) {
        require ROOT_DIR.'/vendor/autoload.php';

        $Qb = new Qb();
        $Orders = new Orders();

        $last_order_time = $Orders->last_sync_time($parameters['id']);

        $config = json_decode($parameters['parameters'], true);
        $service = new Services\TradingService($config);

        $request = new Types\GetOrdersRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $config['authToken'];
        $request->ModTimeFrom = new \DateTime(str_replace(" ", "T", $last_order_time).".000Z");
        //$request->ModTimeTo = new \DateTime("2021-04-26T18:10:00.000Z");
        $request->Pagination = new Types\PaginationType();
        $detail_level = Enums\DetailLevelCodeType::C_RETURN_ALL;
        $request->DetailLevel = [$detail_level];
        $response = $service->getOrders($request);

        if (isset($response->Errors)) {
            foreach ($response->Errors as $error) {
                printf(
                    "%s: %s\n%s\n\n",
                    $error->SeverityCode === Enums\SeverityCodeType::C_ERROR ? 'Error' : 'Warning',
                    $error->ShortMessage,
                    $error->LongMessage
                );
            }
        }

        $pages = $response->PaginationResult->TotalNumberOfPages;

        for ($p = 1; $p <= $pages; $p ++) {
            if($p > 1){
                $request->Pagination->PageNumber = $p;
                $response = $service->getOrders($request);
            }
            $ebay_orders = $response->toArray();

            if (isset($ebay_orders['ReturnedOrderCountActual']) && (int)$ebay_orders['ReturnedOrderCountActual'] !== 0) {
                foreach ($ebay_orders['OrderArray']['Order'] as $o) {
                    if(isset($o['CheckoutStatus']['Status']) && ($o['CheckoutStatus']['Status'] == "Complete" || $o['OrderStatus'] == "Cancelled")) {
                        $items_orig = $this->getOrderItems($o['TransactionArray']['Transaction']);
                        $items = $this->getOrderItems($o['TransactionArray']['Transaction'], "qb");
                        $qb_items = $Qb->get_mapped_items($items);

                        $fields = [
                            'source_id' => $parameters['id'],
                            'order_id' => $o['OrderID'],
                            'qb_ref_number' => $o['ShippingDetails']['SellingManagerSalesRecordNumber'],
                            'order_items' => $items,
                            'order_items_orig' => $items_orig,
                            'customer' => $o['BuyerUserID'],
                            'discount' => "",
                            'is_pickup' => 0,
                            'shipping_price' => isset($o['ShippingServiceSelected']) && $o['ShippingServiceSelected']['ShippingService'] !== "InternationalPriorityShipping"
                                ? $o['ShippingServiceSelected']['ShippingServiceCost']['value']
                                : 0,
                            'tax' => [
                                'rate' => 0,
                                'amount' => 0
                            ],
                            'shipping_address' => $this->formatAddress($o['ShippingAddress']),
                            'billing_address' => $this->formatAddress([
                                'Name' => $o['BuyerUserID'],
                                'Street1' => $o['TransactionArray']['Transaction'][0]['Buyer']['Email'],
                                'Phone' => $o['OrderID'],
                            ]),
                            'qb_items' => $qb_items,
                            'qb_status' => isset($o['ShippedTime']) ? 4 : 0, //for older orders which already are in the Quickbooks
                            'print_time' => isset($o['ShippedTime']) ? str_replace("T", " ", $o['ShippedTime']) : "_NULL_",
                            'shipped_time' => isset($o['ShippedTime']) ? str_replace("T", " ", $o['ShippedTime']) : "_NULL_",
                            'shipped' => isset($o['ShippedTime']) ? 1 : 0,
                            'completed' => $o['OrderStatus'] == "Completed" ? 1 : 0,
                            'cancelled' => $o['OrderStatus'] == "Cancelled" || $o['OrderStatus'] == "CancelPending" ? 1 : 0,
                            'order_create_time' => str_replace("T", " ",
                                substr($o['CreatedTime'], 0, 19)),
                            'order_update_time' => str_replace("T", " ",
                                substr($o['CheckoutStatus']['LastModifiedTime'], 0, 19))
                        ];

                        $Orders->save_order($fields);
                    }
                }
            }
        }
        return "Success";
    }

    function setPrintedNoteEbay(){
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 2");

        return $this->setPrintedNote($config);
    }

    function setPrintedNoteEbay315(){
        global $MysqlDb;

        $config = $MysqlDb->get_first_row("wc_sources", "*", "id = 3");

        return $this->setPrintedNote($config);
    }

    /**
     * sets "printed m/d" in orders "My Note"
     * @param $config
     * @return string
     */
    private function setPrintedNote($config)
    {
        global $MysqlDb;

        require ROOT_DIR . '/vendor/autoload.php';

        $service = new Services\TradingService(json_decode($config['parameters'], true));

        $request = new Types\SetUserNotesRequestType();

        $result = $MysqlDb->select("wc_orders", "id, order_items", "source_id = " . (int)$config['id'] . " AND cancelled = 0 AND print_time IS NOT NULL AND sync_print_note IS NULL");

        while ($row = $MysqlDb->get_result($result)) {
            $order_items = json_decode($row['order_items'], true);

            foreach ($order_items as $oi) {
                $request->OrderLineItemID = $oi['item_id']."-".$oi['transaction_id'];
                $request->TransactionID = $oi['transaction_id'];
                $request->ItemID = $oi['item_id'];
                $action = new Enums\SetUserNotesActionCodeType();
                $request->Action = $action::C_ADD_OR_UPDATE;
                $request->NoteText = "Printed ".date("m/d");

                $response = $service->setUserNotes($request);

                if (isset($response->Errors)) {
                    foreach ($response->Errors as $error) {
                        printf(
                            "%s: %s\n%s\n\n",
                            $error->SeverityCode === Enums\SeverityCodeType::C_ERROR ? 'Error' : 'Warning',
                            $error->ShortMessage,
                            $error->LongMessage
                        );
                    }
                }
                elseif ($response->Ack == "Success") {
                    $MysqlDb->update("wc_orders", ["sync_print_note" => current_time()], "id = ".(int)$row['id']);
                }
            }
        }

        return "Success";
    }
}
