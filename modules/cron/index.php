<?php

$accepted_pages = [
    'run',
    'pex',
    'common',
    'ebay',
    'amazon',
    'opencart'
];

$Router->load_module_page($accepted_pages);
