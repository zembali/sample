<?= "<?xml version=\"1.0\" encoding=\"utf-8\"?>" ?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
    <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier><?= $seller_id ?></MerchantIdentifier>
    </Header>
    <MessageType>Inventory</MessageType>
    <? $n = 0 ?>
    <? foreach ($products as $p): ?>
        <? $n ++ ?>
        <Message>
            <MessageID><?= $n ?></MessageID>
            <OperationType>Update</OperationType>
            <Inventory>
                <SKU><?= $p['sku'] ?></SKU>
                <Quantity><?= $p['qty'] ?></Quantity>
            </Inventory>
        </Message>
    <? endforeach; ?>
</AmazonEnvelope>
