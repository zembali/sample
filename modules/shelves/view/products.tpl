<div class="border border-top-0 border-left-0 border-right-0">
    <h3 class="float-left"><?= $shelf ?></h3>
    <a class="float-right btn btn-light" href="/<?= $module ?>/">
        <i class="fas fa-long-arrow-alt-left"></i> Back
    </a>
    <div class="clearfix-10"></div>
</div>
<div class="clearfix-20"></div>
<form id="add_list_item_form" class="form-group" method="post"
      action="/post/<?= $module ?>/products/add_product/?shelf_id=<?= $Router->get_int('shelf_id') ?>&edit_id=<?= $Router->get_int('edit_id') ?>"
      onsubmit="return false">
    <div class="clearfix"></div>
    <div class="form-group">
        <?= $Form->input_form("part_number", "text", $edit_values, "", "display: inline-block; width: 65%",
            "form-control", "autocomplete='off'") ?>
        <input type="hidden" id="force_add" name="force_add" value="0">
        <button id="submit" type="submit" class="btn btn-primary" style="display: inline-block">Submit</button>
    </div>
</form>

<div class="m-t-20">
    <table class="table m-t-20">
            <? foreach ($products as $p): ?>
                <tr id="row_<?= $p['id'] ?>">
                    <td style="width: 80%">
                        <?= $p['part_number'] ?>
                    </td>
                    <td>
                        <i data-id="<?= $p['id'] ?>"
                           class="checked fas fa-check-circle pointer <?= (int)$p['checked'] ? "text-success" : "" ?>"></i>
                    </td>
                    <td>
                        <a href="/<?= $module ?>/products/?shelf_id=<?= $p['shelf_id'] ?>&edit_id=<?= $p['id'] ?>"">
                            <i class="fa fa-edit text-primary"></i>
                        </a>
                    </td>
                    <td>
                        <i class="fa fa-trash text-danger pointer delete_product" data-id="<?= $p['id'] ?>"></i>
                    </td>
                </tr>
            <? endforeach; ?>
    </table>
</div>

<script>
    $("#part_number").focus();

    $(".delete_product").click(function () {
        if (confirm('Are you sure?')) {
            var product_id = $(this).data('id');
            $.get("/post/<?= $module ?>/products/delete_product", {product_id: product_id})
                .done(function (data) {
                    if (data == "ok") {
                        $("#row_" + product_id).remove();
                    }
                    else {
                        alert(data);
                    }
                })
        }
    })

    $("#add_list_item_form").submit(function () {
        post_call(this, function () {
            $("#force_add").val(0);
        });
    })

    $(document).on("click", ".force", function () {
        $("#force_add").val(1);
        $("#add_list_item_form").submit();
    })

    $(".checked").click(function () {
        var obj = $(this);
        $.get("/post/<?= $module ?>/products/checked/", {edit_id: obj.data('id')})
            .done(function (data) {
                if(data == "checked"){
                    obj.addClass("text-success");
                }
                else{
                    obj.removeClass("text-success");
                }
            })
    })
</script>

