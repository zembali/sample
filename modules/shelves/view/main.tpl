<a href="/body/<?= $module ?>/main/add_shelf_form" class="btn btn-primary float-left" data-toggle="modal"
   data-target="#popup">Add
    Shelf</a>
<div class="float-right">
    <button id="search" class="btn btn-primary float-right">
        <i class="fa fa-search"></i>
    </button>
    <input id="part_n" name="part_n" type="text" class="form-control float-right" style="width: 120px">
</div>
<div class="clearfix"></div>

<div class="m-t-20">
    <table class="table m-t-20">
        <? foreach ($shelf_numbers as $shn): ?>
            <tr>
                <th colspan="4"><?= $shn ?></th>
            </tr>
            <? foreach ($shelves[$shn] as $shelf): ?>
                <tr id="row_<?= $shelf['id'] ?>">
                    <td style="width: 60%">
                        <span class="text-muted"><?= $shelf['num'] ?> -</span>
                        <a href="/<?= $module ?>/products/?shelf_id=<?= $shelf['id'] ?>"><?= $shelf['section'] ?></a>
                    </td>
                    <td>
                        <span class="<?= isset($checked[$shelf['id']]) && (int)$checked[$shelf['id']] == 1 ? "text-success" : "" ?>">
                            <?= isset($count[$shelf['id']]) ? "[" . $count[$shelf['id']] . "]" : "<span class='text-muted'>[0]</span>" ?>
                        </span>
                    </td>
                    <td>
                        <a href="/body/<?= $module ?>/main/add_shelf_form/?shelf_id=<?= $shelf['id'] ?>"
                           data-toggle="modal" data-target="#popup">
                            <i class="fa fa-edit text-primary"></i>
                        </a>
                    </td>
                    <td>
                        <i class="fa fa-trash text-danger pointer delete_shelf" data-id="<?= $shelf['id'] ?>"></i>
                    </td>
                </tr>
            <? endforeach; ?>
        <? endforeach; ?>
    </table>
</div>

<script>
    $(".delete_shelf").click(function () {
        if (confirm('Are you sure?')) {
            var shelf_id = $(this).data('id');
            $.get("/post/<?= $module ?>/main/delete_shelf", {shelf_id: shelf_id})
                .done(function (data) {
                    if (data == "ok") {
                        $("#row_" + shelf_id).remove();
                    } else {
                        alert(data);
                    }
                })
        }
    })

    $("#search").click(function () {
        $.get("/body/<?= $module ?>/main/search/?part_n=" + obj_value('part_n'))
            .done(function (data) {
                alert(data)
            })
    })
</script>
