<div class="modal-header">
    <h4 class="modal-title pull-left">Add/Edit Shelf</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <form id="add_list_item_form" class="form-horizontal" method="post"
          action="/post/<?= $module ?>/main/add_shelf/?shelf_id=<?= $Router->get_int('shelf_id') ?>"
          onsubmit="return false">
        <div class="form-group">
            <label for="num">Shelf Num:</label>
            <?= $Form->input_form("num", "text", $edit_values, "", "", "form-control") ?>
        </div>
        <div class="form-group">
            <label for="section">Shelf Section:</label>
            <?= $Form->input_form("section", "text", $edit_values, "", "", "form-control") ?>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>

<script>

    $("#add_list_item_form").submit(function () {
        post_call(this);
    })
</script>
