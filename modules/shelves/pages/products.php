<?php
namespace controller\shelves;

class products extends \Controller {

    function add_product() {
        global $Router, $MysqlDb;

        if($Router->post('part_number') == false){
            echo request_callback([
                'status' => "error",
                'errors' => ['part_number' => "Field is empty"],
            ]);
            exit;
        }

        $MysqlDb->prepare_vars['part_number'] = $Router->post('part_number');
        $product_info = $MysqlDb->get_first_row("shelves_products", "*", "part_number = '{{part_number}}' AND id != ".$Router->get_int('edit_id'));
        if($Router->post('force_add') == 0 && isset($product_info['id'])) {
            $shelf_info = $MysqlDb->get_first_row("shelves", "*", "id = ".(int)$product_info['shelf_id']);
            echo request_callback([
                'status' => "error",
                'errors' => ['general' => "part_number exists (".$shelf_info['num']."-".$shelf_info['section'].") <button type='button' class='btn btn-warning btn-sm force'>Force add</button>"],
            ]);
            exit;
        }

        if($Router->post('force_add') == 0 && $MysqlDb->record_exist("quickbooks_item", "Name = '{{part_number}}'") == false){
            echo request_callback([
                'status' => "error",
                'errors' => ['general' => "Sku doesn't exist <button type='button' class='btn btn-warning btn-sm force'>Force add</button>"],
            ]);
            exit;
        }

        $fields['shelf_id'] = $Router->get_int('shelf_id');
        $fields['part_number'] = $Router->post('part_number');
        if($Router->get_int('edit_id') == 0) {
            $MysqlDb->insert("shelves_products", $fields);
        }
        else{
            $MysqlDb->update("shelves_products", $fields, "id = ".$Router->get_int('edit_id'));
        }

        echo request_callback([
            'status' => "success",
            'redirect_url' => "/".$Router->module."/products/?shelf_id=".$Router->get_int('shelf_id')
        ]);
    }

    function delete_product() {
        global $Router, $MysqlDb;

        $MysqlDb->delete("shelves_products", "id = " . $Router->get_int('product_id'));

        echo "ok";
        exit;
    }

    function checked() {
        global $Router, $MysqlDb;

        $MysqlDb->update("shelves_products", ['checked' => "_INVERSE_"], "id = ".$Router->get_int('edit_id'));

        $product_info = $MysqlDb->get_first_row("shelves_products", "checked", "id = ".$Router->get_int('edit_id'));

        if($product_info){
            echo (int)$product_info['checked'] == 1 ? "checked" : "unchecked";
        }
        exit;
    }

    function _html() {
        global $Router, $Html, $MysqlDb;

        $edit_values = [];
        if($Router->get_int('edit_id') !== 0) {
            $edit_values = $MysqlDb->get_first_row("shelves_products", "*", "id = ".$Router->get_int('edit_id'));
        }


        $products = $MysqlDb->select("shelves_products", "*", "shelf_id = ".$Router->get_int('shelf_id'), "id DESC");
        $shelf_info = $MysqlDb->get_first_row("shelves", "*", "id = ".$Router->get_int('shelf_id'));

        $Html->content_data = [
            'edit_values' => $edit_values,
            'products' => $products,
            'shelf' => $shelf_info['num']."-".$shelf_info['section'],
        ];
        parent::_html();
    }
}
