<?php
namespace controller\shelves;

class main extends \Controller {

    function add_shelf_form(){
        global $Router, $Html, $MysqlDb;

        $edit_values = [];
        if($Router->get_int('shelf_id') !== 0) {
            $edit_values = $MysqlDb->get_first_row("shelves", "*", "id = ".$Router->get_int('shelf_id'));
        }

        $Html->content_data = ['edit_values' => $edit_values];

        $Router->page = "add_shelf";
        parent::_html();
    }

    function add_shelf(){
        global $Router, $MysqlDb;

        if($Router->post('num') == false){
            echo request_callback([
                'status' => "error",
                'errors' => ['num' => "Field is empty"],
            ]);
            exit;
        }
        if($Router->post('section') == false){
            echo request_callback([
                'status' => "error",
                'errors' => ['section' => "Field is empty"],
                'ok_fields' => 'num'
            ]);
            exit;
        }

        $MysqlDb->prepare_vars['num'] = $Router->post('num');
        $MysqlDb->prepare_vars['section'] = $Router->post('section');
        if($MysqlDb->record_exist("shelves", "num = '{{num}}' AND section = '{{section}}' AND id != ".$Router->get_int('shelf_id'))) {
            echo request_callback([
                'status' => "error",
                'errors' => ['' => "Shelf exists"],
                'ok_fields' => ['num', 'section']
            ]);
            exit;
        }

        $fields['num'] = $Router->post('num');
        $fields['section'] = $Router->post('section');

        if($Router->get_int('shelf_id') == 0) {
            $shelf_id = $MysqlDb->insert("shelves", $fields);
        }
        else {
            $shelf_id = $Router->get_int('shelf_id');
            $MysqlDb->update("shelves", $fields, "id = ".$shelf_id);
        }

        echo request_callback([
            'status' => "success",
            'redirect_url' => "/".$Router->module."/products/?shelf_id=".$shelf_id
        ]);
    }

    function delete_shelf(){
        global $Router, $MysqlDb;
        if ($MysqlDb->record_exist("shelves_products", "shelf_id = ".$Router->get_int('shelf_id'))){
            echo "The shelf is not empty";
            exit;
        }
        else{
            $MysqlDb->delete("shelves", "id = ".$Router->get_int('shelf_id'));

            echo "ok";
            exit;
        }
    }

    function search() {
        global $Router, $MysqlDb, $Html;

        $MysqlDb->prepare_vars['part_number'] = $Router->get('part_n');
        $product_info = $MysqlDb->get_first_row("shelves_products", "*", "part_number = '{{part_number}}'");
        if($product_info) {
            $shelf_info = $MysqlDb->get_first_row("shelves", "*", "id = " . (int)$product_info['shelf_id']);

            echo "Shelf: ".$shelf_info['num']."-".$shelf_info['section'];
        }
        else {
            echo "Can't find";
        }
        exit;
    }

    function _html() {
        global $Router, $Html, $MysqlDb;

        // count
        $count = [];
        $result_count = $MysqlDb->select("shelves_products", "shelf_id, COUNT(*) as p_count", 1, "", "", "shelf_id");
        while($row_count = $MysqlDb->get_result($result_count)){
            $count[$row_count['shelf_id']] = $row_count['p_count'];
            $checked[$row_count['shelf_id']] = !$MysqlDb->record_exist("shelves_products", "checked = 0 AND shelf_id = ".(int)$row_count['shelf_id']);
        }


        $result_shelves = $MysqlDb->select("shelves", "*", 1, "num ASC, section ASC");
        $shelves = [];
        while($row_shelves = $MysqlDb->get_result($result_shelves)){
            $shelves[$row_shelves['num']][] = $row_shelves;
        }

        $shelf_numbers = array_keys($shelves);
        sort($shelf_numbers);

        $Html->content_data = [
            'shelf_numbers' => $shelf_numbers,
            'shelves' => $shelves,
            'count' => $count,
            'checked' => $checked
        ];
        parent::_html();
    }
}
