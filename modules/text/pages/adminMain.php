<?php

namespace controller\text;

class adminMain extends \Controller {

	function __construct() {
		global $Acl;
		parent::__construct();

		$Acl->permission_redirect("admin");
	}

	function add_page(){
		global $Router, $MysqlDb, $UserInfo, $Html;

		$languages = config('languages');
		foreach ($languages as $lang){
			if($Router->post('title_'.$lang) == false){
                echo request_callback([
                    'status'        => "error",
                    'error_message' => $Html->_lang('fill_all_fields')
                ]);
				exit;
			}
		}

		$Text = new \model\text\Text();
		$text_id = $Router->get_int('edit_id') !== 0 ? $Router->get_int('edit_id') : $Text->next_text_id();

		$insert_fields['description'] = $Router->post('description');
		foreach ($languages as $lang){
			$insert_fields['title'] = $Router->post('title_'.$lang);
			if($Router->get_int('edit_id') !== 0 && $MysqlDb->record_exist("texts", "text_id = ".$Router->get_int('edit_id')." AND lang = '".$lang."'")){
				$insert_fields['update_user_id'] = $UserInfo->user_id;
				$insert_fields['update_time'] = current_time();

				$MysqlDb->update("texts", $insert_fields, "text_id = ".$Router->get_int('edit_id')." AND lang = '".$lang."'");
			}
			else{
				$insert_fields['lang'] = $lang;
				$insert_fields['text_id'] = $text_id;
				$insert_fields['add_user_id'] = $UserInfo->user_id;
				$insert_fields['add_time'] = current_time();

				$MysqlDb->insert("texts", $insert_fields);
			}
		}

        echo request_callback([
            'status'        => "ok",
        ]);
		exit;
	}

	function delete_page(){
		global $Router, $MysqlDb;

		$MysqlDb->delete("texts", "text_id = ".$Router->get_int('text_id'));

		echo "ok";
		exit;
	}

	function add_text_form(){
		global $Router, $Html, $MysqlDb;

		$edit_value = [];
		if($Router->get_int('edit_id') !== 0){
			$result = $MysqlDb->select("texts", "*", "text_id = ".$Router->get_int('edit_id'));

			while ($row = $MysqlDb->get_result($result)) {
				$edit_value['title_'.$row['lang']] = $row['title'];
				$edit_value['description'] = $row['description'];
			}
		}

		$Html->content_data = [
			'edit_value'  => $edit_value,
			'languages' => config('languages')
		];

		$Router->page = "add_page";
		parent::_html();
	}

	function _html() {
		global $Html, $MysqlDb, $Router;

		$result = $MysqlDb->select("texts", "*", 1, "id ASC");
		$texts = $text_ids = [];
		while ($row = $MysqlDb->get_result($result)) {
			$texts[$row['text_id']][$row['lang']] = $row['title'];
			$texts[$row['text_id']]['description'] = $row['description'];
			$text_ids[$row['text_id']] = $row['text_id'];
		}

		$Html->content_data = [
			'texts'     => $texts,
			'text_ids'  => $text_ids,
			'languages' => config('languages')
		];

		parent::_html();
	}
}
