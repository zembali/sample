<?php
namespace controller\text;

class contactUs extends \Controller {

	function __construct() {
		parent::__construct();
	}

	function _html() {
		global $Router, $Html;

		$Text = new \model\text\Text();
		$text_info = $Text->get_text_info($Router->get_int('text_id'));
		$Html->content_data['title'] = $text_info['title'];
		$Html->content_data['text'] = $text_info['text'];
		$Html->content_data['text_id'] = $Router->get_int('text_id');
		$Html->content_data['lang'] = $Router->app_lang();

		$Html->content_data['no_title'] = $Router->get('no_title');
		parent::_html();
	}
}