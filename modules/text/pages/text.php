<?php
namespace controller\text;

class text extends \Controller {

	function __construct() {
		parent::__construct();
	}

	function popup_text(){
		global $Html, $Router;

		$Router->page = "popupText";
		$this->_html();
	}

	function send_message(){
		global $Html, $Router, $Form;

		$good_fields = [];
		if($Router->post('name') == false){
			echo request_callback("error", $Html->_lang('fill_required_fields'), "form_field_error", ['field_id' => "name"]);
			exit;
		}
		$good_fields[] = "name";

		$email_validation = $Form->email_validation($Router->post('email'));
		if($email_validation !== "ok"){
			echo request_callback("error", $email_validation, "form_field_error", ['field_id' => "email", 'good_fields' => $good_fields]);
			exit;
		}
		$good_fields[] = "email";

		if($Router->post('message') == false){
			echo request_callback("error", $Html->_lang('fill_required_fields'), "form_field_error", ['field_id' => "message", 'good_fields' => $good_fields]);
			exit;
		}
		/* if(post('verif_image') !== $_SESSION['verif_key']){
			echo request_callback("error", _NO_VERIF_KEY);
			exit;
		} */

		$mail_text = "<b>Name:</b> ".$Router->post('name')."\r\n<BR><BR>
					<b>Email:</b> ".$Router->post('email')."\r\n<BR><BR>
					<b>Subcet:</b> ".$Router->post('subject')."\r\n<BR><BR>
    			  <b>Message:</b><br /> ".nl2br($Router->post('message'))."\r\n<BR><BR>";
		send_mail(config('contact_email'), "From CarEX", $mail_text);

		echo request_callback("ok_message", $Html->_lang('message_sent'), "clear_fields");
		exit;
	}

	function _html() {
		global $Router, $Html;

		$Text = new \model\text\Text();

		$text_info = $Text->get_text_info($Router->get_int('text_id'));
		$Html->content_data['title'] = $text_info['title'];
		$Html->content_data['text'] = $text_info['text'];
		$Html->content_data['text_id'] = $Router->get_int('text_id');
		$Html->content_data['lang'] = $Router->app_lang();

		$Html->content_data['no_title'] = $Router->get('no_title');

		parent::_html();
	}
}