<?php
namespace controller\text;

class editText extends \Controller {

	function __construct() {
		global $Acl;
		parent::__construct();

		$Acl->permission_redirect("admin");
	}

	function save_text(){
		global $Router, $MysqlDb, $UserInfo;

		$MysqlDb->prepare_vars['lang'] = $Router->get('lang');
		$insert_fields = [
			'title'=>$Router->post('title'),
			'text'=>$Router->post_with_tags('text'),
		];
		if(!$MysqlDb->record_exist("texts", "text_id = ".$Router->get_int('text_id')." AND lang = '{{lang}}'")){
			$insert_fields['text_id'] = $Router->get_int('text_id');
			$insert_fields['lang'] = $Router->get('lang');
			$insert_fields['add_user_id'] = $UserInfo->user_id;
			$insert_fields['add_time'] = current_time();

			$MysqlDb->insert("texts", $insert_fields);
		}
		else {
			$insert_fields['update_user_id'] = $UserInfo->user_id;
			$insert_fields['update_time'] = current_time();
			$MysqlDb->update("texts", $insert_fields, "text_id = ".$Router->get_int('text_id')." AND lang = '{{lang}}'");
		}

        echo request_callback([
            'status'       => "ok",
            'redirect_url' => "/text/text?text_id=".$Router->get_int('text_id')."&lang=".$Router->get('lang')
        ]);
		exit;
	}



	function _html() {
		global $Router, $Html, $MysqlDb;

		$Text = new \model\text\Text();
		$text_info = $Text->get_text_info($Router->get_int('text_id'));
		$Html->content_data['title'] = $text_info['title'];
		$Html->content_data['text'] = $text_info['text'];
		$Html->content_data['text_id'] = $Router->get_int('text_id');
		$Html->content_data['lang'] = $Router->app_lang();

		parent::_html();
	}
}