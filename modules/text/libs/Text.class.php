<?php
namespace model\text;

class Text {

	function get_text_info($text_id){
		global $MysqlDb, $Router;

		$MysqlDb->prepare_vars['lang'] = $Router->app_lang();
		return $MysqlDb->get_first_row("texts", "*", "lang = '{{lang}}' AND text_id = ".(int)$text_id);
	}

	function next_text_id(){
		global $MysqlDb;

		$max_id = $MysqlDb->get_first_row("texts", "text_id", 1, "text_id DESC");

		return $max_id['text_id'] + 1;
	}
}