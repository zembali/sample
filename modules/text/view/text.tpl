<div class="text_main">
	<? if ($Acl->has_permission("admin")): ?>
		<div class="edit_text">
			<a href="/admin/text/editText/?text_id=<?= $this->e('text_id') ?>&lang=<?= $this->e('lang') ?>">
                <i class="fas fa-edit text-danger" aria-hidden="true"></i>
            </a>
		</div>
	<? endif; ?>
	<? if ((int)$this->e('no_title') !== 1) : ?>
		<h3 class="page_title"><?= $this->e('title') ?></h3>
	<? endif; ?>
	<div class="text"><?= $this->e('text') ?></div>
	<div class="clearfix"></div>
</div>
<div class="clearfix-20"></div>

<? if ($Acl->has_permission("admin")): ?>
	<script>
		$(".text_main").mouseover(function () {
			$(this).children(".edit_text").show();
			$(this).css("border-color", "#ea8f8f");
		})
		$(".text_main").mouseout(function () {
			$(this).children(".edit_text").hide();
			$(this).css("border-color", "transparent");
		})
	</script>
<? endif; ?>