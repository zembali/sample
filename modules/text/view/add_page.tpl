<div class="modal-header">
	<h4 class="modal-title">Add Page</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
	<form id="add_page_form" class="form-horizontal" method="post"
	      action="/post/<?= $module ?>/adminMain/add_page/?edit_id=<?= $Router->get_int('edit_id') ?>"
	      onsubmit="return false">
		<table class="table">
			<? foreach ($languages as $lang): ?>
				<tr>
					<td><?= $lang ?></td>
					<td>
						<?= $Form->input_form("title_".$lang, "text", $edit_value, "", "", "form-control") ?>
					</td>
				</tr>
			<? endforeach; ?>
			<tr>
				<td>Description</td>
				<td>
					<?= $Form->input_form("description", "text", $edit_value, "", "", "form-control") ?>
				</td>
			</tr>
		</table>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<input type="hidden" id="add_page" nam="add_page" value="0">
				<button id="submit" type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</form>
</div>

<script>
	$("#add_page_form").submit(function () {
		post_call(this, function(){
		    reload();
        })
	})
</script>
