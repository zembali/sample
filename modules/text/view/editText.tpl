<form id="save_text_form" method="post" action="/post/text/editText/save_text/?text_id=<?=$this->e('text_id')?>&lang=<?=$this->e('lang')?>" onsubmit="return false">
    <div class="form-group">
        <label for="title">Title:</label>
        <?=$Form->input_form("title", "text", $this->e('title'), "", "", "form-control") ?>
    </div>
    <div class="form-group">
        <label for="text">Text:</label>
        <?=$Form->input_form("text", "textarea_editor", $this->e('text'), 1, "", "form-control") ?>
    </div>
    <input type="hidden" id="save_text" name="save_text" value="0">
    <button type="submit" class="btn btn-default">Submit</button>
</form>

<script type="text/javascript">
	var instance = CKEDITOR.instances.editor1;
	if (instance) {
		CKEDITOR.remove(instance);
	}
	CKEDITOR.replace('editor1',
		{
			fullPage: false,
			sidebarWidth: '0',

			enterMode: Number(1),
			toolbar: 'gt_toolbar',
			height: '400', width: '800'
		});

	//*** save form
	$("#save_text_form").submit(function () {
		$("button").attr('disabled', true);
		$("#add_info_form textarea").each(function () {
				if (this.id.indexOf('editor') !== -1) {
					$(this).html(eval('CKEDITOR.instances.' + this.id + '.getData()'));
				}
			}
		)

        var obj = this;
		setTimeout(function() {
			post_call(obj);
		}, 500);
	})
</script>