<div class="text_main">
	<h3 class="page_title"><?= _lang('contact_us') ?></h3>
</div>

<div class="contact_us">
	<div class="col-md-12 map">
		<?=$Router->load_module_content_with_parameters("text/text", ['text_id' => 15, 'no_title' => 1])?>
	</div>

	<div class="col-md-6">
		<h3><?= _lang('send_us_mail') ?>:</h3>
		<form id="send_message_form" action="/post/text/text/send_message/" onsubmit="return false">
			<div class="form-group">
				<input type="text" class="form-control" id="name" name="name" placeholder="<?= _lang('name') ?>">
			</div>
			<div class="form-group">
				<input type="email" class="form-control" id="email" name="email" placeholder="<?= _lang('email') ?>">
			</div>
			<div class="form-group">
				<input type="text" class="form-control" id="subject" name="subject" placeholder="<?= _lang('subject') ?>">
			</div>
			<div class="form-group">
				<textarea class="form-control" id="message" name="message" rows="4" placeholder="<?= _lang('message') ?>"></textarea>
			</div>
			<input type="hidden" id="send_message" name="send_message">
			<button id="submit" class="btn pull-right" type="submit" name="button">
				<?= _lang('send') ?>
			</button>
		</form>
	</div>

	<div class="col-md-5 pull-right">
		<div class="col-md-10 pull-right" style="position: relative">
			<div class="clearfix-20"></div>
			<div class="clearfix-20"></div>
			<?= $Router->load_module_content_with_parameters("text/text", ['text_id'  => 11,
			                                                               'no_title' => 0
			]) ?>
		</div>
	</div>

	<div class="clearfix-20"></div>
</div>

<script>
	function clear_fields(){
		$("#send_message_form input[type='text']").val('');
		$("#send_message_form input[type='email']").val('');
		$("#send_message_form textarea").val('');
	}

	var stop_scrolltop = 1;
	$("#send_message_form").submit(function () {
		post_call("send_message", this);
	})
</script>
