<a class="btn btn-success" href="/body/<?=$module?>/adminMain/add_text_form" data-toggle="modal" data-target="#popup_lg">
	<i class="fa fa-plus"></i> Add Page
</a>
<div class="clearfix-10"></div>

<table class="table table-striped table-bordered">
	<tr>
		<? foreach ($languages as $lang): ?>
			<th>Title (<?= $lang ?>)</th>
		<? endforeach; ?>
		<th>Description</th>
		<th>Url</th>
		<th colspan="2"></th>
	</tr>
	<? foreach ($text_ids as $text_id): ?>
		<tr id="text_<?=$text_id?>">
			<? foreach ($languages as $lang): ?>
				<td><?= isset($texts[$text_id][$lang]) ? $texts[$text_id][$lang] : "" ?> </td>
			<? endforeach; ?>
			<td><?=$texts[$text_id]['description']?></td>
			<td>
				<a target="_blank" href="/text/text/?text_id=<?=$text_id?>">
					/text/text/?text_id=<?=$text_id?>
				</a>
			</td>
			<td class="text-center">
				<a href="/body/<?=$module?>/adminMain/add_text_form/?edit_id=<?=$text_id?>" data-toggle="modal" data-target="#popup_lg">
					<i class="fa fa-edit" aria-hidden="true"></i>
				</a>
			</td>
			<td class="text-center">
				<i data-id="<?=$text_id?>" class="fa fa-trash delete_page" aria-hidden="true"></i>
			</td>
		</tr>
	<? endforeach; ?>
</table>

<script>
	$('.delete_page').confirmModal({
		confirmTitle     : 'Delete Page',
		confirmMessage   : 'Are you sure?',
		confirmCallback  : function(target, modal){
			$.get('/post/<?=$module?>/adminMain/delete_page/', {text_id: $(target).data("id")})
				.done(function(data){
					if(data == "ok"){
						$("#text_"+$(target).data("id")).hide();
					}
				})
		},
	});
</script>