<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<? if ((int)$this->e('no_title') !== 1) : ?>
		<h4><?= $this->e('title') ?></h4>
	<? endif; ?>
</div>

<div class="modal-body text_main">
	<? if ($Acl->has_permission("admin")): ?>
		<div class="edit_text">
			<a href="/admin/text/editText/?text_id=<?= $this->e('text_id') ?>&lang=<?= $this->e('lang') ?>"><i
						class="fa fa-pencil" aria-hidden="true"></i></a>
		</div>
	<? endif; ?>
	<div class="text"><?= $this->e('text') ?></div>
</div>


<? if ($Acl->has_permission("admin")): ?>
	<script>
		$(".text_main").mouseover(function () {
			$(this).children(".edit_text").show();
			$(this).css("border-color", "#ea8f8f");
		})
		$(".text_main").mouseout(function () {
			$(this).children(".edit_text").hide();
			$(this).css("border-color", "transparent");
		})
	</script>
<? endif; ?>