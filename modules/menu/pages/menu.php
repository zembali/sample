<?php
namespace controller\menu;

class menu extends \Controller {

	function _html() {
		global $Router, $Html;

		$url_parts = explode("/", $Router->get('url'));

		$Html->content_data['current_page'] = count($url_parts) > 1 ? $url_parts[1] : "";
		$Html->content_data['current_module'] = count($url_parts) > 0 ? $url_parts[0] : "";

		parent::_html();
	}
}