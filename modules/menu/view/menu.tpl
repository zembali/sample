<!-- web -->
<nav class="navbar navbar-expand-sm bg-light">
    <!-- Links -->
    <ul class="navbar-nav">
        <? if($Acl->has_permission("orders_view", "wc")): ?>
            <li class="nav-item <?= $current_module == "wc" && $current_page == "orders" ? "active" : "" ?>">
                <a href="/wc/orders" class="nav-link">Orders</a>
            </li>
        <? endif; ?>

        <? if($Acl->has_permission("products_view", "wc")): ?>
            <li class="nav-item <?= $current_module == "wc" && $current_page == "products" ? "active" : "" ?>">
                <a href="/wc/products" class="nav-link">Products</a>
            </li>
        <? endif; ?>

        <? if($Acl->has_permission("", "labels")): ?>
            <li class="nav-item <?= $current_module == "labels" ? "active" : "" ?>">
                <a href="/labels" class="nav-link">Labels</a>
            </li>
        <? endif; ?>

        <? if($Acl->has_permission("change_price", "wc")): ?>
            <li class="nav-item <?= $current_module == "wc" && $current_page == "updatePrice" ? "active" : "" ?>" style="width: 135px">
                <a href="/wc/updatePrice" class="nav-link">Price Update</a>
            </li>
        <? endif; ?>

        <? if($Acl->has_permission("orders_admin", "wc")): ?>
            <li class="nav-item <?= $current_module == "wc" && $current_page == "qbConfig" ? "active" : "" ?>">
                <a href="/wc/qbConfig" class="nav-link">QB</a>
            </li>
        <? endif; ?>
    </ul>

    <? if($UserInfo->auth_status()): ?>
        <div class="user ml-2 bg-white p-2 small">
            <? if ($Acl->is_root_admin()): ?>
                <a href="/admin/" class="text-dark">Admin Panel</a>
            <? else: ?>
                <span><?= $UserInfo->get_user_info_by_id()['first_name'] ?></span>
            <? endif; ?>
            | <a href="/users/logout/" class="text-info">Logout</a>
        </div>
    <? endif; ?>
</nav>
